<?php

	class m_login extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'u_username';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($user, $pass)
		{
			$user = $this->get_by( 

				array (
					'u_username' => $user,
					'u_password' => $pass,
				));
			

			if($user)
			{
				if($user[0]->u_rights == 1){
					foreach ($user as $row)
					{
						$data = array (
							'u_id'   			=> $row->u_id,
							'u_points'   		=> $row->u_points,
							'u_username'   		=> $row->u_username,
							'u_fullname' 		=> $row->u_fullname,
							'u_email' 			=> $row->u_email,
							'u_bday' 			=> $row->u_bday,
							'u_role' 			=> $row->u_role,
							'prof_pic' 			=> $row->prof_pic,
							'loggedin' 			=> TRUE
						);
						$this->session->set_userdata($data);
						return true;
					}
				}else{
					return false;
				}
			}
			else{
				return false;
				
			}
		}
		public function logout()
		{
			$this->session->set_userdata(array());
			$this->session->sess_destroy();
			//redirect('login');
			redirect(base_url(),'refresh');
		}
	}