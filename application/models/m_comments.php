<?php

	class m_comments extends MY_Model
	{
		protected $_table_name = 'user_comments';
		protected $_order_by = 'comment_date';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function countComments($id)
		{
			$getComments = $this->db->get_where('user_comments', array('link_id' => $id));
			
			return $getComments->result();
		}
		public function countReplyComments($id)
		{
			$getComments = $this->db->get_where('user_replyto_comments', array('link_id' => $id));
			
			return $getComments->result();
		}
		public function countLikesInComment($id)
		{
			
			$getFavs = $this->db->get_where('user_favorite_links', array('link_id' => $id));
			
			return $getFavs->result();
		}
		public function addLikesToComment($data){

			$query = $this->db->insert('user_like_comments', $data);
			$id = $this->db->insert_id();
			if($id > 0){
				return true;
			}else{
				return false;
			}
		}
		public function addLikesReplyToComment($data){

			$query = $this->db->insert('user_like_reply_comments', $data);
			$id = $this->db->insert_id();
			if($id > 0){
				return true;
			}else{
				return false;
			}
		}
		public function addReplyCommentToLink($data){

			$query = $this->db->insert('user_replyto_comments', $data);
			$id = $this->db->insert_id();
			if($id > 0){
				return true;
			}else{
				return false;
			}
		}
		public function getLinksComment($id)
		{
			$newArray = array();

			$this->db->order_by("comment_date", "desc");
			$getComments = $this->db->get_where('user_comments', array('link_id' => $id));
			
			foreach ($getComments->result() as $row)
			{
				$getUserInfo = $this->db->get_where('user_info', array('u_id' => $row->comment_user_id));
				foreach ($getUserInfo->result() as $row2)
				{
					$getLike = $this->db->get_where('user_like_comments', array('comment_id' => $row->id, 'user_id' => $this->session->userdata('u_id')));
					if(count($getLike->result()) > 0){
						$likedByMe = 1;
					}else{
						$likedByMe = 0;
					}
					$data = array (
						'id'   						=> $row->id,
						'link_id'   				=> $row->link_id,
						'link_user_id'   			=> $row->link_user_id,
						'comment_user_id'   		=> $row->comment_user_id,
						'comment_text'   			=> $row->comment_text,
						'comment_date'   			=> date('Y-m-d h:i A', strtotime($row->comment_date)),
						'comment_user_fullname'   	=> $row2->u_fullname,
						'comment_user_points'   	=> $row2->u_points,
						'comment_user_prof_pic'   	=> $row2->prof_pic,
						'liked_by_me'   			=> $likedByMe,
						'comment_user_role'   		=> $row2->u_role
					);
					array_push($newArray,$data);
				}
			}
			
			return $newArray;
		}
		public function getReplyToComment($id)
		{
			$newArray = array();

			$this->db->order_by("comment_date", "desc");
			$getComments = $this->db->get_where('user_replyto_comments', array('comment_id' => $id));
			
			foreach ($getComments->result() as $row)
			{
				$getUserInfo = $this->db->get_where('user_info', array('u_id' => $row->comment_user_id));
				foreach ($getUserInfo->result() as $row2)
				{
					$getLike = $this->db->get_where('user_like_reply_comments', array('comment_id' => $row->id, 'user_id' => $this->session->userdata('u_id')));
					if(count($getLike->result()) > 0){
						$likedByMe = 1;
					}else{
						$likedByMe = 0;
					}
					$data = array (
						'id'   						=> $row->id,
						'link_id'   				=> $row->link_id,
						'link_user_id'   			=> $row->link_user_id,
						'comment_user_id'   		=> $row->comment_user_id,
						'comment_text'   			=> $row->comment_text,
						'comment_date'   			=> date('Y-m-d h:i A', strtotime($row->comment_date)),
						'comment_user_fullname'   	=> $row2->u_fullname,
						'comment_user_points'   	=> $row2->u_points,
						'comment_user_prof_pic'   	=> $row2->prof_pic,
						'liked_by_me'   			=> $likedByMe,
						'comment_user_role'   		=> $row2->u_role
					);
					array_push($newArray,$data);
				}
			}
			
			return $newArray;
		}
	}