<?php

	class m_chat_messages extends MY_Model
	{
		protected $_table_name = 'user_chat_messages';
		protected $_order_by = 'date_created';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getMyChatMessage()
		{
			$chat_id = $this->input->post("chat_id");
			$room_id = $this->input->post("room_id");
			$newArray = array();		
			$getChatMsg = $this->db->get_where('user_chat_messages', array('your_chat_user_id' => $chat_id,'user_id' => $this->session->userdata('u_id')));
			
			foreach ($getChatMsg->result() as $row1)
			{
				$getPic1 = $this->db->get_where('user_info', array('u_id' => $this->session->userdata('u_id')));
				foreach ($getPic1->result() as $row2)
				{
					$data = array (
						'id'   						=> $row1->id,
						'user_id'   				=> $row1->user_id,
						'your_chat_user_id'   		=> $row1->your_chat_user_id,
						'chat_message'   			=> $row1->chat_message,
						'chat_date'   				=> $row1->chat_date,
						'chat_hrs'   				=> date("h:ia",strtotime($row1->chat_date)),
						'prof_pic'   				=> $row2->prof_pic,
						'position'   				=> 0,
						'baseURL'   				=> base_url(),
					);
						array_push($newArray,$data);
				
				}
			}
			
			$getTheirChatMsg = $this->db->get_where('user_chat_messages', array('user_id' => $chat_id,'your_chat_user_id' => $this->session->userdata('u_id')));
			
			foreach ($getTheirChatMsg->result() as $row1)
			{
				$getPic = $this->db->get_where('user_info', array('u_id' => $chat_id));
				foreach ($getPic->result() as $row2)
				{	
					$data2 = array (
						'id'   						=> $row1->id,
						'user_id'   				=> $row1->user_id,
						'your_chat_user_id'   		=> $row1->your_chat_user_id,
						'chat_message'   			=> $row1->chat_message,
						'chat_date'   				=> $row1->chat_date,
						'chat_hrs'   				=> date("h:ia",strtotime($row1->chat_date)),
						'prof_pic'   				=> $row2->prof_pic,
						'position'   				=> 1,
						'baseURL'   				=> base_url(),
					);
						array_push($newArray,$data2);
				
				}
			}
			return $newArray;
		}

		
	}