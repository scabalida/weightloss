<?php

	class m_user extends MY_Model
	{
		protected $_table_name = 'user_info';
		protected $_order_by = 'u_username';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getAllTotalWeightLoss(){
			$getUserID = $this->db->get('user_info');
			$totWeightLoss = 0;
			$totAll = 0;
			foreach($getUserID->result() as $gID){
				$this->db->select('date, weight_loss');
				$this->db->where('user_id', $gID->u_id);
				$this->db->from('user_weight_loss');
				$this->db->order_by("date", "asc");
				$getWeights = $this->db->get();
				
				$userWeight = $getWeights->result();
				if(count($userWeight) > 0){
					$totWeightLoss = floatval($userWeight[0]->weight_loss) - floatval($userWeight[count($userWeight)-1]->weight_loss); 
					$totAll += $totWeightLoss;
				}
				//echo $userWeight[0]->weight_loss;
				//echo $userWeight[count($userWeight)-1]->weight_loss;
				//print_r($userWeight);
				
			}	
			
			return $totAll;
			
		}
		public function topWeightLoss(){
			//$this->db->limit(5);
			$getUserID = $this->db->get('user_info');
			$totWeightLoss = 0;
			$newArray = array();
			foreach($getUserID->result() as $gID){
				$this->db->select('date, weight_loss');
				$this->db->where('user_id', $gID->u_id);
				$this->db->from('user_weight_loss');
				$this->db->order_by("date", "asc");
				$getWeights = $this->db->get();
				
				$userWeight = $getWeights->result();
				if(count($userWeight) > 0){
					$totWeightLoss = floatval($userWeight[0]->weight_loss) - floatval($userWeight[count($userWeight)-1]->weight_loss); 
					$data = array(
						"my_weightloss"	=> $totWeightLoss,
						"u_id" 			=> $gID->u_id,
						"u_username"	=> $gID->u_username,
						"u_fullname"	=> $gID->u_fullname
						
					);
					array_push($newArray,$data);
				}else{
					$data = array(
						"my_weightloss"	=> 0,
						"u_id" 			=> $gID->u_id,
						"u_username"	=> $gID->u_username,
						"u_fullname"	=> $gID->u_fullname
						
					);
					array_push($newArray,$data);
				}
				//echo $userWeight[0]->weight_loss;
				//echo $userWeight[count($userWeight)-1]->weight_loss;
				//print_r($userWeight);
				
			}	
			//arsort($newArray);
			//print_r($newArray);
			return $newArray;
			
		}
		public function addWeightLoss($weight)
		{
			
			$data = array(
				'date' 			=> date('Y-m-d'),
				'weight_loss' 	=> $weight,
				'user_id' 		=> $this->session->userdata('u_id')
			);

			$this->db->insert('user_weight_loss', $data);
			if($this->db->affected_rows()){
				return true;
			}else{
				return false;
			}
		}
		public function editWeightLoss($data,$id)
		{
			$query = $this->db->update('user_weight_loss', $data, array('id' => $id));
			if($query){
				return true;
			}else{
				return false;
			}
			
		
		}
		public function getAllMyWeightLoss()
		{

			$this->db->select('*');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->from('user_weight_loss');
			$this->db->order_by("date", "desc");
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getSpecificWeightLoss($id)
		{
			
			$query = $this->db->get_where('user_weight_loss', array('id' => $id));

			return $query->result();
		
		}
		public function getWeightLoss()
		{

			$this->db->select('date as label, weight_loss as y');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->from('user_weight_loss');
			$this->db->order_by("date", "asc");
			$this->db->limit(15);
			$query = $this->db->get();

			return $query->result();
		
		}
		public function checkInputWeightLoss()
		{
			
			$query = $this->db->get_where('user_weight_loss', array('date' => date('Y-m-d'), 'user_id' => $this->session->userdata('u_id')));

			return $query->result();
		
		}
		public function getUserData($id)
		{
			$this->db->select('u_id, prof_pic, u_username, u_fullname, u_email, u_bday, u_bio, u_points, u_role, u_rights');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAllUserPoints()
		{
			
			$this->db->select('u_points, u_username, u_fullname');
			$this->db->from('user_info');
			$this->db->order_by("u_points", "desc");
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUserPoints($id)
		{
			$this->db->select('u_points');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getAllUsers($id)
		{
			$newArray = array();
			$this->db->select('u_id, prof_pic, u_username, u_fullname, u_email, u_bday, u_role, u_rights');
			$this->db->where('u_id !=', $id);
			$this->db->from('user_info');
			$this->db->order_by("u_id", "desc");
			$query = $this->db->get();
			 foreach($query->result() as $row){
				$this->db->select('*');
				$this->db->where('user_id', $row->u_id);
				$this->db->from('user_weight_loss');
				$this->db->order_by("date", "asc");
				$userWeight = $this->db->get();
				$getWeight = $userWeight->result();
				//print_r($getWeight);
				if(count($getWeight) > 1){
					$totWeightLoss = floatval($getWeight[0]->weight_loss) - floatval($getWeight[count($getWeight)-1]->weight_loss); 
					if($totWeightLoss < 0){
						$totWeightLoss = "<span style='color:red'>&uarr;</span> ".abs($totWeightLoss);
					}else if($totWeightLoss > 0){
						$totWeightLoss = "<span style='color:green'>&darr;</span> ".abs($totWeightLoss);
					}
					$data = array(
						'u_id' => $row->u_id,
						'u_username' => $row->u_username,
						'prof_pic' => $row->prof_pic,
						'u_fullname' => $row->u_fullname,
						'u_email' =>  $row->u_email,
						'u_bday' =>  $row->u_bday,
						'u_role' => $row->u_role,
						'u_rights' => $row->u_rights,
						'total_weight_loss' => $totWeightLoss,
						'current_weight' => $getWeight[0]->weight_loss
					);
					
				}else if(count($getWeight) > 0 && count($getWeight) < 2){
					$data = array(
						'u_id' => $row->u_id,
						'u_username' => $row->u_username,
						'prof_pic' => $row->prof_pic,
						'u_fullname' => $row->u_fullname,
						'u_email' =>  $row->u_email,
						'u_bday' =>  $row->u_bday,
						'u_role' => $row->u_role,
						'u_rights' => $row->u_rights,
						'total_weight_loss' => 0,
						'current_weight' => $getWeight[0]->weight_loss
					);
					
				}else{
					$data = array(
						'u_id' => $row->u_id,
						'u_username' => $row->u_username,
						'prof_pic' => $row->prof_pic,
						'u_fullname' => $row->u_fullname,
						'u_email' =>  $row->u_email,
						'u_bday' =>  $row->u_bday,
						'u_role' => $row->u_role,
						'u_rights' => $row->u_rights,
						'total_weight_loss' => 0,
						'current_weight' => 0
					);
				}
				array_push($newArray,$data);
				
			 }
			return $newArray;
		
		}
		
		public function getMyPicture()
		{
			$id = $this->session->userdata('u_id');
			$this->db->select('prof_pic');
			$this->db->where('u_id', $id);
			$this->db->from('user_info');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getUnameAndPass($pass, $id)
		{
			$data = array();
			$user = $this->get_by( 

				array (
					'u_id' => $id,
					'u_password' => $pass
				));
			
			
			if($user)
			{
				
					foreach ($user as $row)
					{
						$data = array (
							'u_id'   			=> $row->u_id,
							'u_username'   		=> $row->u_username
						);
						
						return $data;
					}

			}
			else{
				return $data;
				
			}
		}
	}