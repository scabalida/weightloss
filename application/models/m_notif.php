<?php

	class m_notif extends MY_Model
	{
		protected $_table_name = 'user_notifications';
		protected $_order_by = 'comment_date';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getUserID($id)
		{
			
			$this->db->select('user_id');
			$this->db->from('user_link_post');
			$this->db->where('u_id', $id);
			$query = $this->db->get();

			return $query->result();
		
		}
		public function countNotReadNotif()
		{
			
			$this->db->select('*');
			$this->db->from('user_notifications');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->where('notif_read', 1);
			$query = $this->db->get();

			return $query->result();
		
		}
		public function makeNotifRead()
		{
			
			$query = $this->db->update('user_notifications', array('notif_read' => 0), array('user_id' => $this->session->userdata('u_id')));

			if($query){
				return true;
			}else{
				return false;
			}
		
		}
		public function getMyNotif()
		{
			$newArray = array();
			$notifTxt = "";
			$profPic = "";
			$this->db->order_by("notif_date", "desc");
			$getNotif = $this->db->get_where('user_notifications', array('user_id' => $this->session->userdata('u_id')));
			foreach ($getNotif->result() as $row1)
			{
				$getNotifUserInfo = $this->db->get_where('user_info', array('u_id' => $row1->notif_user_id));
				foreach ($getNotifUserInfo->result() as $row2)
				{
					$getLinkName = $this->db->get_where('user_link_post', array('u_id' => $row1->link_id));
					foreach ($getLinkName->result() as $row3)
					{
						if($row1->notif_type == 1){
							$notifTxt = "gillar din link";
							$profPic = $row2->prof_pic;
						}else if($row1->notif_type == 2){
							$notifTxt = "kommenterade din länk";
							$profPic = $row2->prof_pic;
						}else if($row1->notif_type == 3){
							$notifTxt = "Grattis! Din länk har godkänts!";
							$profPic = "balloon.jpg";
						}else if($row1->notif_type == 4){
							$notifTxt = "gillar din kommentar";
							$profPic = $row2->prof_pic;
						}else if($row1->notif_type == 5){
							$notifTxt = "har svarade på din kommentar";
							$profPic = $row2->prof_pic;
						}

						$data = array (
							'id'   						=> $row1->id,
							'user_id'   				=> $row1->user_id,
							'link_id'   				=> $row1->link_id,
							'notif_type'   				=> $row1->notif_type,
							'notif_text'   				=> $notifTxt,
							'notif_user_id'   			=> $row1->notif_user_id,
							'notif_date'   				=> $row1->notif_date,
							'prof_pic'   				=> $profPic,
							'u_fullname'   				=> $row2->u_fullname,
							'link_name'   				=> $row3->link_title,
							'baseURL'   				=> base_url(),
						);
						array_push($newArray,$data);
					}
				}
			}
			
			return $newArray;
		
		}
	}