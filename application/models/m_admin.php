<?php

	class m_admin extends MY_Model
	{
		protected $_table_name = 'room';
		protected $_order_by = 'name';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getRoom()
		{
			
			$query = $this->db->get('room');
			return $query->result();
		
		}

		public function selectFour(){
			
			$query = $this->db->query("SELECT TOP(2) FROM room");
			return $query->result();
		}
		
		public function getAttachment($id){
			$query = $this->db->get_where('attachment', array('game_id' => $id));
			
			return $query->result();
		}
		
	}