<?php

	class m_wall_status extends MY_Model
	{
		protected $_table_name = 'user_wall_status';
		protected $_order_by = 'date';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function addWallStatusComment($data)
		{

			$this->db->insert('user_wall_comments', $data);
			if($this->db->affected_rows()){
				return true;
			}else{
				return false;
			}
		}
		public function addCommentToWallStatusComment($data)
		{

			$this->db->insert('user_replyto_wall_comments', $data);
			if($this->db->affected_rows()){
				return true;
			}else{
				return false;
			}
		}
		public function getWeightLoss()
		{

			$this->db->select('weight_loss');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->from('user_weight_loss');
			$this->db->order_by("date", "desc");
			$this->db->limit(1);
			$query = $this->db->get();

			return $query->result();
		
		}
		public function getMyWallStatus()
		{
			$newArray = array();
			$this->db->select('*');
			$this->db->where('user_id', $this->session->userdata('u_id'));
			$this->db->from('user_wall_status');
			$this->db->order_by("date", "desc");
			$query = $this->db->get();
			
			$prof_pic = $this->db->get_where('user_info', array('u_id' => $this->session->userdata('u_id')));
			$img = $prof_pic->result();
			if(count($img) > 0){
				$pic = "images/uploads/".$this->session->userdata('u_id')."/".$img[0]->prof_pic;
			}else{
				$pic = "images/user-pic.png";
			}
			foreach ($query->result() as $row)
			{
				$getWallComments = $this->db->get_where('user_wall_comments', array('wall_status_id' => $row->id));
				$numComments = count($getWallComments->result());
				
				$data = array (
					'id'   					=> $row->id,
					'user_id'   			=> $this->session->userdata('u_id'),
					'status_description'   	=> $row->status_description,
					'current_weight'   		=> $row->current_weight,
					'prof_pic'   			=> $pic,
					'numComments'   		=> $numComments,
					'date'   				=> $row->date,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		}
		public function getFriendsWallStatus($id)
		{
			$newArray = array();
			$this->db->select('*');
			$this->db->where('user_id', $id);
			$this->db->from('user_wall_status');
			$this->db->order_by("date", "desc");
			$query = $this->db->get();
			
			$prof_pic = $this->db->get_where('user_info', array('u_id' => $id));
			$img = $prof_pic->result();
			if(count($img) > 0){
				$pic = "images/uploads/".$id."/".$img[0]->prof_pic;
			}else{
				$pic = "images/user-pic.png";
			}
			foreach ($query->result() as $row)
			{
				$getWallComments = $this->db->get_where('user_wall_comments', array('wall_status_id' => $row->id));
			
				$numComments = count($getWallComments->result());
				
				$data = array (
					'id'   					=> $row->id,
					'user_id'   			=> $id,
					'status_description'   	=> $row->status_description,
					'current_weight'   		=> $row->current_weight,
					'prof_pic'   			=> $pic,
					'numComments'   		=> $numComments,
					'date'   				=> $row->date,
				);
				array_push($newArray,$data);
			}
			
			return $newArray;
		}
		public function getWallStatusComments($id)
		{
			$newArray = array();
			$this->db->select('*');
			$this->db->where('wall_status_id', $id);
			$this->db->from('user_wall_comments');
			$this->db->order_by("comment_date", "desc");
			$query = $this->db->get();
			foreach ($query->result() as $row)
			{
				$getCommentUserData = $this->db->get_where('user_info', array('u_id' => $row->comment_user_id));
				$d = $getCommentUserData->result();
				if(count($d) > 0){
					$pic = "images/uploads/".$row->comment_user_id."/".$d[0]->prof_pic;
				}else{
					$pic = "images/user-pic.png";
				}
				$data = array (
					'id'   					=> $row->id,
					'wall_status_id'   		=> $row->wall_status_id,
					'wall_status_user_id'   => $row->wall_status_user_id,
					'comment_user_id'   	=> $row->comment_user_id,
					'comment_text'   		=> $row->comment_text,
					'pic'   				=> $pic,
					'fullname'   			=> ucwords($d[0]->u_fullname),
					'comment_date'   		=> $row->comment_date,
				);
				array_push($newArray,$data);
			}
			return $newArray;
		}
		public function getWallStatusCommentFromComments($id)
		{
			$newArray = array();
			$this->db->select('*');
			$this->db->where('comment_id', $id);
			$this->db->from('user_replyto_wall_comments');
			$this->db->order_by("comment_date", "desc");
			$query = $this->db->get();
			foreach ($query->result() as $row)
			{
				$getCommentUserData = $this->db->get_where('user_info', array('u_id' => $row->comment_user_id));
				$d = $getCommentUserData->result();
				if(count($d) > 0){
					$pic = "images/uploads/".$row->comment_user_id."/".$d[0]->prof_pic;
				}else{
					$pic = "images/user-pic.png";
				}
				$data = array (
					'id'   					=> $row->id,
					'comment_id'   			=> $row->comment_id,
					'wall_status_id'   		=> $row->wall_status_id,
					'wall_status_user_id'   => $row->wall_status_user_id,
					'comment_user_id'   	=> $row->comment_user_id,
					'comment_text'   		=> $row->comment_text,
					'pic'   				=> $pic,
					'fullname'   			=> ucwords($d[0]->u_fullname),
					'comment_date'   		=> $row->comment_date,
				);
				array_push($newArray,$data);
			}
			return $newArray;
		}

	}