<?php

	class m_linkpost extends MY_Model
	{
		protected $_table_name = 'user_link_post';
		protected $_order_by = 'u_id';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function clickedLink($id)
		{
			$this->db->select('clicks');
			$this->db->where('u_id', $id);
			$this->db->from('user_link_post');
			
			$query = $this->db->get();

			$clickCount = $query->result();
			$newClickCount = $clickCount[0]->clicks + 1;
			return $newClickCount;
		
		}
		public function getLinks()
		{
			$this->db->order_by("u_id", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 0));
			
			return $query->result();
		
		}
		public function getApproveLinks()
		{
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			return $query->result();
		
		}
		public function getAllApproveLinks()
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						$getCommentCount = $this->db->get_where('user_comments', array('link_id' => $row->u_id));
						$data = array (
							'u_id'   			=> $row->u_id,
							'user_id'   		=> $row->user_id,
							'warning'   		=> $row->warning,
							'category'   		=> $row->category,
							'description'   	=> $row->description,
							'link'   			=> $row->link,
							'link_title'   		=> $row->link_title,
							'post_icon'   		=> $row->post_icon,
							'publish_from'   	=> $row->publish_from,
							'publish_to'   		=> $row->publish_to,
							'publish_date_time' => $row->publish_date_time,
							'status'   			=> $row->status,
							'clicks'   			=> $row->clicks,
							'tags'   			=> $row->tags,
							'hearted_already'   => $countFave,
							'loggedin'   		=> $login,
							'comment_count'   	=> count($getCommentCount->result()),
							'u_username'   		=> $row2->u_username
						);
							
						array_push($newArray,$data);
					}
				}
			}
			
			return $newArray;
		
		}
		public function getAllApproveLinksNoWarning()
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0 && $row->warning == "null"){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						$getCommentCount = $this->db->get_where('user_comments', array('link_id' => $row->u_id));
						$data = array (
							'u_id'   			=> $row->u_id,
							'user_id'   		=> $row->user_id,
							'warning'   		=> $row->warning,
							'category'   		=> $row->category,
							'description'   	=> $row->description,
							'link'   			=> $row->link,
							'link_title'   		=> $row->link_title,
							'post_icon'   		=> $row->post_icon,
							'publish_from'   	=> $row->publish_from,
							'publish_to'   		=> $row->publish_to,
							'publish_date_time' => $row->publish_date_time,
							'status'   			=> $row->status,
							'clicks'   			=> $row->clicks,
							'tags'   			=> $row->tags,
							'hearted_already'   => $countFave,
							'loggedin'   		=> $login,
							'comment_count'   	=> count($getCommentCount->result()),
							'u_username'   		=> $row2->u_username
						);
							
						array_push($newArray,$data);
					}
				}
			}
			
			return $newArray;
		
		}
		public function getTenUnapproveLinks()
		{	
			
			$this->db->order_by("u_id", "desc");
			$this->db->limit(2);
			$query = $this->db->get_where('user_link_post', array('status' => 0));
			
			return $query->result();
		
		}
		public function getThisLink($id)
		{
			$this->db->select('*');
			$this->db->where('u_id', $id);
			$this->db->from('user_link_post');
			
			$query = $this->db->get();

			return $query->result();
		
		}
		
		public function viewLinks($category)
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						$str = strtoupper($category);
						$toString = str_replace("[","",$row->category);
						$toString = str_replace("]","",$toString);
						$check = strpos($toString,$str);
						
						if($check != null && $check > 0){
							$data = array (
								'u_id'   			=> $row->u_id,
								'user_id'   		=> $row->user_id,
								'warning'   		=> $row->warning,
								'category'   		=> $row->category,
								'description'   	=> $row->description,
								'link'   			=> $row->link,
								'link_title'   		=> $row->link_title,
								'post_icon'   		=> $row->post_icon,
								'publish_from'   	=> $row->publish_from,
								'publish_to'   		=> $row->publish_to,
								'publish_date_time' => $row->publish_date_time,
								'status'   			=> $row->status,
								'tags'   			=> $row->tags,
								'hearted_already'   => $countFave,
								'loggedin'   		=> $login,
								'u_username'   		=> $row2->u_username
							);
							
							array_push($newArray,$data);
						}
					}
				}
			}
			
			return $newArray;
		
		}
		public function viewLinksNoWarning($category)
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0 && $row->warning == "null"){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						$str = strtoupper($category);
						$toString = str_replace("[","",$row->category);
						$toString = str_replace("]","",$toString);
						$check = strpos($toString,$str);
						
						if($check != null && $check > 0){
							$getCommentCount = $this->db->get_where('user_comments', array('link_id' => $row->u_id));
							$data = array (
								'u_id'   			=> $row->u_id,
								'user_id'   		=> $row->user_id,
								'warning'   		=> $row->warning,
								'category'   		=> $row->category,
								'description'   	=> $row->description,
								'link'   			=> $row->link,
								'link_title'   		=> $row->link_title,
								'post_icon'   		=> $row->post_icon,
								'publish_from'   	=> $row->publish_from,
								'publish_to'   		=> $row->publish_to,
								'publish_date_time' => $row->publish_date_time,
								'status'   			=> $row->status,
								'tags'   			=> $row->tags,
								'hearted_already'   => $countFave,
								'loggedin'   		=> $login,
								'comment_count'   	=> count($getCommentCount->result()),
								'u_username'   		=> $row2->u_username
							);
							
							array_push($newArray,$data);
						}
					}
				}
			}
			
			return $newArray;
		
		}
		public function viewTagLinks($tag)
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						
						$check = strpos($row->tags,$tag);
						
						if($check != null && $check > 0){
							$data = array (
								'u_id'   			=> $row->u_id,
								'user_id'   		=> $row->user_id,
								'warning'   		=> $row->warning,
								'category'   		=> $row->category,
								'description'   	=> $row->description,
								'link'   			=> $row->link,
								'link_title'   		=> $row->link_title,
								'post_icon'   		=> $row->post_icon,
								'publish_from'   	=> $row->publish_from,
								'publish_to'   		=> $row->publish_to,
								'publish_date_time' => $row->publish_date_time,
								'status'   			=> $row->status,
								'tags'   			=> $row->tags,
								'hearted_already'   => $countFave,
								'loggedin'   		=> $login,
								'u_username'   		=> $row2->u_username
							);
							
							array_push($newArray,$data);
						}
					}
				}
			}
			
			return $newArray;
		
		}
		public function viewTagLinksNoWarning($tag)
		{
			$newArray = array();
			$countFave = 0;
			$login = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			
			foreach ($query->result() as $row)
			{
				$publish_date = $row->publish_date_time;
				$todays_date = date('Y-m-d H:i:s');
				
				$strPublish_date = strtotime($publish_date);
				$strTodays_date = strtotime($todays_date);
				
				$toPublish = $strTodays_date - $strPublish_date;

				if($toPublish >= 0 && $row->warning == "null"){
					$query2 = $this->db->get_where('user_info', array('u_id' => $row->user_id));
					
					if($this->session->userdata('loggedin') == TRUE){
						$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = ".$row->u_id." AND user_id = ".$this->session->userdata('u_id')."");

						if($getFavoriteLinks)
						{
							if($getFavoriteLinks->result() == null){
								$countFave = 0;
							}else{
								foreach ($getFavoriteLinks->result() as $linkH)
								{
									
									if($linkH->id == NULL ){
										$countFave = 0;
									}else{
										$countFave = 1;
									}
									
								}
							}
						}else{
								$countFave = 0;
						}
						$login = 1;
					}else{
						$countFave = 0;
						$login = 0;
					}
					foreach ($query2->result() as $row2)
					{	
						
						$check = strpos($row->tags,$tag);
						
						if($check != null && $check > 0){
							$getCommentCount = $this->db->get_where('user_comments', array('link_id' => $row->u_id));
							$data = array (
								'u_id'   			=> $row->u_id,
								'user_id'   		=> $row->user_id,
								'warning'   		=> $row->warning,
								'category'   		=> $row->category,
								'description'   	=> $row->description,
								'link'   			=> $row->link,
								'link_title'   		=> $row->link_title,
								'post_icon'   		=> $row->post_icon,
								'publish_from'   	=> $row->publish_from,
								'publish_to'   		=> $row->publish_to,
								'publish_date_time' => $row->publish_date_time,
								'status'   			=> $row->status,
								'tags'   			=> $row->tags,
								'hearted_already'   => $countFave,
								'loggedin'   		=> $login,
								'comment_count'   	=> count($getCommentCount->result()),
								'u_username'   		=> $row2->u_username
							);
							
							array_push($newArray,$data);
						}
					}
				}
			}
			
			return $newArray;
		
		}
	
	}