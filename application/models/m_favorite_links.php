<?php

	class m_favorite_links extends MY_Model
	{
		protected $_table_name = 'user_favorite_links';
		protected $_order_by = 'id';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function getAllFavored()
		{	

			$query = $this->db->get('user_favorite_links');
			
			return $query->result();
		
		}
		public function checkTheHeartedLink($data)
		{	

			$query = $this->db->get_where('user_favorite_links', $data);
			
			return $query->result();
		
		}
		public function getAllUserFavorites()
		{	
			$newArray = array();
			$this->db->order_by("id", "desc");
			$query = $this->db->get_where('user_favorite_links', array('user_id' => $this->session->userdata('u_id')));
			
			foreach ($query->result() as $row)
			{
				$query2 = $this->db->get_where('user_link_post', array('u_id' => $row->link_id));
				foreach ($query2->result() as $row2)
				{	
					$query3 = $this->db->get_where('user_info', array('u_id' => $row2->user_id));
					foreach ($query3->result() as $row3)
					{
						$data = array (
							'u_id'   			=> $row2->u_id,
							'user_id'   		=> $row2->user_id,
							'warning'   		=> $row2->warning,
							'category'   		=> $row2->category,
							'description'   	=> $row2->description,
							'link'   			=> $row2->link,
							'link_title'   		=> $row2->link_title,
							'post_icon'   		=> $row2->post_icon,
							'publish_from'   	=> $row2->publish_from,
							'publish_to'   		=> $row2->publish_to,
							'publish_date_time'	=> $row2->publish_date_time,
							'status'   			=> $row2->status,
							'tags'   			=> $row2->tags,
							'u_username'   		=> $row3->u_username
						);
						array_push($newArray,$data);
					}
				}
			}
			return $newArray;
		}
		public function mostLinkFavored($id)
		{	

			$query = $this->db->get_where('user_link_post', array('u_id' => $id));
			
			return $query->result();
		
		}
		public function testthis(){
			$newArray = array();
			$countFave = 0;
			$this->db->order_by("publish_date_time", "desc");
			$query = $this->db->get_where('user_link_post', array('status' => 1));
			//foreach ($query->result() as $row)
			//{
				//$this->db->select('id');
				//$this->db->where('link_id', 19);
				//$this->db->where('user_id', $this->session->userdata('u_id'));
				//$this->db->from('user_favorite_links');
				$getFavoriteLinks = $this->db->query("SELECT id FROM user_favorite_links WHERE link_id = 12 AND user_id = ".$this->session->userdata('u_id')."");
				
				if($getFavoriteLinks)
				{
					echo "222";
					foreach ($getFavoriteLinks->result() as $linkH)
					{
						if(($linkH->id) > 0 ){
							$countFave = 1;
						}else{
							$countFave = 0;
						}
					}
					
				}else{
					echo "333";
					$countFave = 0;
				}
				
				echo $countFave;

				//array_push($newArray,);
			//}
			//return $newArray;
		}
	}