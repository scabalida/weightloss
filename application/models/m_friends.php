<?php

	class m_friends extends MY_Model
	{
		protected $_table_name = 'user_friends_list';
		protected $_order_by = 'friend_date';
		
		public function __construct()
		{
			parent::__construct();
		}
		public function chckFriend($id)
		{
			
			$query = $this->db->get_where('user_friends_list', array('user_id' => $this->session->userdata('u_id'),'friend_id' => $id,'status' => 1));

			return $query->result();
		
		}
		public function searchUser($name)
		{
			$newArray = array();
			$this->db->like('u_fullname', $name);
			$this->db->where('u_rights', 1);
			//$this->db->limit(15);
			$query = $this->db->get('user_info');
			if(count($query->result()) > 0){
				foreach($query->result() as $row2){
					$chkFriend = $this->db->get_where('user_friends_list', array('user_id' => $this->session->userdata('u_id'),'friend_id' => $row2->u_id));
					if(count($chkFriend->result()) <= 0 && $row2->u_id != $this->session->userdata('u_id')){	
						
							$data = array(
									'u_id' => $row2->u_id,
									'u_username' => $row2->u_username,
									'u_fullname' => $row2->u_fullname,
									'u_bio' => $row2->u_bio,
									'u_email' => $row2->u_email,
									'u_bday' => $row2->u_bday,
									'fr_status' => 0,
									'u_points' => $row2->u_points,
									'prof_pic' => $row2->prof_pic,
									'baseURL' => base_url(),
									'u_role' => $row2->u_role
							);
							array_push($newArray,$data);
						
					}else if(count($chkFriend->result()) > 0 && $row2->u_id != $this->session->userdata('u_id')){
						foreach($chkFriend->result() as $row3){
							if($row3->status == 0){	
								$data = array(
										'u_id' => $row2->u_id,
										'u_username' => $row2->u_username,
										'u_fullname' => $row2->u_fullname,
										'u_bio' => $row2->u_bio,
										'u_email' => $row2->u_email,
										'u_bday' => $row2->u_bday,
										'fr_status' => 1,
										'u_points' => $row2->u_points,
										'prof_pic' => $row2->prof_pic,
										'baseURL' => base_url(),
										'u_role' => $row2->u_role
								);
								array_push($newArray,$data);
							}
						}
					}
				}
			}else{
				$newArray = array();
			}
			return $newArray;
		}
		public function getAllMyFriends()
		{
			$newArray = array();
			$query = $this->db->get_where('user_friends_list', array('user_id' => $this->session->userdata('u_id'), 'status' => 1));
			if(count($query->result()) > 0){
				foreach($query->result() as $row){
					$getFriendData = $this->db->get_where('user_info', array('u_id' => $row->friend_id));
					foreach($getFriendData->result() as $row2){
						$data = array(
							'u_id' => $row2->u_id,
							'u_username' => $row2->u_username,
							'u_fullname' => $row2->u_fullname,
							'u_bio' => $row2->u_bio,
							'u_email' => $row2->u_email,
							'u_bday' => $row2->u_bday,
							'u_points' => $row2->u_points,
							'prof_pic' => $row2->prof_pic,
							'baseURL' => base_url(),
							'u_role' => $row2->u_role
						);
						array_push($newArray,$data);
					}
				}
			}else{
				$newArray = array();
			}
			return $newArray;
		}
		public function getFriendRequest()
		{
			$newArray = array();
			$this->db->order_by("friend_date", "desc");
			$query = $this->db->get_where('user_friends_list', array('user_id' => $this->session->userdata('u_id'), 'status' => 0));
			if(count($query->result()) > 0 ){
				foreach($query->result() as $row){
					if($row->who_requested != $this->session->userdata('u_id')){
						$getFriendData = $this->db->get_where('user_info', array('u_id' => $row->who_requested));
						
						foreach($getFriendData->result() as $row2){
							$data = array(
								'u_id' => $row2->u_id,
								'u_username' => $row2->u_username,
								'u_fullname' => $row2->u_fullname,
								'u_bio' => $row2->u_bio,
								'u_email' => $row2->u_email,
								'u_bday' => $row2->u_bday,
								'u_points' => $row2->u_points,
								'prof_pic' => $row2->prof_pic,
								'baseURL' => base_url(),
								'u_role' => $row2->u_role
							);
							array_push($newArray,$data);
						}
					}
				}
			}
			return $newArray;
		}
		public function confirmFriendRequest($friend_id)
		{
			$data = array(
				'status' => 1
			);
			$query1 = $this->db->update('user_friends_list', $data, array('user_id' => $friend_id,'friend_id' => $this->session->userdata('u_id')));
			$query2 = $this->db->update('user_friends_list', $data, array('friend_id' => $friend_id,'user_id' => $this->session->userdata('u_id')));
			
			if($query1 && $query2){
				return true;
			}else{
				return false;
			}
		}
		public function deleteFriendRequest($friend_id)
		{
			$query1 = $this->db->delete('user_friends_list', array('user_id' => $friend_id,'friend_id' => $this->session->userdata('u_id')));
			$query2 = $this->db->delete('user_friends_list', array('friend_id' => $friend_id,'user_id' => $this->session->userdata('u_id')));
			
			if($query1 && $query2){
				return true;
			}else{
				return false;
			}
		}
		public function checkChatRoom($data)
		{
			$query = $this->db->get_where('user_chat_room', $data);
			
			return $query->result();
		
		}
		public function checkFriendRequest($id)
		{
			$query = $this->db->get_where('user_friends_list', array('user_id' => $this->session->userdata('u_id'),'friend_id' => $id));
			
			return $query->result();
		
		}
		
	}