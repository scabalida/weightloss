<?php

	class m_chat_room extends MY_Model
	{
		protected $_table_name = 'user_chat_room';
		protected $_order_by = 'date_created';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function getMyChatRooms()
		{
			$newArray = array();	
			$getChatRooms = $this->db->get_where('user_chat_room', array('user_id' => $this->session->userdata('u_id')));
			
			foreach ($getChatRooms->result() as $row1)
			{
				$getMyChatMateName = $this->db->get_where('user_info', array('u_id' => $row1->your_chat_user_id));
				foreach ($getMyChatMateName->result() as $row2)
				{
					$getChatid = $this->db->get_where('user_chat_room', array('user_id' => $row1->your_chat_user_id));
						$data = array (
							'u_id'   					=> $row1->u_id,
							'room_user_ID'   			=> $getChatid->result()[0]->u_id,
							'user_id'   				=> $row1->user_id,
							'your_chat_user_id'   		=> $row1->your_chat_user_id,
							'date_created'   			=> $row1->date_created,
							'chat_seen'   				=> $row1->chat_seen,
							'my_fullname'   			=> ucwords($this->session->userdata('u_fullname')),
							'my_chatmate_fullname'   	=> ucwords($row2->u_fullname),
							'my_chatmate_profpic'   	=> $row2->prof_pic,
							'baseURL'   				=> base_url(),
						);
							array_push($newArray,$data);
				}
			}
			
			return $newArray;
		}
		
		public function updateChatSeen($data,$id){

			$this->db->where('u_id', $id);
			$query = $this->db->update('user_chat_room', $data);
			
			if($query){
				return true;
			}else{
				return false;
			}
			
		}
	}