
<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#weightLossTable_filter{float:right}
	#weightLossTable .sorting_disabled::before,
	#weightLossTable .sorting_disabled::after{display:none}
	#userWallStatusTable .sorting_disabled::before,
	#userWallStatusTable .sorting_disabled::after{display:none}
	#userWallStatusTable thead{display:none}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="invalid-feedback" id="editProfError"><h5 class="text-center">User Profile unsuccessfully updated.</h5></div>
			<div class="valid-feedback" id="editProfSuccess"><h5 class="text-center">User Profile successfully updated.</h5></div>
			<input type="hidden" id="base_url" value="<?php echo base_url();?>">
			<div id="myProf" class="content-holder">
				<button type="button" id="clickEditProf" class="btn btn-dark btn-md pull-right">EDIT PROFILE</button>
				<div class="row">
					
					<div class="col-xl-1 col-md-1"></div>
					<div class="col-xl-5 col-md-5">
						<h4>My Profile</h4>
						<br>
						<h3>User Name</h3>
						<h5 id="myusername"></h5><span id="mypoints"></span>
						<br>
						<h3>Full Name</h3>
						<h5 id="myfullname"></h5>
						<br>
						<h3>Date of Birth</h3>
						<h5 id="mybday"></h5>
						<br>
						<h3>Email Address</h3>
						<h5 id="myaddress"></h5>
						<br>
					</div>
					<div class="col-xl-5 col-md-5">
						<br>
						<br>
						<center>
							<img src="<?php echo base_url();?>images/user-pic.png" id="myImgProfile" style="max-width:200px; width:100%; height:auto;">	
						</center>
						<br>
						<h3>Biography</h3>
						<h3 class="text-justify" id="mybio"></h3>
					</div>
					<div class="col-xl-1 col-md-1"></div>
				</div>
			</div>
			<div id="editProf" class="content-holder" style="display:none">
				<button type="button" id="clickMyProf" class="btn btn-dark btn-md pull-right">MY PROFILE</button>
				<h4>Edit Profile</h4>
					<form class="needs-validation-editprofile" id="editUserProfileForm" role="form" method="POST" novalidate="">
						<div class="row">
                            <div class="col-xl-6 col-md-6">
								<br>
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editfullname">Full Name</label>
                                    </div>    
                                    <input type="text" class="form-control" name="editfullname" id="editfullname" required>
									<div class="invalid-feedback">Please provide a name.</div>
                                </div>
								<br>
								<div class="form-group">
									<label for="editbday" class="control-label">Date of Birth</label>
									<input type="date" class="form-control" name="editbday" id="editbday" required="">
									<div class="invalid-feedback">Please provide your birthday.</div>
								</div>
								<br>
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editemail">Email Address</label>
                                    </div>    
                                    <input type="email" class="form-control" name="editemail" id="editemail" required>
									<div class="invalid-feedback">Please provide a valid email.</div>
                                </div>
							</div>
							<div class="col-xl-6 col-md-6">
								<center>
								<img src="<?php echo base_url();?>images/user-pic.png" id="dispImgProfile" style="max-width:150px; width:100%; height:auto;">
								
								<div class="form-group">
									<br/>
									<label for="imgfile" class="control-label">Profile Picture</label>
									<input type="file" name="imgfile" id="imgfile">
								</div>
								</center>
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editbio">Biography</label>
                                    </div>    
                                    <textarea type="text" class="form-control" name="editbio" id="editbio" rows="3" required></textarea>
									<div class="invalid-feedback">Please provide your biography.</div>
                                </div>
							</div>
						</div>
						<div class="row btn-area">
                            <div class="col-xl-6">
                               <button id="btnEditProfile" class="btn btn-dark" type="submit">UPPDATERA NU</button>
                            </div>
                        </div>
					</form>
			</div>
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
				  <h5><a class="nav-link active" data-toggle="tab" id="progBtn" href="#menu1">Weight Loss Progress</a></h5>
				</li>
				<li class="nav-item">
					<h5><a class="nav-link" data-toggle="tab" id="diaryBtn" href="#menu2">Diary</a></h5>
				</li>
			</ul>
			<div class="tab-content">
				
				<div id="menu1" class="container tab-pane active"><br>
								<div class="valid-feedback" id="addWeightLossSuccess"><h5 class="text-center">Successfully added weight.</h5></div>
								<div class="valid-feedback" id="addWeightLossError"><h5 class="text-center">Unsuccessfully added weight.</h5></div>
								<div class="container">
									<div class="row">
										<div class="col-xl-8 col-md-8"></div>
										<div class="col-xl-4 col-md-4">
											<form id="addWeightLossTday" method="post">
												<div class="input-group mb-3">
												  <input type="text" class="form-control" placeholder="Weight Today(lbs)" id="weightloss" aria-describedby="basic-addon2">
												  <div class="input-group-append">
													<button class="btn btn-info" type="submit" style="border-radius: 0px 4px 4px 0px;">SAVE</button>
												  </div>
												</div>
												<div class="invalid-feedback" style="    padding-bottom: 2px;">Please provide your weight today.</div>
											</form>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-12 col-md-12">
											<div id="chartContainer" style="height: 370px; width: 100%;"></div>
											<div style="display:none" id="noWeightLossData" class="alert alert-light" role="alert">
												<h4>No data to be displayed.</h4>
											</div>
										</div>
									</div>
								</div>
								
								<div class="content-holder" id="weightLossDataContainer">
									<div class="valid-feedback" id="editWeightLossSuccess"><h5 class="text-center">Successfully updated weight.</h5></div>
									<div class="valid-feedback" id="editWeightLossError"><h5 class="text-center">Unsuccessfully updated weight.</h5></div>
									<h4 class="text-center">Weight Loss Data List</h4>
									<div class="row">
										<div class="col-xl-1 col-md-1 col-sm-1"></div>
										<div class="col-xl-10 col-md-10 col-sm-10">
											<h5 class="text-center">Total Weight Loss: <span id="totalWeightLoss" style="color:#007bff"></span></h5>
											<table id="weightLossTable" class="table table-striped table-bordered" style="width:100%;display:none">
												<thead>
													<tr>
														<th scope="col text-center">Date</th>
														<th scope="col text-center">Weight (lbs)</th>
														<th scope="col text-center" style="text-align:center">Edit</th>
													</tr>
												</thead>
												<tbody id="tbodyWeightLossTable">
													
												</tbody>
											</table>
										</div>
										<div class="col-xl-1 col-md-1 col-sm-1"></div>
									</div>
									<div style="display:none" id="noWeightLossTable" class="alert alert-light" role="alert">
										<h4>No data to be displayed.</h4>
									</div>
								</div>
				</div>
				<div id="menu2" class="container tab-pane fade"><br>
								<div class="content-holder" id="userWallStatus">
								<h5><strong>Add a Post</strong></h5>
								<form id="addWallStatusForm" method="post">
									<div class="row">
										<div class="col-xl-11 col-md-11 col-sm-11">
											<textarea id="status_description" name="status_description" class="form-control" rows="2" placeholder="Vad tänker du på?"></textarea>
										</div>
										<div class="col-xl-1 col-md-1 col-sm-1">
											<button type="submit" class="btn btn-dark btn-md pull-right">POSTA</button>
										</div>
									</div>
								</form>
							</div>
							<div class="content-holder">
								<h5><strong>My Post</strong></h5>
								<div class="row">
									<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12">
										<table id="userWallStatusTable" class="table" style="width:100%;display:none">
											<thead>
												<tr>
													<th scope="col text-center"></th>
												</tr>
											</thead>
											<tbody id="tbodyuserWallStatusTable">
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
				</div>
			</div>
		</div>
		
	</main>   
</div>  
<div class="modal fade" id="editWeightLossModal" tabindex="-1" role="dialog" aria-labelledby="deleteApprovePostLinkModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
		<h4 class="modal-title" id="exampleModalLabel">Update Weight Loss</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editWeightLossForm" role="form" method="POST">
			<div class="form-group">
				<label for="editWeightloss" class="control-label">Weight Loss</label>
				<input type="text" class="form-control" name="editWeightloss" id="editWeightloss" required>
				<input type="hidden" name="weightID" id="weightID">
				<div class="invalid-feedback">Please provide your weight loss.</div>
			  </div>
		
      </div>
      <div class="modal-footer">
		<button type="submit" class="btn btn-success">SAVE</button>
		</form>
      </div>
    </div>
  </div>
</div>
    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
	<script src="<?php echo base_url();?>js/canvasjs.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->  
    <script src="<?php echo base_url();?>js/profile.js"></script> 
	<script src="<?php echo base_url();?>js/script.js"></script> 
	<script>
	$(document).ready(function(){
		$("#progBtn").click(function(){
			$("#menu1").show();
			$("#menu2").hide();
		});
		$("#diaryBtn").click(function(){
			$("#menu1").hide();
			$("#menu2").show();
		});
	});
	</script>
</body>
</html>