<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#WallStatusTable .sorting_disabled::before,
	#WallStatusTable .sorting_disabled::after{display:none}
	#WallStatusTable thead{display:none}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="invalid-feedback" id="editProfError"><h5 class="text-center">User Profile unsuccessfully updated.</h5></div>
			<div class="valid-feedback" id="editProfSuccess"><h5 class="text-center">User Profile successfully updated</h5></div>
			<div id="myProf" class="content-holder">
				 <input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
				 <input type="hidden" name="baseURL" id="baseURL" value="<?php echo base_url();?>">
				<div class="row">
					<div class="col-xl-3 col-md-3">
					
							<img id="myImgProfile" style="margin: 20px auto;width:100%; height:auto;    max-width: 200px;">	
						
					</div>
					<div class="col-xl-7 col-md-7">
						<br>
						<h3>User Name</h3>
						<h5 id="myusername"></h5><span id="mypoints"></span>
						<br>
						<h3>Full Name</h3>
						<h5 id="usrFullName"></h5>
						<br>
						<h3>Biography</h3>
						<h3 class="text-justify" id="mybio"></h3>
					</div>
					<div class="col-xl-2 col-md-2">
						<button type="button" id="btnAdd"></button>
					</div>
				</div>
			</div>
			<?php if($this->session->userdata('loggedin') == TRUE && $chckFriend > 0){ ?>
			<div class="content-holder">
				<h5><strong>My Post</strong></h5>
				<div class="row">
					<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12">
						<table id="WallStatusTable" class="table" style="width:100%;display:none">
							<thead>
								<tr>
									<th scope="col text-center"></th>
								</tr>
							</thead>
							<tbody id="tbodyWallStatusTable">
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		
	</main>   
</div>  

    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->  
	<script src="<?php echo base_url();?>js/script.js"></script> 
    <script src="<?php echo base_url();?>js/notif.js"></script> 
    <script src="<?php echo base_url();?>js/chat.js"></script> 
	<script>
		 var WallStatusTable = $('#WallStatusTable').DataTable();
		(function($){
			getUserData();
			checkFriendRequest();
			getWallStatus();
		})(jQuery);
		function getUserData(){
			var id = $("#user_id").val();
			var baseURL = $("#baseURL").val();
			$.ajax({
					type: "POST",
					url: "../../profile/getUserData",
					dataType: "json",
					data:{id:id},
					success:
						function(data) {
							//console.log(data[0].prof_pic);
							var dbDate = new Date(data[0].u_bday);
							var yyyy = dbDate.getFullYear();
							var mm = dbDate.getMonth() + 1;
							var dd = dbDate.getDate();
							var dispDate = getMonthDesc(mm)+" "+minTwoDigits(dd)+", "+yyyy;
							if(data[0].prof_pic == ""){
								$("#myImgProfile").attr("src",baseURL+"images/user-pic.png");
							}else{
								$("#myImgProfile").attr("src",baseURL+"images/uploads/"+data[0].u_id+"/"+data[0].prof_pic);
							}
							$("#usrFullName").html(data[0].u_fullname);
							
							//$("#myaddress").html(data[0].u_email);
							$("#mybio").html(data[0].u_bio);
							//$("#mybday").html(dispDate);
							if(data[0].u_points == ""){
									$("#myusername").html(data[0].u_username+" (0)");
							}else{
								$("#myusername").html(data[0].u_username+" ("+data[0].u_points+")");
							}
							
						},
					error:
					function(data){
						//console.log("false");		
					}
			});
		}
		function getWallStatus(){
			WallStatusTable.destroy();
			var base_url = $("#baseURL").val();
			var id = $("#user_id").val();
		   $.ajax({
				type: "POST",
				url: "../../profile/getFriendsWallStatus",
				dataType: "json",
				data:{id:id},
				success:
					function(data) {
						if(data.length > 0){
							$("#tbodyWallStatusTable").empty();
							for(var x=0;x < data.length;x++){
								$("#WallStatusTable").find('tbody')
									.append($('<tr>')
										.append($('<td>')
											.append($('<div>')
												.attr('class', "alert alert-secondary")
												.attr('role', "alert")
												.append($('<div>')
													.attr('class', "row")
													.append($('<div>')
														.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
														.append($('<h5>')
															.append(data[x].status_description)
															.append($('<br>'))
															.append($('<br>'))
															.append("<span style='font-size: 12px;'>"+data[x].date+"&ensp;-&ensp;Current Weight: "+data[x].current_weight+" lbs</span>")
														)
														.append($('<span>')
															.append("&ensp;&ensp;<i onclick=replyToThisWallStatus("+data[x].id+","+data[x].user_id+") class='fa fa-comment' style='font-size:15px;cursor:pointer;margin-left: -10px;'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Comment</span></i>")
														)
														.append($('<span>')
															.append(chckComments(data[x].numComments,data[x].id))
														)
	
													)
													.append($('<div>')
														.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
														.attr('style', "padding: 0 0 0 10px")
														.append($('<img>')
															.attr('style', "max-width: 65px;border-radius: 50%;")
															.attr('src', base_url+data[x].prof_pic)
														)
													)
													.append($('<div>')
														.attr('id', "txtArea"+data[x].id)
														.attr('style', "width: 95%;padding-top: 8px;margin-left: 4%;display:none")
														.append($('<div>')
															.attr('class', "input-group mb-3")
															.append("<span onclick=hideReplyComment("+data[x].id+") style='margin-top: -5px;padding-right: 10px;font-size: 36px;font-weight: bold;cursor:pointer'>-</span>")
															.append($('<textarea>')
																.attr('class', "form-control")
																.attr('rows', "1")
																.attr('id', "replyToComment"+data[x].id)
																.attr('placeholder', "Reply To This Comment")
															)
															.append($('<div>')
																.attr('class', "input-group-append")
																.append($('<button>')
																	.attr('class', "btn btn-dark")
																	.attr('onclick', "saveReplyTo("+data[x].id+","+data[x].user_id+")")
																	.attr('type', "button")
																		.append("KOMMENTERA")
																)
															)
														)
													)
												)
											)
										)
									);
							}
							$("#WallStatusTable").show();
							WallStatusTable = $('#WallStatusTable').DataTable({
								  "searching": false,
								  "lengthChange": false,
								  "ordering": false,
								  "pageLength": 10
								});
						}else{
							$("#WallStatusTable").hide();
						}
					},
				error:
				function(data){
					console.log("false");		
				}
			});		
		}
		function chckComments(num,id){
			if(num > 0){
				return "<span style='font-size:11px;color:#007bff;cursor:pointer' id='viewCommentsArea"+id+"' onclick='viewComments("+id+")'>&emsp;View Comments ("+num+")</span><div id='showComments"+id+"' style='display:none;background:#fff;padding-bottom: 10px;margin-top: 10px;'></div>";
			}
			
		}
		function viewComments(id){
			//console.log(id);
			var base_url = $("#baseURL").val();
			$.ajax({
					type: "POST",
					url: "../../profile/getWallStatusComments",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							console.log(data);
							for(var x=0;x < data.length;x++){
								$("#showComments"+id)
								.append($('<table style="display:block;padding-left: 15px;">')
									.append($('<tr>')
										.append($('<td>')
											.append($('<img>')
												.attr('style', "max-width: 35px;border-radius: 50%;")
												.attr('src', base_url+data[x].pic)
											)
										)
										.append($('<td>')
											.attr('style', "width: 100%;")
											.append($('<p>')
												
												.append("<strong style='color:#007bff;'>"+data[x].fullname+"</strong>")
												.append("&emsp;"+data[x].comment_text+"")
												.append($('<br>'))
												.append($('<br>'))
												.append("<span onclick=replyToWallComment("+data[x].id+","+data[x].wall_status_id+","+data[x].wall_status_user_id+") style='font-family: Open Sans, sans-serif;cursor:pointer'><i class='fa fa-comment'></i>&nbsp;Reply</span>")
												.append("<span style='color: rgb(0, 123, 255);cursor: pointer;' onclick=viewReplies('"+data[x].id+"')>&emsp;View Replies</span>")
												.append("<i style='float: right;'>"+data[x].comment_date+"</i>")
											)
											.append($('<div>')
												.attr('id', "showCommentsToWall"+data[x].id)
												.attr('style', "display:none;background:#fff;    margin-top: 10px;")
											)
											.append($('<div>')
														.attr('id', "txtAreaComment"+data[x].id)
														.attr('style', "width: 95%;padding-top: 8px;margin-left: 4%;display:none")
														.append($('<div>')
															.attr('class', "input-group mb-3")
															.append("<span onclick=hideReplyWallComment("+data[x].id+") style='margin-top: -5px;padding-right: 10px;font-size: 36px;font-weight: bold;cursor:pointer'>-</span>")
															.append($('<textarea>')
																.attr('class', "form-control")
																.attr('rows', "1")
																.attr('id', "wallReplyToComment"+data[x].id)
																.attr('placeholder', "Reply To This Comment")
															)
															.append($('<div>')
																.attr('class', "input-group-append")
																.append($('<button>')
																	.attr('class', "btn btn-dark")
																	.attr('onclick', "saveReplyToWallComment()")
																	.attr('type', "button")
																		.append("KOMMENTERA")
																)
															)
														)
													)
										)
										
											
									)
								);

								
							}
							$("#showComments"+id).show();
							$("#viewCommentsArea"+id).hide();
						},
					error:
						function(data){	
					}
				})
		}
		function saveReplyTo(wall_status_id,wall_status_user_id){
			var comment = $("#replyToComment"+wall_status_id).val();
			console.log(comment);
			if(comment != undefined && comment != ""){
				$.ajax({
						type: "POST",
						url: "../../profile/addWallStatusComment",
						dataType: "json",
						data:  {wall_status_id:wall_status_id,wall_status_user_id:wall_status_user_id,comment:comment},
						success:
							function(data) {
								getWallStatus();
							},
						error:
						function(data){	
						}
				});
			}
		}
		function replyToThisWallStatus(id,user_id){

			$("#txtArea"+id).show();
			
		}
		function replyToWallComment(id,wall_status_id,wall_status_user_id){

		$("#txtAreaComment"+id).show();
		wall_comment_id = id;
		wall_id = wall_status_id;
		wall_user_id = wall_status_user_id;
				
	}
	function hideReplyWallComment(id){
		$("#txtAreaComment"+id).hide();
	}
	function saveReplyToWallComment(){
			var comment = $("#wallReplyToComment"+wall_comment_id).val();
			console.log(wall_comment_id);
			console.log(wall_id);
			console.log(wall_user_id);
			console.log(comment);
			if(comment != undefined && comment != ""){
				
				$.ajax({
						type: "POST",
						url: "../../profile/addCommentToWallStatusComment",
						dataType: "json",
						data:  {comment_id:wall_comment_id,wall_status_id:wall_id,wall_status_user_id:wall_user_id,comment:comment},
						success:
							function(data) {
								$("#showComments"+wall_id).empty();
								viewComments(wall_id);
							},
						error:
						function(data){	
						}
				});
			}
	}
		function viewReplies(id){
			//console.log(id);
			var base_url = $("#baseURL").val();
			$.ajax({
					type: "POST",
					url: "../../profile/getWallStatusCommentFromComments",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							console.log(data);
							$("#showCommentsToWall"+id).empty();
							for(var x=0;x < data.length;x++){
								
								$("#showCommentsToWall"+id)
								.append($('<table style="display:block;padding-left: 15px;">')
									.append($('<tr>')
										.append($('<td>')
											.append($('<img>')
												.attr('style', "max-width: 35px;border-radius: 50%;")
												.attr('src', base_url+data[x].pic)
											)
										)
										.append($('<td>')
											.append($('<p>')
												.attr('style', "margin-bottom: 0;")
												.append("<strong style='color:#007bff;'>"+data[x].fullname+"</strong>")
												.append("&emsp;"+data[x].comment_text+"")
												.append($('<br>'))
												.append($('<br>'))
												.append("<i>"+data[x].comment_date+"</i>")
											)
										)
									)
								);
							}
							$("#showCommentsToWall"+id).show();
							
						},
					error:
						function(data){	
					}
				})
		}
		function hideReplyComment(id){
			$("#txtArea"+id).hide();
		}
		function minTwoDigits(n) {
		  return (n < 10 ? '0' : '') + n;
		}
		function getMonthDesc(m){
			var mDesc = "";
			switch (m){
				case 1:
				mDesc = "January";
				break;
				case 2:
				mDesc = "February";
				break;
				case 3:
				mDesc = "March";
				break;
				case 4:
				mDesc = "April";
				break;
				case 5:
				mDesc = "May";
				break;
				case 6:
				mDesc = "June";
				break;
				case 7:
				mDesc = "July";
				break;
				case 8:
				mDesc = "August";
				break;
				case 9:
				mDesc = "September";
				break;
				case 10:
				mDesc = "October";
				break;
				case 11:
				mDesc = "November";
				break;
				case 12:
				mDesc = "December";
				break;
			}
			return mDesc;
		}
		function checkFriendRequest(){
			var id = $("#user_id").val();
			$.ajax({
					type: "POST",
					url: "../../friends/checkFriendRequest",
					dataType: "json",
					data:{id:id},
					success:
						function(data) {
							//console.log(data);
							if(data.length > 0){
								if(data[0].status == 0){
									$("#btnAdd").html("REQUEST SENT");
									$("#btnAdd").prop('disabled', true);
									$("#btnAdd").attr('class', "btn btn-warning btn-md pull-right");
								}else{
									$("#btnAdd").html("FRIENDS");
									$("#btnAdd").prop('disabled', true);
									$("#btnAdd").attr('class', "btn btn-primary btn-md pull-right");
								}
							}else{
								$("#btnAdd").html("ADD AS FRIEND");
								$("#btnAdd").attr("onclick","addAsFriend("+id+")");
								$("#btnAdd").attr('class', "btn btn-dark btn-md pull-right");
							}
							
						},
					error:
					function(data){
						//console.log("false");		
					}
			});
		}
		function addAsFriend(id){
			//console.log(id);
			$.ajax({
					type: "POST",
					url: "../../friends/addFriendUser",
					dataType: "json",
					data:{id:id},
					success:
						function(data) {
							checkFriendRequest();
						},
					error:
					function(data){
						//console.log("false");		
					}
			});
		}
	</script>
</body>
</html>