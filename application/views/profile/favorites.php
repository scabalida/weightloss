<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#linkTable_filter{float:right}
	.width25{width:20px !important;}
	#linkTable tbody tr td button i.fa{font-size:18px}
	#linkTable tbody tr td{vertical-align:middle;word-break: break-all;}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="valid-feedback deleteMessageSucc"><h5 class="text-center">User successfully deleted.</h5></div>
			<div class="invalid-feedback deleteMessageError"><h5 class="text-center">User unsuccessfully deleted.</h5></div>
			<div class="valid-feedback" id="userEditSuccess"><h5 class="text-center">User has been updated.</h5></div>
			<div class="invalid-feedback" id="userEditError"><h5 class="text-center">Error in updating the user.</h5></div>
			  
			<div class="content-holder">
				<div class="upcoming-links"> 
					<table id="allLinksTable" class="table table-striped table-bordered" style="width:100%;display:none;">
							<thead>
								<tr>
									<th scope="col"></th>
								</tr>
							</thead>
							<tbody id="tbodyallLinksTable">
								
							</tbody>
					</table>
					<div style="display:none" id="nofavoritelink" class="alert alert-light" role="alert">
						<h4>No data to be displayed.</h4>
					</div>
				</div>
			</div>
		</div>
	</main>   
</div>  

    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/favorites.js"></script>    
	<script src="<?php echo base_url();?>js/script.js"></script> 


</body>
</html>