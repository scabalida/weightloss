	<div class="container" id="iframe-container" style="display:none;padding:25px;background-color:#fff;">
		<input type="hidden" id="link_id" value="<?php echo $link_id;?>">
		<input type="hidden" id="base_url" value="<?php echo base_url();?>">
		<input type="hidden" id="baseURL" value="<?php echo base_url();?>">
		<iframe id="iframeSRC" src="" frameborder="0" target="_top" allow="encrypted-media" allowfullscreen  style="width:100%;height:600px;"></iframe>
		<br>
		<br>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3 class="h3"><strong><span id="dispLinkTitle"></span></strong></h3>
				<h6 class="h6"><strong>Kommentarer</strong></h6>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<p class="pull-right"><strong><span id="cntComments"></span> kommentarer</strong></p>
			</div>
		</div>
		<input type="hidden" id="sessLoggedIn" value="<?php echo $this->session->userdata('loggedin'); ?>">
		<div id="comment-container">
			<?php if($this->session->userdata('loggedin') == TRUE){ ?>
			<form role="form" method="post" id="addCommentToLink">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							
							<textarea id="comment-text" class="form-control" rows="2" placeholder="Skriv en kommentar"></textarea>
							<input type="hidden" id="comment_link_id">
							<input type="hidden" id="comment_link_user_id">
						</div>
						<div class="invalid-feedback" id="noComment"><p class="text-center">Please write a comment.</p></div>
						<div class="invalid-feedback" id="failComment"><p class="text-center">Fail to add a comment.</p></div>
						<div class="valid-feedback" id="succComment"><p class="text-center">Successful in adding a comment.</p></div>
					</div>
				</div>
				<?php } ?>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<p class="text-left" id="commentNumberOfLikes"><i class="fa fa-heart"></i><span id="numberOfLikes"></span></p>
					</div>
					<?php if($this->session->userdata('loggedin') == TRUE){ ?>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<button type="submit" class="btn btn-dark btn-md pull-right">KOMMENTERA</button>
					</div>
					<?php } ?>
				</div>
			
			</form>
			<div class="upcoming-links"> 
				<table id="allCommentsTable" class="table table-striped table-bordered" style="width:100%;">
					<thead>
						<tr>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody id="tbodyAllCommentsTable">
							
					</tbody>
				</table>
				<div style="display:none" id="noAllComments" class="alert alert-light" role="alert">
					<h4>No comment/s.</h4>
				</div>
			</div> 
				
		</div>
	
	</div>
    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
	    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/user.js"></script>  
    <script src="<?php echo base_url();?>js/script.js"></script> 

	<script>
		var addChk = 0;
		var allLinksDataTable = $('#allLinksTable').DataTable();
		 var getCommentLinkDataTable = $('#allCommentsTable').DataTable();
		(function() {
			
			targetToIframe();
			getTheCommentsOfLink();

			countNotReadNotif();
			setInterval(function(){ 
				//getMyNotif();
				countNotReadNotif();
				countFR();
			}, 2000);
			$("#dropdownFRLink").click(function(){
				getMyFriendRequest();
			});
			$("#dropdownNotifLink").click(function(){
				getMyNotif();
			   $.ajax({
					type: "POST",
					url: "../../user/makeNotifRead",
					dataType: "json",
					success:
						function(data) {
							if(data == "true"){
								$("#countNotifs").html(0);
								$("#countNotifs").hide();
							}
						},
					error:
					function(data){
						//console.log("false");		
					}
				});
			});
			$("#addCommentToLink").submit(function(e){
				e.preventDefault();
				var comment = $("#comment-text").val();
				var link_id = $("#comment_link_id").val();
				var link_user_id = $("#comment_link_user_id").val();
				if(comment == "" || comment == undefined){
					$("#noComment").show();
					$("#failComment").hide();
					$("#succComment").hide();
				}else{
					$("#noComment").hide();
					$.ajax({
						type: "POST",
						url: "../../comments/addCommentToLink",
						dataType: "json",
						data:  {link_id:link_id,link_user_id:link_user_id,comment:comment},
						success:
							function(data) {
								if(data == "true"){
									$("#succComment").show();
									addChk = 1;
									getTheCommentsOfLink();
									setTimeout(function(){ $("#succComment").hide(); }, 2000);
								}else{
									$("#failComment").show();
									setTimeout(function(){$("#failComment").hide(); }, 2000);
								}
								
							},
						error:
						function(data){
							$("#failComment").show();
							setTimeout(function(){$("#failComment").hide(); }, 2000);		
						}
					});
				}
				
			});
		})();	
		function countFR(){
			$.ajax({
					type: "GET",
					url: "../../friends/getFriendRequest",
					dataType: "json",
					success:
						function(data) {
							if(data.length > 0){
								$("#countFriendRequest").show();
								$("#countFriendRequest").html(data.length);
							}else{
								$("#countFriendRequest").hide();
							}
							
						},
					error:
					function(data){
						//console.log(data);		
					}
				});
		}
		function countNotReadNotif(){
			$.ajax({
					type: "GET",
					url: "../../user/countNotReadNotif",
					dataType: "json",
					success:
						function(data) {
							if(data > 0){
								$("#countNotifs").show();
								$("#countNotifs").html(data);
							}else{
								$("#countNotifs").hide();
							}
							
						},
					error:
					function(data){
						//console.log(data);		
					}
				});
		}
		function getThePic(profpic,type,baseURL,id){
			if(type == 1 || type == 2 || type == 4 || type == 5){
				if(profpic == ""){
					return baseURL+"images/user-pic.png";
				}else{
					return baseURL+"images/uploads/"+id+"/"+profpic;
				}
			}else{
				if(profpic == ""){
					return baseURL+"images/user-pic.png";
				}else{
					return baseURL+"images/balloon.jpg";
				}
			}
		}
		function getTheNotifText(type,fullname,notif_text,link_name){
			
			if(type == 1 || type == 2){
				return "<strong>"+fullname+"</strong> "+notif_text+" <strong>"+link_name+"</strong>";
			}else if(type == 4){
				return "<strong>"+fullname+"</strong> "+notif_text+" in <strong>"+link_name+"</strong>";
			}else if(type == 5){
				return "<strong>"+fullname+"</strong> "+notif_text+" in <strong>"+link_name+"</strong>";
			}else if(type == 3){
				return "<strong>"+notif_text+"</strong>";
			}
		}
		function getMyNotif(){
			$.ajax({
					type: "GET",
					url: "../../user/getMyNotifs",
					dataType: "json",
					success:
						function(data) {
							//console.log(data);
							if(data.length > 0){
								$("#dropdown-text").empty();
								for(var x=0;x < data.length;x++){
									$("#dropdown-text")
										.append($('<a>')
											.attr('class', "dropdown-item")
											.attr('href', "#")
											.append($('<div>')
												.attr('class', "row")
												.append($('<div>')
													.attr('class', "col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3")
													.append($('<img>')
															.attr('src', getThePic(data[x].prof_pic,data[x].notif_type,data[x].baseURL,data[x].notif_user_id))
													)
												)
												.append($('<div>')
													.attr('class', "col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9")
													.append($('<p>')
														.attr('style', "font-size:14px;margin-bottom: 5px;margin-top: 5px;")
														.append(getTheNotifText(data[x].notif_type,data[x].u_fullname,data[x].notif_text,data[x].link_name))
													)
													.append($('<p>')
														.append(data[x].notif_date)
													)
												)
												
											)
										);
								}
							}else{
								$("#dropdown-text").empty();
								$("#dropdown-text")
										.append($('<div>')
											.attr('class', "alert alert-light")
											.attr('role', "alert")
											.attr('style', "width: 250px;")
											.append($('<h5>')
												.append("No notification/s.")
											)
										);
							}	
						},
					error:
					function(data){
						//console.log(data);		
					}
				});
		}
		function targetToIframe(){
			var id = $("#link_id").val();
			$.ajax({
					type: "POST",
					url: "../../user/getThisLink",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							//console.log(data);
							//console.log(data[0].link_title);
							var link = data[0].link;
							var chkYoutube = link.indexOf("watch?v=");
							var newLink = "";
							if(chkYoutube > 0){
								newLink = link.replace("watch?v=", "embed/");
							}else{
								newLink = link;
							}
							document.getElementById('iframeSRC').src=newLink;
							$("#dispLinkTitle").html(data[0].link_title);
							$("#comment_link_id").val(data[0].u_id);
							
							$("#comment_link_user_id").val(data[0].user_id);
							$("#home-container").hide();
							$("#iframe-container").show();
						},
					error:
					function(data){
						//console.log("false");		
					}
				});
		}
		function getTheCommentsOfLink(){
			var id = $("#link_id").val();
			var baseURL = $("#baseURL").val();
			getCommentLinkDataTable.destroy();
		   $.ajax({
				type: "POST",
				url: "../../comments/getLinksComment",
				data:  {link_id:id},
				dataType: "json",
				success:
					function(data) {
						//console.log(data);
						if(data.length > 0){
							$("#tbodyAllCommentsTable").empty();
							for(var x=0;x < data.length;x++){
								//console.log(x);
								var wlh = $("#base_url").val();
								var myP = 0;
								var link = "";
								if(data[x].comment_user_prof_pic == ""){
									link = wlh+"images/user-pic.png";
								}else{
									link = wlh+"images/uploads/"+data[x].comment_user_id+"/"+data[x].comment_user_prof_pic;
								}
								if(data[x].comment_user_points == ""|| data[x].comment_user_points == undefined){myP = 0;}else{myP = data[x].comment_user_points; }
								$("#tbodyAllCommentsTable").append($('<tr>')
										.append($('<td>')
											.append($('<article>')
												.attr('class', "post")
												.append($('<div>')
													.attr('class', "post-header")
													.append($('<div>')
														.attr('class', "image-holder")
														.attr('style', "width: 85px;")
														.append($('<img>')
															.attr('src', link)
														)
													)
													.append($('<div>')
														.attr('class', "text")
														.append($('<p>')
															.attr('class', "pull-right")
															.append(data[x].comment_date)
														)
														.append($('<p>')
															.append("<a href="+baseURL+'profile/view/'+data[x].comment_user_id+"><strong>"+data[x].comment_user_fullname+"</strong></a>, "+userType(data[x].comment_user_role)+", <strong>"+myP+"</strong> po&auml;ng")
														)
														.append($('<p>')
															.append(data[x].comment_text)
														)
														.append($('<p>')
															.attr('style', "margin-bottom: 0")
															.append(changeColorLikeIcon(data[x].id,data[x].link_id,data[x].comment_user_id,data[x].liked_by_me))
															.append("&ensp;&ensp;<i onclick=replyToThisComment("+data[x].id+","+data[x].link_id+","+data[x].comment_user_id+") class='fa fa-comment' style='font-size:15px;cursor:pointer'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Reply</span></i>")
														)
													)
													.append($('<div>')
														.attr('id', "txtArea"+data[x].id)
														.attr('style', "width: 91%;padding-top: 8px;margin-left: 9%;display:none")
														.append($('<div>')
															.attr('class', "input-group mb-3")
															.append("<span onclick=hideReplyComment("+data[x].id+") style='margin-top: -5px;padding-right: 10px;font-size: 36px;font-weight: bold;cursor:pointer'>-</span>")
															.append($('<textarea>')
																.attr('class', "form-control")
																.attr('rows', "1")
																.attr('id', "replyToComment"+data[x].id)
																.attr('placeholder', "Reply To This Comment")
															)
															.append($('<div>')
																.attr('class', "input-group-append")
																.append($('<button>')
																	.attr('class', "btn btn-dark")
																	.attr('onclick', "saveReplyTo("+data[x].id+","+data[x].link_id+","+data[x].link_user_id+","+data[x].comment_user_id+")")
																	.attr('type', "button")
																		.append("KOMMENTERA")
																)
															)
														)
													)
														.append($('<div>')
															.attr('style', "width: 91%;margin-left: 9%;")
															.append($('<table>')
																.attr('id', "dispReplyToTable"+data[x].id)
																.attr('class', "table table-striped table-bordered")
																.append($('<thead>')
																	.append($('<tr>')
																		.append($('<th>')
																			.attr('scope', "col")
																		)
																	)
																)
																.append($('<tbody>')
																	.attr('id', "tbodyDispReplyToTable"+data[x].id)
																)
																
															)
														)
															
													
												)
											)
										)
									);
									displayCommentReplies(data[x].id);
							}
								getCommentLinkDataTable = $('#allCommentsTable').DataTable({
								  "searching": false,
								  "lengthChange": false,
								  "ordering": false,
								  "info": false, 
								  "pageLength": 7
								});
							$("#allCommentsTable").show();
							$("#noAllComments").hide();
							$("#allCommentsTable thead").css("display","none");
						}else{
							$("#allCommentsTable").hide();
							$("#noAllComments").show();
						}
						countHearts();
						countComments();
						if(data.length == 1 && addChk == 1){
							window.location.reload();
							//console.log(121);
						}
					},
				error:
				function(data){
					//console.log("false");		
				}
			});		

		}
		function displayCommentReplies(comment_id){
			//console.log(comment_id);
			var baseURL = $("#baseURL").val();
			$.ajax({
				type: "POST",
				url: "../../comments/getReplyToComment",
				data:  {comment_id:comment_id},
				dataType: "json",
				success:
					function(data) {
						//console.log(data);
						if(data.length > 0){
							$("#tbodyDispReplyToTable"+comment_id).empty();
							for(var x=0;x < data.length;x++){
								//console.log(x);
								var wlh = $("#base_url").val();
								var myP = 0;
								var link = "";
								if(data[x].comment_user_prof_pic == ""){
									link = wlh+"images/user-pic.png";
								}else{
									link = wlh+"images/uploads/"+data[x].comment_user_id+"/"+data[x].comment_user_prof_pic;
								}
								if(data[x].comment_user_points == ""|| data[x].comment_user_points == undefined){myP = 0;}else{myP = data[x].comment_user_points; }
								$("#tbodyDispReplyToTable"+comment_id).append($('<tr>')
										.append($('<td>')
											.append($('<article>')
												.attr('class', "post")
												.append($('<div>')
													.attr('class', "post-header")
													.append($('<div>')
														.attr('class', "image-holder")
														.attr('style', "width: 85px;")
														.append($('<img>')
															.attr('src', link)
														)
													)
													.append($('<div>')
														.attr('class', "text")
														.append($('<p>')
															.attr('class', "pull-right")
															.append(data[x].comment_date)
														)
														.append($('<p>')
															.append("<a href="+baseURL+'profile/view/'+data[x].comment_user_id+"><strong>"+data[x].comment_user_fullname+"</strong></a>, "+userType(data[x].comment_user_role)+", <strong>"+myP+"</strong> po&auml;ng")
														)
														.append($('<p>')
															.append(data[x].comment_text)
														)
														.append($('<p>')
															.attr('style', "margin-bottom: 0")
															.append(changeColorLikeReplyToIcon(data[x].id,data[x].link_id,data[x].comment_user_id,data[x].liked_by_me))
															
														)
														
													)
												)
											)
										)
									);
							}
							var tblID = '#dispReplyToTable'+comment_id;
							//console.log(tblID);

								$(tblID).DataTable({
								  "searching": false,
								  "lengthChange": false,
								  "ordering": false,
								  "info": false, 
								  "pageLength": 5
								});
							$('#dispReplyToTable'+comment_id).show();
						}
						countHearts();
						countComments();
					},
				error:
				function(data){
					//console.log("false");		
				}
			});
		}
		function saveReplyTo(comment_id,link_id,link_user_id,comment_user_id){
			var comment = $("#replyToComment"+comment_id).val();
			//console.log(comment);
			if(comment != undefined && comment != ""){
				$.ajax({
						type: "POST",
						url: "../../comments/addReplyCommentToLink",
						dataType: "json",
						data:  {comment_id:comment_id,link_id:link_id,link_user_id:link_user_id,comment:comment,comment_user_id:comment_user_id},
						success:
							function(data) {
								getTheCommentsOfLink();
							},
						error:
						function(data){	
						}
				});
			}
		}
		function hideReplyComment(id){
			$("#txtArea"+id).hide();
		}
		function replyToThisComment(id,link_id,comment_user_id){
			
			//console.log(id);
			//console.log(link_id);
			//console.log(comment_user_id);
			
			if($("#sessLoggedIn").val() == 1){
				$("#txtArea"+id).show();
			}
		}
		function changeColorLikeReplyToIcon(id,link_id,comment_user_id,liked_by_me){
			//$("#dispReplyToTable"+id+"_wrapper").css("width","91%");
			//$("#dispReplyToTable"+id+"_wrapper").css("margin-left","9%");
			if(liked_by_me == 0){
				return "<i onclick=likeThisReplyToComment("+id+","+link_id+","+comment_user_id+") class='fa fa-thumbs-up' style='font-size:15px;cursor:pointer'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Like</span></i>";
			}else{
				return "<i onclick=likeThisReplyToComment("+id+","+link_id+","+comment_user_id+") class='fa fa-thumbs-up' style='font-size:15px;cursor:pointer;color:#007bff;'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Like</span></i>";
			}
		}
		function changeColorLikeIcon(id,link_id,comment_user_id,liked_by_me){
			if(liked_by_me == 0){
				return "<i onclick=likeThisComment("+id+","+link_id+","+comment_user_id+") class='fa fa-thumbs-up' style='font-size:15px;cursor:pointer'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Like</span></i>";
			}else{
				return "<i onclick=likeThisComment("+id+","+link_id+","+comment_user_id+") class='fa fa-thumbs-up' style='font-size:15px;cursor:pointer;color:#007bff;'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Like</span></i>";
			}
		}
		function likeThisReplyToComment(comment_id,link_id,comment_user_id){
			//console.log(comment_id);
			//console.log(link_id);
			$.ajax({
					type: "POST",
					url: "../../comments/addLikesReplyToComment",
					dataType: "json",
					data:  {link_id:link_id,comment_id:comment_id,comment_user_id:comment_user_id},
					success:
						function(data) {
							getTheCommentsOfLink();
							
						},
					error:
					function(data){
						//console.log("false");		
					}
			});
		}
		function likeThisComment(comment_id,link_id,comment_user_id){
			//console.log(comment_id);
			//console.log(link_id);
			$.ajax({
					type: "POST",
					url: "../../comments/addLikesToComment",
					dataType: "json",
					data:  {link_id:link_id,comment_id:comment_id,comment_user_id:comment_user_id},
					success:
						function(data) {
							getTheCommentsOfLink();
							
						},
					error:
					function(data){
						//console.log("false");		
					}
			});
		}
		function countHearts(){
			var id = $("#link_id").val();
				$.ajax({
					type: "POST",
					url: "../../comments/countLikesInComment",
					dataType: "json",
					data:  {link_id:id},
					success:
						function(data) {
							if(data > 0){
								$("#numberOfLikes").html(data);
							}else{
								$("#numberOfLikes").html(0);
							}
							
						},
					error:
					function(data){
						//console.log("false");		
					}
				});
		}
		function countComments(){
			var id = $("#link_id").val();
				$.ajax({
					type: "POST",
					url: "../../comments/countComments",
					dataType: "json",
					data:  {link_id:id},
					success:
						function(data) {
							if(data > 0){
								$("#cntComments").html(data);
							}else{
								$("#cntComments").html(0);
							}
						},
					error:
					function(data){
						//console.log("false");		
					}
				});
		}
		function getThePicFR(profpic,baseURL,id){

				if(profpic == ""){
					return baseURL+"images/user-pic.png";
				}else{
					return baseURL+"images/uploads/"+id+"/"+profpic;
				}

		}
		function getMyFriendRequest(){
				$("#dropdown-text").empty();
				$.ajax({
					type: "GET",
					cache: false,
					url: "../../friends/getFriendRequest",
					dataType: "json",
					success:
						function(data) {
							//console.log(data);
							if(data.length > 0){
								
								for(var x=0;x < data.length;x++){
									$("#dropdown-text")
										.append($('<div>')
											.attr('class', "dropdown-item")
											.append($('<div>')
												.attr('class', "row")
												.append($('<div>')
													.attr('class', "col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4")
													.append($('<img>')
														.attr('style', "width:100%;height:60px")
														.attr('src', getThePicFR(data[x].prof_pic,data[x].baseURL,data[x].u_id))
													)
												)
												.append($('<div>')
													.attr('class', "col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8")
													.attr('style', "padding-left:0")
													.append($('<p>')
														.attr('style', "text-align:center;font-size:16px;margin-bottom: 5px;margin-top: 5px;font-weight: bold;text-transform: capitalize;")
														.append(data[x].u_fullname)
														
													)
													.append($('<p>')
														.attr('style', "text-align:center")
														.append($('<button>')
															.attr('class', "btn btn-primary")
															.attr('type', "button")
															.attr('style', "margin-right:4px;padding:5px 5px 5px 5px;")
															.attr('onclick', "confirmFriendRequest("+data[x].u_id+")")
															.append("Confirm")
														)
														.append($('<button>')
															.attr('class', "btn btn-secondary")
															.attr('type', "button")
															.attr('style', "margin-right:4px;padding:5px 5px 5px 5px;")
															.attr('onclick', "deleteFriendRequest("+data[x].u_id+")")
															.append("Delete")
														)
													)
													
												)
												
											)
										);
								}
							}else{
								$("#dropdown-text")
										.append($('<div>')
											.attr('class', "alert alert-light")
											.attr('role', "alert")
											.attr('style', "width: 250px;border:0")
											.append($('<h5>')
												.append("No Friend Request/s.")
											)
										);
							}	
						},
					error:
					function(data){
						//console.log(data);		
					}
				});
		}
		function confirmFriendRequest(friend_id){
			//console.log(friend_id);
			$.ajax({
					type: "POST",
					url: "../../friends/confirmFriendRequest",
					dataType: "json",
					data: {friend_id:friend_id},
					success:
						function(data) {
							getMyFriendRequest();
						},
					error:
					function(data){
						//console.log("false");		
					}
				});

		}
		function deleteFriendRequest(friend_id){
			//console.log(friend_id);
			$.ajax({
					type: "POST",
					url: "../../friends/deleteFriendRequest",
					dataType: "json",
					data: {friend_id:friend_id},
					success:
						function(data) {
							getMyFriendRequest();
						},
					error:
					function(data){
						//console.log("false");		
					}
				});
		}
	</script>
	
	
</body>
</html>	