<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td{    word-break: break-all;}
	#linkTable tbody tr td{    word-break: break-all;}
	#linkTable_filter{float:right}
	#approvelinkTable_filter{float:right}
	#editApprovePostForm .bootstrap-datetimepicker-widget{top:0px !Important;bottom:auto !Important;}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="valid-feedback deleteMessageSucc"><h5 class="text-center">Post successfully deleted.</h5></div>
			<div class="invalid-feedback deleteMessageError"><h5 class="text-center">Post unsuccessfully deleted.</h5></div>
			<div class="valid-feedback" id="editPostSuccess"><h5 class="text-center">Post has been updated.</h5></div>
			<ul class="nav nav-pills mb-3" id="pills-tab-approvepost" role="tablist">
			  <li class="nav-item">
				<a class="nav-link active" id="pills-unapprove-tab" data-toggle="pill" href="#pills-unapprove" role="tab" aria-controls="pills-unapprove" aria-selected="true">Unapprove Post</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" id="pills-approved-tab" data-toggle="pill" href="#pills-approved" role="tab" aria-controls="pills-approved" aria-selected="false">Approved Post</a>
			  </li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-unapprove" role="tabpanel" aria-labelledby="pills-unapprove-tab">
					<div class="content-holder">
						<input type="hidden" id="getBaseURL" value="<?php echo base_url();?>">
						<?php if($this->session->userdata('u_role') == 6){?>
								<table class="table table-hover" id="linkTable" style="display:none;width:100%">
									<thead>
										<tr>
										  <th scope="col">Title</th>
										  <th scope="col">Description</th>
										  <th scope="col">Link</th>
										  <th scope="col">Category</th>
										  <th scope="col">Warning</th>
										  <th scope="col">Tags</th>
										  <th scope="col" class="text-center"></th>
										  <th scope="col" class="text-center"></th>
										  <th scope="col" class="text-center"></th>
										</tr>
									</thead>
									<tbody id="tbodylinktable">
										
									</tbody>
								</table>
								<div style="display:none" id="unapprovenodata" class="alert alert-light" role="alert">
									<h4>No data to be displayed.</h4>
								</div>
						<?php } ?>
					</div>
				</div>
			  <div class="tab-pane fade" id="pills-approved" role="tabpanel" aria-labelledby="pills-approved-tab">
				<div class="content-holder">
						<input type="hidden" id="getBaseURL" value="<?php echo base_url();?>">
						<?php if($this->session->userdata('u_role') == 6){?>
								<table class="table table-hover" id="approvelinkTable" style="display:none;width:100%">
									<thead>
										<tr>
										  <th scope="col">Title</th>
										  <th scope="col">Description</th>
										  <th scope="col">Link</th>
										  <th scope="col">Category</th>
										  <th scope="col">Warning</th>
										  <th scope="col">Tags</th>
										  <th scope="col">Published Date</th>
										  <th scope="col" class="text-center"></th>
										  <th scope="col" class="text-center"></th>
										</tr>
									</thead>
									<tbody id="tbodyapprovelinktable">
										
									</tbody>
								</table>
								<div style="display:none" id="approvenodata" class="alert alert-light" role="alert">
									<h4>No data to be displayed.</h4>
								</div>
						<?php } ?>
					</div>
			  </div>
			</div>
		</div>
	</main>   
</div>  
<!-- Modal -->
<div class="modal fade" id="approvePostLinkModal" tabindex="-1" role="dialog" aria-labelledby="approvePostLinkModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
		<h5 class='text-left'>Publish Date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
		<div class="container">
			<div class="row">
				<div class='col-md-3'></div>
				<div class='col-md-6'>
					<h5 class='text-center'>Datetime To Publish</h5>
					<div class="form-group">
						<input type='text' class="form-control" id="publishto" />
						<div class="invalid-feedback pubDateTo">Please provide a date and time.</div>
					</div>
				</div>
				<div class='col-md-3'></div>
				<!--<div class='col-md-6'>
					<h5 class='text-center'>Datetime From</h5>
					<div class="form-group">
						<input type='text' class="form-control" id="publishfrom" />
					</div>
					<div class="invalid-feedback pubDateFrom">Please provide a date and time.</div>
				</div>
				<div class='col-md-6'>
					<h5 class='text-center'>Datetime To</h5>
					<div class="form-group">
						<input type='text' class="form-control" id="publishto" />
						<div class="invalid-feedback pubDateTo">Please provide a date and time.</div>
					</div>
				</div>-->
			</div>
		</div>
      </div>
      <div class="modal-footer">
		<button type="button" id="approveNow" class="btn btn-success">SAVE</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deletePostLinkModal" tabindex="-1" role="dialog" aria-labelledby="deletePostLinkModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5 class='text-center'>Are you sure you want to delete?</h5>
      </div>
      <div class="modal-footer">
		<button type="button" id="deleteNow" class="btn btn-success">YES</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteApprovePostLinkModal" tabindex="-1" role="dialog" aria-labelledby="deleteApprovePostLinkModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5 class='text-center'>Are you sure you want to delete?</h5>
      </div>
      <div class="modal-footer">
		<button type="button" id="deleteApproveNow" class="btn btn-success">YES</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="openApproveLink" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe id="getPostLinkURL" src=""></iframe>
			</div>
		</div>
    </div>
  </div>
</div>
<div class="modal fade" id="editApprovePost" tabindex="-1" role="dialog" aria-labelledby="editApprovePost">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel">Edit Post</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<form class="needs-validation-editlink" id="editApprovePostForm" role="form" method="POST" novalidate="">
						<div class="row">
                            <div class="col-xl-6">
                                <h3>V&#228;lj ikon</h3>
                                <ul class="meta-links">
                                    <li>
                                        <input class="pick-tag" type="radio" id="pick-1" value="ico1.png" name="editPicks">
                                        <label for="pick-1">
                                            <img src="<?php echo base_url();?>images/ico1.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico2.png" id="pick-2" name="editPicks">
                                        <label for="pick-2">
                                            <img src="<?php echo base_url();?>images/ico2.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio"  value="ico3.png" id="pick-3" name="editPicks">
                                        <label for="pick-3">
                                            <img src="<?php echo base_url();?>images/ico3.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico4.png" id="pick-4" name="editPicks">
                                        <label for="pick-4">
                                            <img src="<?php echo base_url();?>images/ico4.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico5.png" id="pick-5" name="editPicks">
                                        <label for="pick-5">
                                            <img src="<?php echo base_url();?>images/ico5.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico6.png" id="pick-6" name="editPicks">
                                        <label for="pick-6">
                                            <img src="<?php echo base_url();?>images/ico6.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico7.png" id="pick-7" name="editPicks">
                                        <label for="pick-7">
                                            <img src="<?php echo base_url();?>images/ico7.png" alt="images description">
                                        </label>
                                    </li>
                                </ul>
                                <div class="form-group">
                                    <div class="label-area">
                                        <label for="editUrlLinkPost">URL</label>
                                    </div>    
                                    <input type="text" class="form-control" name="editUrlLinkPost" id="editUrlLinkPost" required>
									<div class="invalid-feedback">Please provide a url.</div>
                                </div>
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editlinkTitle">Link Title</label>
                                    </div>    
                                    <input type="text" class="form-control" name="editlinkTitle" id="editlinkTitle" required>
									<div class="invalid-feedback">Please provide a tile of the link.</div>
                                </div>
                                <div class="form-group">
                                    <label for="editLinkDescription">Beskrivning</label>
                                    <textarea class="form-control" name="editLinkDescription" id="editLinkDescription" rows="2" required></textarea>
									<div class="invalid-feedback">Please provide a description.</div>
                                </div>
                            </div>
                            <div class="col-xl-6">
								<br>
								<br>
								<br>
								<div class="form-group cCateg">
                                    <label for="editLinkCategory">Kategorier <span class="label-tag">(klicka p&#229; kategorin f&#246;r att ta bort)</span></label>
                                    <div id="edlinkcateg"></div>
									<div class="invalid-feedback editlinkCategory">Choose atleast 1 category.</div>
                                </div> 
                                <div class="form-group cWarn">
                                    <label for="editLinkWarning">Varningar </label>
                                    <div id="edlinkwarn"></div>
									<div class="invalid-feedback editlinkWarning">Choose atleast 1 warning.</div>
                                </div>  
                                <div class="form-group">
                                    <label for="editLinkTag">Taggar</label>   
                                    <input type="text" id="editLinkTag" name="editLinkTag" class="form-control" placeholder="#gudrun, #r&#246;vfinne, #feministj&#228;vel, #&#228;cklig" required>
									<div class="invalid-feedback">Please provide a tag.</div>
								</div>
								<!--<div class="form-group">
                                    <div class="label-area">
                                        <label for="editPublishedFrom">Published From</label>
                                    </div>    
									<input type='text' class="form-control" name="editPublishedFrom" id="editPublishedFrom" required/>
									<div class="invalid-feedback">Please provide a date.</div>
                                </div>-->
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editPublishedTo">Datetime To Publish</label>
                                    </div>    
									<input type='text' class="form-control" name="editPublishedTo" id="editPublishedTo" required/>
									<div class="invalid-feedback">Please provide a date.</div>
                                </div>
                            </div>    
                        </div>   
                        <div class="row btn-area">
                            
                            <div class="col-xl-6">
                               <button id="btnEditPostLink" type="submit" class="btn btn-dark" type="submit">UPPDATERA NU</button>
                            </div>
                        </div>    
				</form>
			</div>
	  </div>
	</div>
</div>
    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/moment.js"></script>
    <script src="<?php echo base_url();?>js/transition.js"></script>
    <script src="<?php echo base_url();?>js/collapse.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
	<script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
 	<script src="<?php echo base_url();?>js/postlink.js"></script>    
 	<script src="<?php echo base_url();?>js/listpost.js"></script>    
    <script src="<?php echo base_url();?>js/script.js"></script> 
	<script type="text/javascript">
            $(function () {
              //  $('#publishfrom').datetimepicker({sideBySide: true});
				//$('#editPublishedFrom').datetimepicker({sideBySide: true});
				$('#editPublishedTo').datetimepicker({sideBySide: true});
                $('#publishto').datetimepicker({sideBySide: true});
            });
    </script>
</body>
</html>