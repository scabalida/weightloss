<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#userTable_filter{float:right}
	.width25{width:20px !important;}
	#userTable tbody tr td button i.fa{font-size:18px}
	#userTable tbody tr td{vertical-align:middle;word-break: break-all;}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="valid-feedback deleteMessageSucc"><h5 class="text-center">User successfully deleted.</h5></div>
			<div class="invalid-feedback deleteMessageError"><h5 class="text-center">User unsuccessfully deleted.</h5></div>
			<div class="valid-feedback" id="userEditSuccess"><h5 class="text-center">User has been updated.</h5></div>
			<div class="invalid-feedback" id="userEditError"><h5 class="text-center">Error in updating the user.</h5></div>
			  
			<div class="content-holder">
				<table id="userTable" class="table table-striped table-bordered" style="width:100%;display:none">
					<thead>
						<tr>
							<th scope="col text-center">Full Name</th>
							<th scope="col text-center">Email</th>
							<th scope="col text-center">Birthdate</th>
							<th scope="col text-center">Current Weight</th>
							<th scope="col text-center">Total Weight Loss</th>
							<th scope="col text-center">User Type</th>
							<th scope="col text-center" style="width:25px">Edit</th>
							<th scope="col text-center" style="width:25px">Remove</th>
						</tr>
					</thead>
					<tbody id="tbodyallusers">
						
					</tbody>
				</table>
				<div style="display:none" id="nousers" class="alert alert-light" role="alert">
					<h4>No data to be displayed.</h4>
				</div>
			</div>
		</div>
	</main>   
</div>  
<!-- Modal -->
<div class="modal fade" id="removeUserModal" tabindex="-1" role="dialog" aria-labelledby="removeUserModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5 class='text-center'>Are you sure you want to delete?</h5>
      </div>
      <div class="modal-footer">
		<button type="button" id="deleteNow" class="btn btn-success">YES</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
        
      </div>
    </div>
  </div>
</div>
	<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="editUserModal">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title" id="exampleModalLabel">Update User</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body">
			<form class="edituser-needs-validation" id="editUserForm" role="form" method="POST" novalidate>
			  <div class="form-group">
				<label for="fname" class="control-label">Full Name</label>
				<input type="text" class="form-control" name="fname" id="fname" required>
				<div class="invalid-feedback">Please provide your full name.</div>
			  </div>
			  <div class="form-group">
				<label for="email" class="control-label">Email Address</label>
				<input type="email" class="form-control" name="email" id="email" required>
				<div class="invalid-feedback">Please provide valid email address.</div>
			  </div>
			  <div class="form-group">
				<label for="bday" class="control-label">Date of Birth</label>
				<input type="date" class="form-control" name="bday" id="bday" required>
				<div class="invalid-feedback">Please provide your birthday.</div>
			  </div>
			  <div class="form-group">
				<label for="user_role" class="control-label">User Type</label>
				<select class="form-control" id="user_role">
					<option value="1">Regular member</option>
					<option value="2">Donator Member</option>
					<option value="3">VIP Member</option>
					<option value="4">Updater Member</option>
					<option value="5">Operator</option>
					<option value="6">Adminstrator</option>
				</select>
		      </div>
		  </div>
		  <div class="modal-footer">
			<button type="submit" id="editUserBtn" class="btn btn-dark">SAVE</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/user_info.js"></script>    
	<script src="<?php echo base_url();?>js/script.js"></script> 


</body>
</html>