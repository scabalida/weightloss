<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="invalid-feedback" id="editUserPassError"><h5 class="text-center">Username and Passord unsuccessfully updated.</h5></div>
			<div class="invalid-feedback" id="editUserPassNotMatch"><h5 class="text-center">Old Passord does not match.</h5></div>
			<div class="valid-feedback" id="editUserPassSuccess"><h5 class="text-center">Username and Passord successfully updated</h5></div>
			<div class="content-holder">
				<h4>Update Username and Password</h4>
					<form class="needs-validation-edituserpass" id="editUserPassForm" role="form" method="POST" novalidate="">
						<div class="row">
                            <div class="col-xl-6 col-md-6">
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="editusername">Username</label>
                                    </div>    
                                    <input type="text" class="form-control" value="<?php echo $this->session->userdata('u_username'); ?>" name="editusername" id="editusername" required>
									<div class="invalid-feedback">Please provide a username.</div>
                                </div>
								<div class="form-group">
									<label for="editoldpass" class="control-label">Old Password</label>
									<input type="password" class="form-control" name="editoldpass" placeholder="********" id="editoldpass" required="">
									<div class="invalid-feedback">Please provide your old password.</div>
								</div>
							</div>
							<div class="col-xl-6 col-md-6">
								<div class="form-group">
									<label for="editnewpass" class="control-label">New Password</label>
									<input type="password" class="form-control" name="editnewpass" placeholder="********" id="editnewpass" required="">
									<div class="invalid-feedback">Please provide your new password.</div>
								</div>
								<div class="form-group">
									<label for="editcnewpass" class="control-label">Confirm New Password</label>
									<input type="password" class="form-control" name="editcnewpass" placeholder="********" id="editcnewpass" required="">
									<div class="invalid-feedback">Please confirm your new password.</div>
									<div class="invalid-feedback errorUserPass">New Passwords does not match.</div>
								</div>
							</div>
						</div>
						<div class="row btn-area">
                            <div class="col-xl-6">
                               <button id="btnEditUserPass" class="btn btn-dark" type="submit">UPPDATERA NU</button>
                            </div>
                        </div>
					</form>
			</div>
		</div>
	</main>   
</div>  

    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
   <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/user_info.js"></script>    
	<script src="<?php echo base_url();?>js/script.js"></script>  
    

</body>
</html>