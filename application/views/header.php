<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Weight Loss Group - Track your weight, add articles, videos and track your weight loss progress!</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!--Bootstrap-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
    <!--Stylesheets-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/animate.css"> 
    <link rel='stylesheet prefetch' href='http://kendo.cdn.telerik.com/2017.3.913/styles/kendo.common.min.css'> 
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/style.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/custom.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datetimepicker.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/dataTables.bootstrap4.min.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div id="top-wrap">
    <div class="top-one">
        <ul class="lang">
            <li>
                <a href="#">
                    <img src="<?php echo base_url();?>images/flag-1.png" alt="images description">
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="<?php echo base_url();?>images/flag-2.png" alt="images description">
                </a>
            </li>
        </ul>
        <div class="container">
            <nav class="navbar nav navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="<?php echo base_url();?>">
                    <img src="<?php echo base_url();?>images/logo.jpg" alt="logo description"/>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo base_url();?>">HOME</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ABOUT THE WEIGHT LOSS GROUP</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://www.facebook.com/groups/300458277384404/">FACEBOOK</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url();?>">LINKS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/donate">DONATE</a>
                        </li>
						
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
						<?php if($this->session->userdata('loggedin') == TRUE){ ?>
						
                        <img src="<?php echo base_url();?>images/plus.jpg" alt=""/>
                        <div class="navbar-links">
							<div class="dropdown" id="header-dropdown">
								<a class="dropdown-toggle" href="#" role="button" id="dropdownFRLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<div class="button-link"> 
										<i class="fa fa-users"></i> 
										<span class="badge badge-danger" id="countFriendRequest"></span>
									</div>
								</a>
								<a class="dropdown-toggle" href="#" role="button" id="dropdownNotifLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<div class="button-link"> 
										<i class="fa fa-bell"></i> 
										<span class="badge badge-danger" id="countNotifs"></span>
									</div>
								</a>
								<div class="dropdown-menu" id="dropdown-text">
									
								</div>
								<a href="<?php echo base_url();?>chat">
									<div class="button-link"> 
										<i class="fa fa-envelope"></i> 
										<span class="badge badge-danger"></span>
									</div>
								</a>
							</div>
                        </div>
                        <div class="dropdown show">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img id="dp" style="max-width:40px" src="<?php echo base_url()."images/uploads/".$this->session->userdata('u_id')."/".$this->session->userdata('prof_pic')?>" alt=""/> 
                                <span class="name"><?php echo $this->session->userdata('u_username'); ?>(<?php if($this->session->userdata('u_points')==""){echo 0;}else{echo $this->session->userdata('u_points');}?>)</span> 
                            </a>  
                            <ul class="dropdown-menu dropdown-menu-right">
							
								<?php if($this->session->userdata('loggedin') == TRUE && $this->session->userdata('u_role') == 6){ ?>
									<li><a href="<?php echo base_url();?>admin-panel" class="dropdown-item">ADMIN PANEL</a></li>
									<li><a href="<?php echo base_url();?>user-management" class="dropdown-item">MANAGE USERS</a></li>
								<?php } ?>
                                <li><a href="<?php echo base_url();?>my-profile" class="dropdown-item">PROFILE</a></li>
								<li><a href="<?php echo base_url();?>friends" class="dropdown-item">MY FRIENDS</a></li>
                                <li><a href="<?php echo base_url();?>user-settings" class="dropdown-item">USER SETTINGS</a></li>
                                <li><a href="<?php echo base_url();?>favorite-links" class="dropdown-item">FAVOURITES</a></li>
                               
                                
                                <li><a href="<?php echo base_url();?>login/logout" class="dropdown-item">LOG OUT</a></li>
                            </ul>  
                        </div>
						
						<?php } else{ ?>
							<div class="navbar-links">
								<a href="#" data-toggle="modal" data-target="#signInModal">SIGN UP</a>
							</div>
						<?php }?>
                    </form>
                </div>
            </nav>
        </div>   
    </div>    
    <div class="top2-wrap">
        <div class="container">
            <div class="navcontainer2"> 
                <div class="navcontainer2-holder">
                   
                    <ul class="navlist2">
                        <li><a href="<?php echo base_url();?>category/view/funny" >FUNNY</a></li>
                        <li><a href="<?php echo base_url();?>category/view/diet-and-food" >DIET AND FOOD</a></li>
                        <li><a href="<?php echo base_url();?>category/view/cardio" >CARDIO</a></li>
                        <li><a href="<?php echo base_url();?>category/view/weight-training" >WEIGHT TRAINING</a></li>
                        <li><a href="<?php echo base_url();?>category/view/motivational" >MOTIVATIONAL</a></li>
                        <li><a href="<?php echo base_url();?>category/view/workout-music" >WORKOUT MUSIC</a></li>
                        <li><a href="<?php echo base_url();?>category/view/mental-health" >MENTAL HEALTH</a></li>
                        <li><a href="<?php echo base_url();?>category/view/medical" >MEDICAL</a></li>
                        <li><a href="<?php echo base_url();?>category/view/technology" >TECHNOLOGY</a></li>
                        <li><a href="<?php echo base_url();?>category/view/other" >OTHER</a></li>
                       
                    </ul>
                </div>    
            </div>
        </div>    
    </div>
    <div class="top2-wrap second">
        <div class="container">
            <div class="navcontainer2">
                <div class="navcontainer2-holder"> 
                    <ul class="navlist2">
                        <li><a href="#">ADMIN CHAT<span class="badge badge-danger">2</span></a></li>
                        
                    </ul>
                </div>    
            </div>
        </div>    
    </div>
    </div> 
