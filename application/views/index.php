	<div class="container" id="iframe-container" style="display:none">
		<iframe id="iframeSRC" src="" frameborder="0" target="_top" allow="encrypted-media" allowfullscreen  style="width:100%;height:600px;"></iframe>
	</div>
	<div class="container" id="home-container">
        <main id="mian">
            <aside id="sidebar">
				<?php if($this->session->userdata('loggedin') != TRUE){ ?>
				<div class="widget">
					<h2>LOG IN NOW</h2>
					<div class="widget-holder">
						<form class="form-horizontal"  data-toggle="validator"  id="loginForm" role="form" method="POST">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Username" id="username" name="inputUsername" required>
                            </div>
							<div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" id="password" name="inputPassword" required>
                            </div>
							<div class="invalid-feedback text-center loginerror">Username or Password is invalid.</div><br/>
                            <button type="submit" class="btn btn-block btn-dark btn-md">SIGN IN</button>
                        </form>
					</div> 
				</div> 
				<?php } ?>
                <div class="widget">
                    <h2>ALL USERS HAVE LOST</h2>
                    <div class="widget-holder">
						<h5 class="text-center" style="color:#007bff"><span id="totWeightLossUsers"></span> lbs</h5>
                    </div>    
                </div>   
                <div class="widget">
                    <h2>MOST POPULAR</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><a href="#vecka">DAY</a></li>
                            <li><a href="#manad">MONTH</a></li>
                            <li><a href="#ar">YEAR</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="vecka">
                                <div class="tab-content">
									<div  id="favoritedAjaxload"><img src="<?php echo base_url();?>images/loading.gif" style="width:70px;display:block;margin:30px auto;" /></div>
                                    <div id="favoriteDayContainer" style="display:none">
                                        <div class="tab-content-holder">
                                            <div class="side-post-r">
												<div class="post">
													<a style="cursor:pointer;" id="favored_link_url">
														<img id="favored_link_image">
														<div class="p-caption">
															<div class="like-tag"><i class="fa fa-heart"></i> <span  id="favored_link_hearted"></span></div>
															<p id="favored_link_title"></p>
														</div>  
													</a>      
												</div>  
                                            </div>    
                                        </div> 
										<div style="display:none" id="noFavoriteDay" class="alert alert-light" role="alert">
											<h5 class="text-center">No data to be displayed.</h5>
										</div>
                                    </div>
                                </div>
                            </div>
                            <div id="manad" style="display:none">
                                <div class="tab-content-holder">
									<div class="side-post-r">
                                        <div class="post">
                                            <a style="cursor:pointer;" id="month_favored_link_url">
                                                <img id="month_favored_link_image">
                                                <div class="p-caption">
                                                    <div class="like-tag"><i class="fa fa-heart"></i> <span  id="month_favored_link_hearted"></span></div>
                                                    <p id="month_favored_link_title"></p>
                                                </div>  
                                            </a>      
                                        </div>  
                                    </div> 
                                </div>
								<div style="display:none" id="noFavoriteMonth" class="alert alert-light" role="alert">
									<h5 class="text-center">No data to be displayed.</h5>
								</div>
                            </div>
                            <div id="ar"  style="display:none">
                                <div class="tab-content-holder">
                                    <div class="side-post-r">
                                        <div class="post">
                                            <a style="cursor:pointer;" id="year_favored_link_url">
                                                <img id="year_favored_link_image">
                                                <div class="p-caption">
                                                    <div class="like-tag"><i class="fa fa-heart"></i> <span  id="year_favored_link_hearted"></span></div>
                                                    <p id="year_favored_link_title"></p>
                                                </div>  
                                            </a>      
                                        </div>  
                                    </div> 
								</div>   
								<div style="display:none" id="noFavoriteYear" class="alert alert-light" role="alert">
									<h5 class="text-center">No data to be displayed.</h5>
								</div>
                            </div>
                        </div>
                    </div>    
                </div> 
                <!--<div class="widget">
                    <h2>OUR TOP USERS</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><h3 style="margin:5px;">Username</h3></li>
                            <li><h3 style="margin:5px;">Points</h3></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tradar">
                                <div class="tab-content">
                                    <div id="populara">
                                        <div class="tab-content-holder" style="padding: 0;"> 
                                            <ul class="popular-list" id="rankUserPoints" style="display:none">
                                                 
                                            </ul>
											<div style="display:none" id="noRankUser" class="alert alert-light" role="alert">
												<h5 class="text-center">No data to be displayed.</h5>
											</div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>-->
				<div class="widget">
                    <h2>WEIGHT LOSS LEADERS</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><h3 style="margin:5px;">Username</h3></li>
                            <li><h3 style="margin:5px;">Weight Loss</h3></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tradar-wl">
                                <div class="tab-content">
                                    <div id="populara-wl">
                                        <div class="tab-content-holder" style="padding: 0;"> 
                                            <ul class="popular-list" id="topWeightLoss" style="display:none">
                                                 
                                            </ul>
											<div style="display:none" id="notopWeightLoss" class="alert alert-light" role="alert">
												<h5 class="text-center">No data to be displayed.</h5>
											</div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </aside>
            <div id="content">
				<?php if($this->session->userdata('loggedin') == TRUE){ ?>
                
				<p class="head-tagline">Weight Loss Links</p>
                <div class="content-holder">
                    <form class="needs-validation-addlink" id="addPostLinkForm" role="form" method="POST" novalidate="">
                        <div class="valid-feedback" id="linkPostSuccess"><h5 class="text-center">Link has been created.</h5></div>
						<div class="row">
                            <div class="col-xl-6">
                                <h3>Choose An Icon</h3>
                                <ul class="meta-links">
                                    <li>
                                        <input class="pick-tag" type="radio" id="pick-1" value="ico1.png" name="picks" checked="checked">
                                        <label for="pick-1">
                                            <img src="<?php echo base_url();?>images/ico1.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico2.png" id="pick-2" name="picks">
                                        <label for="pick-2">
                                            <img src="<?php echo base_url();?>images/ico2.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio"  value="ico3.png" id="pick-3" name="picks">
                                        <label for="pick-3">
                                            <img src="<?php echo base_url();?>images/ico3.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico4.png" id="pick-4" name="picks">
                                        <label for="pick-4">
                                            <img src="<?php echo base_url();?>images/ico4.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico5.png" id="pick-5" name="picks">
                                        <label for="pick-5">
                                            <img src="<?php echo base_url();?>images/ico5.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico6.png" id="pick-6" name="picks">
                                        <label for="pick-6">
                                            <img src="<?php echo base_url();?>images/ico6.png" alt="images description">
                                        </label>
                                    </li>
                                    <li>
                                        <input class="pick-tag" type="radio" value="ico7.png" id="pick-7" name="picks">
                                        <label for="pick-7">
                                            <img src="<?php echo base_url();?>images/ico7.png" alt="images description">
                                        </label>
                                    </li>
                                </ul>
                                <div class="form-group">
                                    <div class="label-area">
                                        <label for="url">URL</label>
                                       <!-- <div class="check-area"> 
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            <label class="form-check-label" for="exampleCheck1">B&#228;dda in</label>
                                        </div>-->
                                    </div>    
                                    <input type="text" class="form-control" name="urlLinkPost" id="urlLinkPost" required>
									<div class="invalid-feedback">Please provide a url.</div>
                                </div>
								<div class="form-group">
                                    <div class="label-area">
                                        <label for="linkTitle">Link Title</label>
                                    </div>    
                                    <input type="text" class="form-control" name="linkTitle" id="linkTitle" required>
									<div class="invalid-feedback">Please provide a tile of the link.</div>
                                </div>
                                <!--<div class="form-group">
                                    <div class="label-area">
                                        <label for="rubrik">Rubrik</label>
                                        <div class="check-area"> 
                                            <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                            <label class="form-check-label" for="exampleCheck2">Engelska</label>
                                        </div>
                                    </div> 
                                    <input type="text" class="form-control" id="rubrik">
                                </div>-->
                                
                                <div class="g-recaptcha" id="addLinkCaptcha" data-sitekey="6Lfvp20UAAAAALc5YzA7iA_v3_sqmEsKWVVwrlmq"></div>   
                                <!--<div class="form-group">
                                    <label for="thumbnail">Thumbnail</label>
                                    <input type="text" class="form-control" id="thumbnail">
                                </div>-->
                            </div>
                            <div class="col-xl-6">
								<br>
                                <!--<div class="form-group">
                                    <div class="label-area">
                                        <label>Preview</label>
                                        <div class="check-area"> 
                                            <button type="button" class="btn btn-dark btn-sm">LISTA</button>
                                            <button type="button" class="btn btn-dark btn-sm">RUTN&#196;T</button>
                                            <button type="button" class="btn btn-dark btn-sm">TEXT</button>
                                        </div>
                                    </div> 
                                    <div class="post-view">
                                        <div class="image-area">
                                            <img src="images/img1.png" alt="images description">
                                        </div>  
                                        <div class="text">
                                            <h6>There are many variations of passages</h6>
                                            <div class="post-view-meta">
                                                <span class="like-counter"><i class="fa fa-heart"></i> 2030</span>
                                                <p>2019 Clicks</p>
                                                <p>16 <strong>generate</strong> </p>
                                                <p>Contrary to <strong>Clicks</strong></p>
                                            </div>    
                                        </div>  
                                    </div>    
                                </div>-->
								<div class="form-group">
                                    <label for="beskrivning">Description</label>
                                    <textarea class="form-control" name="linkDescription" id="linkDescription" rows="2" required></textarea>
									<div class="invalid-feedback">Please provide a description.</div>
                                </div>
								<div class="form-group">
                                    <label for="beskrivning">Categories <span class="label-tag">(Choose Categories)</span></label>
                                    <select id="addlinkCategory" class="form-control" multiple="multiple" data-placeholder="Select..." required>
                                        <option>FUNNY</option>
                                        <option>DIET AND FOOD</option>
                                        <option>SPORT</option>
                                        <option>CARDIO</option>
                                        <option>WEIGHT TRAINING</option>
                                        <option>MOTIVATIONAL</option>
                                        <option>WORKOUT MUSIC</option>
                                        <option>MENTAL HEALTH</option>
                                        <option>MEDICAL</option>
                                        <option>TECHNOLOGY</option>
                                        <option>OTHER</option>
                                    </select>
									<div class="invalid-feedback addlinkCategory">Choose atleast 1 category.</div>
                                </div> 
                                <div class="form-group">
                                    <label>Warnings </label>
                                    <select class="form-control" multiple="multiple" id="addlinkWarning" data-placeholder="Select..." required>
                                        <option>18+</option>
                                        <option>Bad Language</option>
                                        <option>Nudity</option>
                                    </select>
									<div class="invalid-feedback addlinkWarning">Choose atleast 1 warning.</div>
                                </div>  
                                <!--<div class="form-group">
                                    <label>Till&#228;ggningstid <span class="label-tag">(l&#228;mna blankt ifall den ska l&#228;ggas upp direkt)</span></label>
                                    <select class="custom-select" id="inputGroupSelect02">
                                        <option selected>15/12/2016 01:30:15</option>
                                        <option value="1">15/12/2016 01:30:15</option>
                                        <option value="2">15/12/2016 01:30:15</option>
                                        <option value="3">15/12/2016 01:30:15</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    <div class="label-area">
                                        <label for="rubrik">Toppl&#228;nk</label>
                                        <div class="check-area"> 
                                            <div class="switch">
                                                <input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round" type="checkbox">
                                                <label for="cmn-toggle-1"></label>
                                            </div>
                                        </div>
                                    </div> 
                                    <select class="custom-select" id="inputGroupSelect04">
                                        <option selected>24 timmar</option>
                                        <option value="1">12 timmar</option>
                                    </select>
                                </div> -->
                                <div class="form-group">
                                    <label for="taggar">Tags</label>   
                                    <input type="text" id="linkTag" name="linkTag" class="form-control" placeholder="#healthy #food #diet etc..." required>
									<div class="invalid-feedback">Please provide a tag.</div>
								</div>
								
                            </div>    
                        </div>   
                        <div class="row btn-area">
                            
                            <div class="col-xl-6">
                               <!-- <button class="btn btn-dark" type="button">SPARA</button>-->
                                <button id="btnAddPostLink" class="btn btn-dark" type="submit">Add link</button>
                            </div>
							 <!-- <div class="col-xl-6">
                                <button class="btn btn-dark" type="button">BL&#196;DDRA</button>
                            </div> 		-->	
                        </div>    
                    </form>     
                </div>   
              
				<br>
				<div class="container">
					
					<div class="row">
						<div class="col-xl-8 col-md-8"></div>
						<div class="col-xl-4 col-md-4">
							<a href="<?php echo base_url(); ?>my-profile#weightLossDataContainer" class="btn btn-dark pull-right">EDIT MY WEIGHT</a>
						</div>
					</div>
					<div class="row">
						<!--<h5 class="text-center">Total Weight Loss: <span id="totalWeightLoss" style="color:#007bff"></span></h5>-->
						<div class="col-xl-12 col-md-12">
							<div id="chartContainer" style="height: 370px; width: 100%;"></div>
							<div style="display:none" id="noWeightLossData" class="alert alert-light" role="alert">
								<h4>No data to be displayed.</h4>
							</div>
						</div>
					</div>
				</div>
				<br>
				  <?php } ?>
				<p class="head-tagline">Weight Loss Links</p>
				<div class="valid-feedback" id="linkHeartSuccess"><h5 class="text-center">Added favorite link.</h5></div>
				<div class="valid-feedback" id="addedHeartAlready"><h5 class="text-center">You already favorite this link.</h5></div>
				<div class="valid-feedback" id="linkHeartError"><h5 class="text-center">Login first before you add this link as favorite.</h5></div>
                <div id="ajaxBg" style="display:none"></div>
				<img src="<?php echo base_url();?>images/loading.gif" id="ajaxload" />
				<div class="upcoming-links" id="linkscontainer" style="display:none"> 
					
					<table id="allLinksTable" class="table table-striped table-bordered" style="width:100%;display:none;">
						<thead>
							<tr>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody id="tbodyallLinksTable">
							
						</tbody>
					</table>
					<div style="display:none" id="nolinks" class="alert alert-light" role="alert">
						<h4>No data to be displayed.</h4>
					</div>
				</div>    
            </div>    
			
        </main>   
    </div>  
	<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="signInModal">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title" id="exampleModalLabel">Register Now</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body">
			<form class="needs-validation" id="addUserForm" role="form" method="POST" novalidate>
			  <div class="valid-feedback" id="userAddSuccess"><h5 class="text-center">User has been created.</h5></div>
			  <div class="invalid-feedback" id="userAddError"><h5 class="text-center">Error in creating the user.</h5></div>
			  <!--<center><img src="images/user-pic.png" id="dispImgProfile" style="max-width:100px; width:100%; height:auto;"></center>
			  <div class="form-group">
				<label for="imgfile" class="control-label">Profile Picture</label>
				<input type="file" name="imgfile" id="imgfile" required>
				<div class="invalid-feedback errorImgPic">Please provide a picture of yours.</div>
			  </div>-->
			  <div class="form-group">
				<label for="uname" class="control-label">Username</label>
				<input type="text" class="form-control" name="uname" id="uname" required>
				<div class="invalid-feedback">Please provide a username.</div>
			  </div>
			  <div class="form-group">
				<label for="upassword" class="control-label">Password</label>
				<input type="password" class="form-control" name="upassword" id="upassword" required>
				<div class="invalid-feedback">Please provide a password.</div>
			  </div>
			  <div class="form-group">
				<label for="cpassword" class="control-label">Confirm Password</label>
				<input type="password" class="form-control" name="cpassword" id="cpassword" required>
				<div class="invalid-feedback">Please provide a confirm password.</div>
				<div class="invalid-feedback errorPass">Passwords does not match.</div>
			  </div>
			  <div class="form-group">
				<label for="fname" class="control-label">Full Name</label>
				<input type="text" class="form-control" name="fname" id="fname" required>
				<div class="invalid-feedback">Please provide your full name.</div>
			  </div>
			  <div class="form-group">
				<label for="email" class="control-label">Email Address</label>
				<input type="email" class="form-control" name="email" id="email" required>
				<div class="invalid-feedback">Please provide valid email address.</div>
			  </div>
			  <div class="form-group">
				<label for="bday" class="control-label">Date of Birth</label>
				<input type="date" class="form-control" name="bday" id="bday" required>
				<div class="invalid-feedback">Please provide your birthday.</div>
			  </div>
			  <div class="form-group">
                   <label for="bio" class="control-label">Biography</label>
                   <textarea type="text" class="form-control" name="bio" id="bio" rows="3" required></textarea>
				<div class="invalid-feedback">Please provide your biography.</div>
              </div>	
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-dark">SIGN IN</button>
		  </div>
		  </form>
		</div>
	  </div>
	</div>
	<div class="modal fade" id="registerSuccess" tabindex="-1" role="dialog" aria-labelledby="registerSuccess">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body">
			<div class="valid-feedback" style="display:block" id="userAddSuccess"><h5 class="text-center">Registration has been completed. Please check your email to verify your account.</h5></div>
		  </div>
		  <div class="modal-footer">
		  </div>
		  </form>
		</div>
	  </div>
	</div>