<style>
	#content{max-height: 540px;    padding: 5px;}
	#mian{    padding-bottom: 0;}
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	#userTable_filter{float:right}
	.width25{width:20px !important;}
	#userTable tbody tr td button i.fa{font-size:18px}
	#userTable tbody tr td{vertical-align:middle;word-break: break-all;}
</style>
<div class="container">
    <main id="mian">
		<div id="content">
			<div class="content-holder">
				<div class="row">
					<div class="col-lg-3 col-xl-3 col-md-3 col-sm-3 col-xs-3">
						<div id="chat-rooms">
							<h2>INKORG</h2>
							<div class="chat-room-holder">
								<ul id="allChatRoom">	

								</ul>
							</div>
						</div>
					</div>
					<div class="col-lg-9 col-xl-9 col-md-9 col-sm-9 col-xs-9">
						<div id="chat-msg">
							<!--<div class="row">
								<div class="col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11">
									<div class="alert alert-dark" role="alert">
										<div class="row">
											<div class="col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1" style="    padding: 0 0 0 10px;">
												<img src="http://localhost/bloom/images/logo.jpg">
											</div>
											<div class="col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11">
												<p class="chat-text">KRAMPANFALL</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1">
									<p class="chat-time">20:20</p>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1">
									<p class="chat-time">20:20</p>
								</div>
								<div class="col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11">
									<div class="alert alert-secondary" role="alert">
										<div class="row">
											<div class="col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11">
												<p class="chat-text">KRAMPANFALL</p>
											</div>
											<div class="col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1" style="    padding: 0 10px 0 0">
												<img src="http://localhost/bloom/images/logo.jpg">
											</div>
										</div>
									</div>
								</div>
							</div>
							-->
						</div>
						<div id="chat-area" style="display:none">
							<form method="post" id="addChatMessage" role="form">
								<div class="row">
									<div class="col-lg-10 col-xl-10 col-md-10 col-sm-10 col-xs-10">
										<div class="form-group">
											<textarea class="form-control" name="addChat" id="addChat" rows="2" required=""></textarea>
											<input type="hidden" id="myChatUserID">
											<input type="hidden" id="room_id">
											<input type="hidden" id="my_room_id">
										</div>
									</div>
									<div class="col-lg-2 col-xl-2 col-md-2 col-sm-2 col-xs-2">
										<div class="form-group">
											<button type="submit" class="btn btn-block btn-dark" type="submit">SKICKA</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>   
</div>  

