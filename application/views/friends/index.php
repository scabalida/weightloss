<style>
	#linkTable tbody tr td{vertical-align:middle}
	#approvelinkTable tbody tr td{vertical-align:middle}
	#linkTable tbody tr td button i.fa{font-size:20px}
	#approvelinkTable tbody tr td button i.fa{font-size:20px}
	.widget{    border: 1px solid #999;}
</style>
<div class="container" onload="">
    <main id="mian">
		<div id="content">
			<div class="invalid-feedback" id="editUserPassError"><h5 class="text-center">Username and Passord unsuccessfully updated.</h5></div>
			<div class="invalid-feedback" id="editUserPassNotMatch"><h5 class="text-center">Old Passord does not match.</h5></div>
			<div class="valid-feedback" id="editUserPassSuccess"><h5 class="text-center">Username and Passord successfully updated</h5></div>
			<div class="content-holder">
				<h4>Friends List</h4>
				<div class="row" style="margin-right:0;margin-left:0;">
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div id="listOfFriends" class="row" style="display:none">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<div class="img-friend-container">
											<img src="http://localhost/bloom/images/uploads/3/XF2.jpg">
										</div>
									</div>
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<p style="font-size:3vh;margin-bottom: 5px;margin-top: 10px;">
											<strong style="display: block;    text-align: center;">John Wick</strong>
										</p>
										<p style="display:block;text-align:center">sample@email.com</p>
									</div>
								</div>
							</div>
						</div>
						<div id="noListOfFriends" class="alert alert-light" role="alert" style="display:none">
							<h4>No data to be displayed.</h4>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="widget">
							<h2>SEARCH A USER</h2>
							<div class="widget-holder">
								<div class="tab-content">
									<div class="tab-content">
										<form id="searchUserForm" method="post">
											<div class="input-group mb-3">
												<input class="form-control" id="getUserFullName" placeholder="Enter Full Name">
												<div class="input-group-append">
													<button class="btn btn-dark" type="submit" style="border-radius: 0 4px 4px 0;"><i class="fa fa-search" style="font-size: 17px;"></i></button>
												</div>
											</div>
										</form>
										<div class="tab-content-holder" style="padding: 0;"> 
											<ul class="popular-list" id="searchResult">
												<li>
													<div class="left-col">
														<h4>NO DATA.</h4>
													</div>  
												</li>  
											</ul>
										</div>  
									</div>
								</div>
							</div>    
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>   
</div>  

    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
   <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/user_info.js"></script>    
	<script src="<?php echo base_url();?>js/script.js"></script>  
    <script src="<?php echo base_url();?>js/notif.js"></script> 
    <script src="<?php echo base_url();?>js/chat.js"></script> 
    <script src="<?php echo base_url();?>js/friends.js"></script> 

</body>
</html>