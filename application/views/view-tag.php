	<div class="container" id="iframe-container" style="display:none">
		<iframe id="iframeSRC" src="" frameborder="0" target="_top" allow="encrypted-media" allowfullscreen  style="width:100%;height:600px;"></iframe>
	</div>
	<div class="container" id="home-container">
        <main id="mian">
            <aside id="sidebar">
				<?php if($this->session->userdata('loggedin') != TRUE){ ?>
				<div class="widget">
					<h2>LOG IN NOW</h2>
					<div class="widget-holder">
						<form class="form-horizontal"  data-toggle="validator"  id="loginForm" role="form" method="POST">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Username" id="username" name="inputUsername" required>
                            </div>
							<div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" id="password" name="inputPassword" required>
                            </div>
							<div class="invalid-feedback text-center loginerror">Username or Password is invalid.</div><br/>
                            <button type="submit" class="btn btn-block btn-dark btn-md">SIGN IN</button>
                        </form>
					</div> 
				</div> 
				<?php } ?>
                <div class="widget">
                    <h2>ALL USERS HAVE LOST</h2>
                    <div class="widget-holder">
						<h5 class="text-center" style="color:#007bff"><span id="totWeightLossUsers"></span> lbs</h5>
                    </div>    
                </div>
                <div class="widget">
                    <h2>MOST POPULAR</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><a href="#vecka">DAY</a></li>
                            <li><a href="#manad">MONTH</a></li>
                            <li><a href="#ar">YEAR</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="vecka">
                                <div class="tab-content">
									<div  id="favoritedAjaxload"><img src="<?php echo base_url();?>images/loading.gif" style="width:70px;display:block;margin:30px auto;" /></div>
                                    <div id="favoriteDayContainer" style="display:none">
                                        <div class="tab-content-holder">
                                            <div class="side-post-r">
												<div class="post">
													<a style="cursor:pointer;" id="favored_link_url">
														<img id="favored_link_image">
														<div class="p-caption">
															<div class="like-tag"><i class="fa fa-heart"></i> <span  id="favored_link_hearted"></span></div>
															<p id="favored_link_title"></p>
														</div>  
													</a>      
												</div>  
                                            </div>    
                                        </div> 
										<div style="display:none" id="noFavoriteDay" class="alert alert-light" role="alert">
											<h5 class="text-center">No data to be displayed.</h5>
										</div>
                                    </div>
                                </div>
                            </div>
                            <div id="manad" style="display:none">
                                <div class="tab-content-holder">
									<div class="side-post-r">
                                        <div class="post">
                                            <a style="cursor:pointer;" id="month_favored_link_url">
                                                <img id="month_favored_link_image">
                                                <div class="p-caption">
                                                    <div class="like-tag"><i class="fa fa-heart"></i> <span  id="month_favored_link_hearted"></span></div>
                                                    <p id="month_favored_link_title"></p>
                                                </div>  
                                            </a>      
                                        </div>  
                                    </div> 
                                </div>
								<div style="display:none" id="noFavoriteMonth" class="alert alert-light" role="alert">
									<h5 class="text-center">No data to be displayed.</h5>
								</div>
                            </div>
                            <div id="ar"  style="display:none">
                                <div class="tab-content-holder">
                                    <div class="side-post-r">
                                        <div class="post">
                                            <a style="cursor:pointer;" id="year_favored_link_url">
                                                <img id="year_favored_link_image">
                                                <div class="p-caption">
                                                    <div class="like-tag"><i class="fa fa-heart"></i> <span  id="year_favored_link_hearted"></span></div>
                                                    <p id="year_favored_link_title"></p>
                                                </div>  
                                            </a>      
                                        </div>  
                                    </div> 
								</div>   
								<div style="display:none" id="noFavoriteYear" class="alert alert-light" role="alert">
									<h5 class="text-center">No data to be displayed.</h5>
								</div>
                            </div>
                        </div>
                    </div>    
                </div> 
                <!--<div class="widget">
                    <h2>OUR TOP USERS</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><h3 style="margin:5px;">Username</h3></li>
                            <li><h3 style="margin:5px;">Points</h3></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tradar">
                                <div class="tab-content">
                                    <div id="populara">
                                        <div class="tab-content-holder" style="padding: 0;"> 
                                            <ul class="popular-list" id="rankUserPoints" style="display:none">
                                                 
                                            </ul>
											<div style="display:none" id="noRankUser" class="alert alert-light" role="alert">
												<h5 class="text-center">No data to be displayed.</h5>
											</div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>-->
				<div class="widget">
                    <h2>WEIGHT LOSS LEADERS</h2>
                    <div class="widget-holder">
                        <ul class="tabset">
                            <li><h3 style="margin:5px;">Username</h3></li>
                            <li><h3 style="margin:5px;">Weight Loss</h3></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tradar-wl">
                                <div class="tab-content">
                                    <div id="populara-wl">
                                        <div class="tab-content-holder" style="padding: 0;"> 
                                            <ul class="popular-list" id="topWeightLoss" style="display:none">
                                                 
                                            </ul>
											<div style="display:none" id="notopWeightLoss" class="alert alert-light" role="alert">
												<h5 class="text-center">No data to be displayed.</h5>
											</div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </aside>
            <div id="content">
				<p class="head-tagline">Kommande l&#228;nkars</p>
				<div class="valid-feedback" id="linkHeartSuccess"><h5 class="text-center">Added favorite link.</h5></div>
				<div class="valid-feedback" id="addedHeartAlready"><h5 class="text-center">You already favorite this link.</h5></div>
				<div class="valid-feedback" id="linkHeartError"><h5 class="text-center">Login first before you add this link as favorite.</h5></div>
                <div id="ajaxBg" style="display:none"></div>
				<img src="<?php echo base_url();?>images/loading.gif" id="ajaxload" />
				<input type="hidden" id="tag_name" value="<?php echo $tag;?>">
				<input type="hidden" id="baseURL" value="<?php echo base_url();?>">
				<div class="upcoming-links" id="linkscontainer" style="display:none"> 
					<table id="allLinksTable" class="table table-striped table-bordered" style="width:100%;display:none;">
						<thead>
							<tr>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody id="tbodyallLinksTable">
							
						</tbody>
					</table>
					<div style="display:none" id="nolinks" class="alert alert-light" role="alert">
						<h4>No data to be displayed.</h4>
					</div>
				</div>    
            </div>    
			
        </main>   
    </div> 
    <!--**********-->
    <!--JavaScript-->
    <!--**********-->
    <!--Plugin & Libs-->
    <script src="<?php echo base_url();?>js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>js/popper.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/login.js"></script>
	    <script src="<?php echo base_url();?>js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>js/animate.js"></script>
    <script src='http://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js'></script>
    <!--Custom JavaScript-->
    <script src="<?php echo base_url();?>js/user.js"></script>  
    <script src="<?php echo base_url();?>js/login.js"></script>  
    <script src="<?php echo base_url();?>js/script.js"></script> 

	<script>
		var allLinksDataTable = $('#allLinksTable').DataTable();
		(function() {
			viewTag();
			favoredToday();
			favoredMonth();
			favoredYear();
			//rankedUsers();
			topWeightLoss();
			getAllTotalWeightLoss();
			$("#loginForm").submit(function(e) {
				e.preventDefault();
						 
				$.ajax({
					type: "POST",
					url: "../../login/loginNow",
					dataType: "json",
					data:  $(this).serialize() ,
					success:
						function(data) {
							console.log(data);
							if(data == "true"){
								window.location.reload();
								console.log("true");
								$(".loginerror").hide();
							}
							else if(data == "false"){
								console.log("false");
								$(".loginerror").show();
							}
						},
					error:
						function(data){
							$(".loginerror").show();		
						}
				});
			});
		})();	
		function sortByKey(array, key) {
			return array.sort(function(a, b) {
				var x = a[key]; var y = b[key];
				return ((x > y) ? -1 : ((x < y) ? 1 : 0));
			});
		}
		function topWeightLoss(){
			//var key = 
			   $.ajax({
				type: "GET",
				url: "../../user/topWeightLoss",
				dataType: "json",
				success:
					function(data) {
						console.log();
						var newArray = sortByKey(data,"my_weightloss");
						if(newArray.length > 0){
							$("#topWeightLoss").empty();
							var zxc = 0;
							if(newArray.length > 9){zxc = 10}else{newArray.length = zxc;}
							for(var x=0;x < zxc;x++){
								$("#topWeightLoss")
									.append($('<li>')
										.attr('style', "padding: 18px 50px 18px 40px;")
										.append($('<div>')
											.attr('class', "left-col")
											.append($('<div>')
												.append(newArray[x].u_username)
											)
										)
										.append($('<div>')
											.attr('class', "right-col")
											.append($('<div>')
												.append(newArray[x].my_weightloss+" lbs")
											)
										)
									);
							}
							$("#topWeightLoss").show();
							$("#notopWeightLoss").hide();
							
						}else{
							$("#topWeightLoss").hide();
							$("#notopWeightLoss").show();
						}
					},
				error:
				function(data){
					console.log("false");		
				}
			});	
		}
		function getAllTotalWeightLoss(){
			$.ajax({
				type: "GET",
				url: "../../user/getAllTotalWeightLoss",
				dataType: "json",
				success:
					function(data) {
						$('#totWeightLossUsers').html(data);

					},
				error:
				function(data){
					console.log("false");		
				}
			});
		}
		function applyPostControl(x,id,hearted){
			if(x == 1){
				return $('<div>')
					.attr('class', "post-controls")
					.append($('<ul>')
						.attr('class', "meta last")
						.append($('<i>')
							.attr('style', "font-size: 16px;    padding: 0 5px;")
							.append($('<a>')
								.attr('onclick', "heartThisLink("+id+")")
								.attr('style', "cursor:pointer")
								.append(chckHeart(hearted))
							)
						)
						.append($('<i>')
							.attr('style', "font-size: 16px;    padding: 0 5px;")
							.append($('<a>')
								.attr('onclick', "")
								.attr('href', "#")
								.append($('<i>')
									.attr('class', "fa fa-pencil")
								)
							)
						)
						.append($('<i>')
							.attr('style', "font-size: 16px;    padding: 0 5px;")
								.append($('<a>')
									.attr('onclick', "")
									.attr('href', "#")
									.append($('<i>')
									.attr('class', "fa fa-times")
								)
							)
						)
					);
			}else{
				return "";
			}
		}
		function targetToIframe(id){
			console.log(id);
			$.ajax({
					type: "POST",
					url: "../../user/getThisLink",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							console.log(data[0].link);
							document.getElementById('iframeSRC').src=data[0].link;
							$("#home-container").hide();
							$("#iframe-container").show();
						},
					error:
					function(data){
						console.log("false");		
					}
				});
		}
		function chckHeart(x){
			if(x == 1){
				return $('<i>').attr('class', "fa fa-heart").attr('style', "color:red");
			}else{
				return $('<i>').attr('class', "fa fa-heart");
			}
		}
		function splitTag(tags){
			var baseURL = document.getElementById("baseURL").value;
			var newTag = tags.replace(/#/g, "");
			var newTag = newTag.replace(/\s/g, "");
			var res = newTag.split(",");
			var tagsData = [];
			
			for(x = 0; x < res.length; x++){
				var d = "<a href="+baseURL+"tags/view/"+res[x]+"> #"+res[x]+" </a>";
				tagsData.push(d);
			}
			return tagsData;
		}
		function viewTag(){
			var tag = document.getElementById("tag_name").value;
			allLinksDataTable.destroy();
			$.ajax({
				type: "POST",
				url: "../getThisTag",
				dataType: "json",
				data:  {tag:tag},
				success:
					function(data) {
						//console.log(data);
						if(data.length > 0){
						$("#tbodyallLinksTable").empty();
						for(var x=0;x < data.length;x++){
							$("#allLinksTable").find('tbody')
								.append($('<tr>')
									.append($('<td>')
										.append($('<article>')
											.attr('class', "post")
											.append($('<div>')
												.attr('class', "post-header")
												.append(applyPostControl(data[x].loggedin,data[x].u_id,data[x].hearted_already)
												)
												.append($('<div>')
													.attr('class', "post-meta")
													.append($('<ul>')
														.attr('class', "meta")
														.append($('<i>')
															.append($('<a>')
																.attr('onclick', "")
																.append($('<i>')
																	.attr('class', "fa fa-globe")
																)
																.append("&nbsp;")
																.append($('<strong>')
																	.append(data[x].link_title)
																	.append("&ensp;")
																	.append("&emsp;")
																)
															)
														)
														.append($('<i>')
															.append($('<a>')
																.attr('onclick', "")
																.append($('<i>')
																	.attr('class', "fa fa-exclamation-triangle")
																)
																.append("&nbsp;")
																.append(splitTag(data[x].tags))
															)
														)
													)
												)
											)
											.append($('<div>')
												.attr('class', "post-header")
												.append($('<div>')
													.attr('class', "image-holder")
													.append($('<img>')
														.attr('src', "https://api.site-shot.com/?url="+data[x].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=250")
													)
												)
												.append($('<div>')
													.attr('class', "text")
													.append($('<p>')
														.append(data[x].description)
													)
													.append($('<button>')
														.attr('type', "button")
														.attr('onclick', "targetToIframe("+data[x].u_id+")")
														.attr('class', "btn btn-default pull-left")
														.append("VIEW")
													)
													.append($('<span>')
														.attr('class', "by")
														.append($('<strong>')
															.append("Tipsad av:")
														)
														.append("&nbsp;")
														.append(data[x].u_username)
													)
												)
											)
										)
									)
								);
						}
						$("#allLinksTable").show();
						$("#nolinks").hide();
						$("#allLinksTable thead").hide();
						$("#allLinksTable tbody td").css({"padding": "0","border": "none"});
						$("#allLinksTable tbody td .post p").css({"max-width": "88%","word-break": "break-all"});
						allLinksDataTable = $('#allLinksTable').DataTable({
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 5
						});
						setTimeout(function(){ $("#ajaxload").hide();$("#ajaxBg").hide();$("#linkscontainer").show(); }, 3000);
					}else{
						setTimeout(function(){ $("#ajaxload").hide();$("#ajaxBg").hide();$("#linkscontainer").show(); }, 3000);
						$("#allLinksTable").hide();
						$("#nolinks").show();
					}
					},
				error:
				function(data){
					console.log("false");		
				}
			});	
		}
		function favoredToday(){
			$.ajax({
					type: "GET",
					url: "../../favorite_links/getAllFavoredDay",
					dataType: "json",
					success:
						function(data) {
							//console.log(data);
							//console.log(data.data[0].link_title);
							if(data.data.length > 0 ){
								$("#favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
								$("#favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
								$("#favored_link_title").html(data.data[0].link_title);
								$("#favored_link_hearted").html(data.count);
								
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer").show(); }, 3000);
							}else{
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer .tab-content-holder").hide();$("#noFavoriteDay").show(); }, 3000);
							}
						},
					error:
					function(data){
						//console.log("false");		
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer").show();$("#favoriteDayContainer .tab-content-holder").hide();$("#noFavoriteDay").show(); }, 3000);
					}
				});
		}
		function favoredMonth(){
			$.ajax({
					type: "GET",
					url: "../../favorite_links/getAllFavoredMonth",
					dataType: "json",
					success:
						function(data) {
							//console.log(data);
							//console.log(data.data[0].link_title);
							if(data.data.length > 0 ){
								$("#month_favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
								$("#month_favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
								$("#month_favored_link_title").html(data.data[0].link_title);
								$("#month_favored_link_hearted").html(data.count);
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad").show(); }, 3000);
							}else{
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad .tab-content-holder").hide();$("#noFavoriteMonth").show(); }, 3000);
							}
						},
					error:
					function(data){
						//console.log("false");		
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad").show();$("#manad .tab-content-holder").hide();$("#noFavoriteMonth").show(); }, 3000);
					}
				});
		}
		function favoredYear(){
			$.ajax({
					type: "GET",
					url: "../../favorite_links/getAllFavoredYear",
					dataType: "json",
					success:
						function(data) {
							//console.log(data);
							//console.log(data.data[0].link_title);
							if(data.data.length > 0){
								$("#year_favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
								$("#year_favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
								$("#year_favored_link_title").html(data.data[0].link_title);
								$("#year_favored_link_hearted").html(data.count);
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar").show(); }, 3000);
							}else{
								setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar .tab-content-holder").hide();$("#noFavoriteYear").show(); }, 3000);
							}
						},
					error:
					function(data){
						//console.log("false");		
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar").show();$("#ar .tab-content-holder").hide();$("#noFavoriteYear").show(); }, 3000);
					}
				});
		}
		function rankedUsers(){
		   $.ajax({
				type: "GET",
				url: "../../user/rankUserPoints",
				dataType: "json",
				success:
					function(data) {
						if(data.length > 0){
							$("#rankUserPoints").empty();
							for(var x=0;x < data.length;x++){
								$("#rankUserPoints")
									.append($('<li>')
										.attr('style', "padding: 18px 50px 18px 40px;")
										.append($('<div>')
											.attr('class', "left-col")
											.append($('<div>')
												.append(data[x].u_username)
											)
										)
										.append($('<div>')
											.attr('class', "right-col")
											.append($('<div>')
												.append(data[x].u_points)
											)
										)
									);
							}
							$("#rankUserPoints").show();
							$("#noRankUser").hide();
							
						}else{
							$("#rankUserPoints").hide();
							$("#noRankUser").show();
						}
					},
				error:
				function(data){
					console.log("false");		
				}
			});	
		}
	</script>
	
</body>
</html>	