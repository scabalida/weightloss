<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class chat extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_chat_room');
		$this->load->model('m_chat_messages');
    }
    

		public function index() {

			$this->load->view('header');
			$this->load->view('chat/chat');
			$this->load->view('chat/footer');

		}
		public function addChatMessage() {

			$data = array (
				'user_id'   			=> $this->session->userdata('u_id'),
				'your_chat_user_id'   	=> $this->input->post('myChatUserID'),
				'chat_message'   		=> $this->input->post('getMsg'),
				'chat_date'   			=> date('Y-m-d H:i:s')
			);
			$data2 = array (
				'chat_seen'   => 1
			);
		//print_r($data2);
		//echo $this->input->post('chtrmID');
			$query = $this->m_chat_messages->save($data);
			$query2 = $this->m_chat_room->save($data2,$this->input->post('chtrmID'));
		
			if($query && $query2){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}

		}
		public function getMyChatRooms() {

			$query = $this->m_chat_room->getMyChatRooms();
		
			echo json_encode($query);

		}
		public function seenChat() {

			$data2 = array (
				'chat_seen'   => 0
			);
			$query2 = $this->m_chat_room->save($data2,$this->input->post('id'));
			echo json_encode($query2);

		}
		
		public function getMyChatMessage() {
			$chat_id = $this->input->post('chat_id');
			
			$getAllChat = $this->m_chat_messages->getMyChatMessage($chat_id);
			
			
			foreach ($getAllChat as $key => $part) {
					   $sort[$key] = strtotime($part['chat_date']);
				  }
			array_multisort($sort, SORT_ASC, $getAllChat);

			echo json_encode($getAllChat);

		}

	}