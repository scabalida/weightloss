<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class category extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_linkpost');
    }
    
		public function view($category){

			$this->load->view('header');
			$this->load->view('view-category',array("category"=>$category));

		}
		
		public function getThisCategory(){
			$category = $this->input->post('category');
			$data = $this->m_linkpost->viewLinks($category);
			echo json_encode($data);
			
		}
		
	}