<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class favorite_links extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_favorite_links');
    }

	public function my_favorites(){
		$this->load->view('header');
		$this->load->view('profile/favorites');
	}
	public function testtest(){
		
		$this->m_favorite_links->testthis();
	
	}
	public function getAllFavoredDay(){
	
			$dayArray = [];
			$data= $this->m_favorite_links->getAllFavored();
			foreach ($data as $row)
			{
				$thisYear = date("Y");
				$thisMonth = date("m");
				$thisDay = date("d");
				$year = date("Y", strtotime($row->favorited_date));
				$month = date("m", strtotime($row->favorited_date));
				$day = date("d", strtotime($row->favorited_date));
				if($thisDay == $day){
					
					array_push($dayArray,$row->link_id);
				}
			}
			$count=array_count_values($dayArray);
			arsort($count);
			$keys=array_keys($count);
			
			$num_favored = $count[$keys[0]];
			
			$getLinkData = $this->m_favorite_links->mostLinkFavored($keys[0]);
			
			echo json_encode( array("count" => $num_favored, "data"=>$getLinkData));
			
	}
	public function getAllFavoredMonth(){
	
			$dayArray = [];
			$data= $this->m_favorite_links->getAllFavored();
			foreach ($data as $row)
			{
				$thisYear = date("Y");
				$thisMonth = date("m");
				$thisDay = date("d");
				$year = date("Y", strtotime($row->favorited_date));
				$month = date("m", strtotime($row->favorited_date));
				$day = date("d", strtotime($row->favorited_date));
				if($thisMonth == $month){
					
					array_push($dayArray,$row->link_id);
				}
			}
			$count=array_count_values($dayArray);
			arsort($count);
			$keys=array_keys($count);
			
			$num_favored = $count[$keys[0]];
			
			$getLinkData = $this->m_favorite_links->mostLinkFavored($keys[0]);
			
			echo json_encode( array("count" => $num_favored, "data"=>$getLinkData));
			
	}
	public function getAllFavoredYear(){
	
			$dayArray = [];
			$data= $this->m_favorite_links->getAllFavored();
			foreach ($data as $row)
			{
				$thisYear = date("Y");
				$thisMonth = date("m");
				$thisDay = date("d");
				$year = date("Y", strtotime($row->favorited_date));
				$month = date("m", strtotime($row->favorited_date));
				$day = date("d", strtotime($row->favorited_date));
				if($thisYear == $year){
					
					array_push($dayArray,$row->link_id);
				}
			}
			$count=array_count_values($dayArray);
			arsort($count);
			$keys=array_keys($count);
			
			$num_favored = $count[$keys[0]];
			
			$getLinkData = $this->m_favorite_links->mostLinkFavored($keys[0]);
			
			echo json_encode( array("count" => $num_favored, "data"=>$getLinkData));
			
	}
	public function getAllUserFavorites(){

			$data= $this->m_favorite_links->getAllUserFavorites();
			echo json_encode($data);
	}
	public function addUserFavoriteLink(){
			$data = array(
				'user_id'		 => $this->session->userdata('u_id'),
				'link_id'		 => $this->input->post('link_id'),
				'favorited_date' => date("Y-m-d")
			);
			
			$chk = $this->m_favorite_links->checkTheHeartedLink($data);
			
			if(count($chk) > 0){
				echo json_encode('finish');
			}else{
				$query = $this->m_favorite_links->save($data);
				if($query){
					echo json_encode('true');
				}else{
					echo json_encode('false');
				}
			}
			
	}
		// End Dashboard Class
	}