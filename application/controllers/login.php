<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class login extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_login');
    }
    
		public function index() {
			
		
			$this->load->view('header');
			$this->load->view('index');
			$this->load->view('footer');
		}
		
		public function loginNow(){
				
			$username =  $this->input->post('inputUsername');
			$password =  md5($this->input->post('inputPassword'));
			
			if($this->m_login->login($username, $password)){
				echo json_encode('true');		
			}
			else{
				echo json_encode('false');
			}
			
		}
		
		public function updaterights($all){
			$hash = "7d7edf4122b9d9be2d4af422a1817af3";
			$getID = str_replace($hash,"",$all);
			$data = array(
				'u_rights' => 1
			);
			//echo $getID;
			if($this->m_login->save($data,$getID) ){
				//redirect('login');
				redirect(base_url(),'refresh');
			}
			else{
				//redirect('login');
				redirect(base_url(),'refresh');				
			}
		}
		
		public function logout()
		{
			$this->m_login->logout();
			
		}
		
		
		// End Dashboard Class
	}