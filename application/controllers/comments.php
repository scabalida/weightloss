<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class comments extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_comments');
		$this->load->model('m_notif');
		$this->load->model('m_linkpost');
    }
    
		public function addCommentToLink() {

			$data = array(
				'link_id' => $this->input->post('link_id'),
				'link_user_id' => $this->input->post('link_user_id'),
				'comment_user_id' => $this->session->userdata('u_id'),
				'comment_text' => $this->input->post('comment'),
				'comment_date' => date('Y-m-d H:i:s')
			);
			$notif_data = array(
				'user_id' => $this->input->post('link_user_id'),
				'link_id' => $this->input->post('link_id'),
				'notif_type' => 2,
				'notif_read' => 1,
				'notif_user_id' => $this->session->userdata('u_id'),
				'notif_date' => date('Y-m-d H:i:s')
			);
			//print_r($data);
			//print_r($notif_data);
			if($this->input->post('link_user_id') != $this->session->userdata('u_id')){
				$notif_query = $this->m_notif->save($notif_data);
			}
			$query = $this->m_comments->save($data);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		
		
		}
		public function addReplyCommentToLink() {

			$data = array(
				'comment_id' => $this->input->post('comment_id'),
				'link_id' => $this->input->post('link_id'),
				'link_user_id' => $this->input->post('link_user_id'),
				'comment_user_id' => $this->session->userdata('u_id'),
				'comment_text' => $this->input->post('comment'),
				'comment_date' => date('Y-m-d H:i:s')
			);
			$notif_data = array(
				'user_id' => $this->input->post('comment_user_id'),
				'link_id' => $this->input->post('link_id'),
				'notif_type' => 5,
				'notif_read' => 1,
				'notif_user_id' => $this->session->userdata('u_id'),
				'notif_date' => date('Y-m-d H:i:s')
			);
			//print_r($data);
			//print_r($notif_data);
			if($this->input->post('comment_user_id') != $this->session->userdata('u_id')){
				$notif_query = $this->m_notif->save($notif_data);
			}
			$query = $this->m_comments->addReplyCommentToLink($data);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		
		
		}
		public function addLikesToComment() {

			$data = array(
				'comment_id' => $this->input->post('comment_id'),
				'user_id' => $this->session->userdata('u_id'),
				'link_id' => $this->input->post('link_id'),
				'liked_date' => date('Y-m-d H:i:s')
			);
			$notif_data = array(
				'user_id' => $this->input->post('comment_user_id'),
				'link_id' => $this->input->post('link_id'),
				'notif_type' => 4,
				'notif_read' => 1,
				'notif_user_id' => $this->session->userdata('u_id'),
				'notif_date' => date('Y-m-d H:i:s')
			);
			//print_r($data);
			
			if($this->input->post('comment_user_id') != $this->session->userdata('u_id')){
				$notif_query = $this->m_notif->save($notif_data);
			}	
			$query = $this->m_comments->addLikesToComment($data);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		
		
		}
		public function addLikesReplyToComment() {

			$data = array(
				'comment_id' => $this->input->post('comment_id'),
				'user_id' => $this->session->userdata('u_id'),
				'link_id' => $this->input->post('link_id'),
				'liked_date' => date('Y-m-d H:i:s')
			);
			$notif_data = array(
				'user_id' => $this->input->post('comment_user_id'),
				'link_id' => $this->input->post('link_id'),
				'notif_type' => 4,
				'notif_read' => 1,
				'notif_user_id' => $this->session->userdata('u_id'),
				'notif_date' => date('Y-m-d H:i:s')
			);
			//print_r($data);
			
			if($this->input->post('comment_user_id') != $this->session->userdata('u_id')){
				$notif_query = $this->m_notif->save($notif_data);
			}	
			$query = $this->m_comments->addLikesReplyToComment($data);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		
		
		}

		public function view_link($id) {

			$cnt = $this->m_linkpost->clickedLink($id);
			$data = array(
				'clicks' => $cnt
			);
			$this->m_linkpost->save($data,$id);
			
			$this->load->view('header');
			$this->load->view('view-link',array("link_id"=>$id));

		}
		public function countComments() {
			$id = $this->input->post('link_id');
			
			$query = $this->m_comments->countComments($id);
			$query1 = $this->m_comments->countReplyComments($id);
		
			echo json_encode(count($query) + count($query1));

		}
		public function countLikesInComment() {
			$id = $this->input->post('link_id');
			
			$query = $this->m_comments->countLikesInComment($id);
		
			echo json_encode(count($query));

		}
		public function getLinksComment() {
			$id = $this->input->post('link_id');
			
			$query = $this->m_comments->getLinksComment($id);
		
			echo json_encode($query);

		}
		public function getReplyToComment() {
			$id = $this->input->post('comment_id');
			
			$query = $this->m_comments->getReplyToComment($id);
		
			echo json_encode($query);

		}

	}