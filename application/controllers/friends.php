<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class friends extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_friends');
		$this->load->model('m_user');
		$this->load->model('m_chat_room');
    }
    
		public function index(){

			$this->load->view('header');
			$this->load->view('friends/index');

		}
		public function addFriendUser(){
			$data1 = array(
				'user_id' => $this->session->userdata('u_id'),
				'friend_id' => $this->input->post('id'),
				'status' => 0,
				'who_requested' => $this->session->userdata('u_id'),
				'friend_date' => date('Y-m-d')
			);
			$data2 = array(
				'user_id' => $this->input->post('id'),
				'friend_id' => $this->session->userdata('u_id'),
				'status' => 0,
				'who_requested' => $this->session->userdata('u_id'),
				'friend_date' => date('Y-m-d')
			);
			$data1= $this->m_friends->save($data1);
			$data2= $this->m_friends->save($data2);
			
			$getUserEmail = $this->m_user->getUserData($this->session->userdata('u_id'));
			$getFriendEmail = $this->m_user->getUserData($this->input->post('id'));
			
			if($data1 && $data2){
				$this->load->library('email');
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "localhost";
				$config['smtp_port']           = "25";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;
				
				$this->email->initialize($config);
				$this->email->from($getUserEmail[0]->u_email, "Top Secret");
				$this->email->to($getFriendEmail[0]->u_email);
				$message = "
							<html>
							<head>
							</head>
							<body>
								<p>Hi ".ucwords($getFriendEmail[0]->u_fullname).",</p>
								<p>".ucwords($getUserEmail[0]->u_fullname)." wants to be your friend.</p>
							</body>
							</html>
							";
				$this->email->subject(ucwords($getUserEmail[0]->u_fullname)." added you as a friend");
				$this->email->message($message);

				if($this->email->send()){
					echo json_encode('true');
				}
				else{
					//echo $this->email->print_debugger();
					echo json_encode('false');
				}
			}else{
				echo json_encode("false");
			}
		}
		public function searchUser(){
			$name = $this->input->post('name');
			$data= $this->m_friends->searchUser($name);
			echo json_encode($data);
		}
		public function getFriendsData(){

			$data= $this->m_friends->getAllMyFriends();
			if(count($data) > 0){
				foreach ($data as $key => $part) {
					$sort[$key] = $part['u_fullname'];
				}
				array_multisort($sort, SORT_ASC, $data);
			}
			echo json_encode($data);
		}
		public function getFriendRequest(){

			$data= $this->m_friends->getFriendRequest();
			echo json_encode($data);
		}
		public function checkFriendRequest(){
			$id = $this->input->post('id');
			$data= $this->m_friends->checkFriendRequest($id);
			echo json_encode($data);
		}
		public function confirmFriendRequest(){
			$friend_id = $this->input->post('friend_id');
			$data= $this->m_friends->confirmFriendRequest($friend_id);
			if($data){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}
		}
		public function deleteFriendRequest(){
			$friend_id = $this->input->post('friend_id');
			$data= $this->m_friends->deleteFriendRequest($friend_id);
			if($data){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}
		}
		public function addChatRoom(){
			$chk1 = array(
				'user_id' => $this->session->userdata('u_id'),
				'your_chat_user_id' => $this->input->post('friend_id')
			);
			$data1 = array(
				'user_id' => $this->session->userdata('u_id'),
				'your_chat_user_id' => $this->input->post('friend_id'),
				'chat_seen' => 0,
				'date_created' => date('Y-m-d H:i:s')
			);
			$data2 = array(
				'user_id' => $this->input->post('friend_id'),
				'your_chat_user_id' => $this->session->userdata('u_id'),
				'chat_seen' => 0,
				'date_created' => date('Y-m-d H:i:s')
			);
			$checkChatRoom= $this->m_friends->checkChatRoom($chk1);
			$base_url = base_url();
			if(count($checkChatRoom) >0 ){
				echo json_encode($base_url);
			}else{
				$data1= $this->m_chat_room->save($data1);
				$data2= $this->m_chat_room->save($data2);
				
				echo json_encode($base_url);
			}
			
		}
		
	}