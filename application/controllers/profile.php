<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class profile extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_friends');
		$this->load->model('m_user');
		$this->load->model('m_wall_status');
    }
		public function test(){
			$id = 47;
			$data['chckFriend']= $this->m_friends->chckFriend($id);
			echo count($data['chckFriend']);
		}
		public function view($id){
			
			$data['id'] = $id;
			$data['chckFriend']= count($this->m_friends->chckFriend($id));
			$this->load->view('header');
			$this->load->view('profile/view',$data);

		}
		public function getUserData(){
			$id = $this->input->post('id');
			$data= $this->m_user->getUserData($id);
			echo json_encode($data);

		}
		public function getMyWallStatus(){
			$data= $this->m_wall_status->getMyWallStatus();
			echo json_encode($data);

		}
		public function getFriendsWallStatus(){
			$id = $this->input->post('id');
			$data= $this->m_wall_status->getFriendsWallStatus($id);
			echo json_encode($data);

		}
		public function getWallStatusComments(){
			$id = $this->input->post('id');
			$data= $this->m_wall_status->getWallStatusComments($id);
			echo json_encode($data);

		}
		public function getWallStatusCommentFromComments(){
			$id = $this->input->post('id');
			$data= $this->m_wall_status->getWallStatusCommentFromComments($id);
			echo json_encode($data);

		}
		public function addWallStatus(){
			$current_weight = $this->m_wall_status->getWeightLoss();
			$data = array(
				'user_id' => $this->session->userdata('u_id'),
				'current_weight' => $current_weight[0]->weight_loss,
				'status_description' => $this->input->post('status_description'),
				'date' => date('Y-m-d H:i')
			);
			
			$query = $this->m_wall_status->save($data);
			if($query){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}
		}
		public function addWallStatusComment(){
			
			$data = array(
				'wall_status_id' => $this->input->post('wall_status_id'),
				'wall_status_user_id' => $this->input->post('wall_status_user_id'),
				'comment_user_id' => $this->session->userdata('u_id'),
				'comment_text' => $this->input->post('comment'),
				'comment_date' => date('Y-m-d H:i:s')
			);
			
			$query = $this->m_wall_status->addWallStatusComment($data);
			if($query){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}
		}
		public function addCommentToWallStatusComment(){
			
			$data = array(
				'comment_id' => $this->input->post('comment_id'),
				'wall_status_id' => $this->input->post('wall_status_id'),
				'wall_status_user_id' => $this->input->post('wall_status_user_id'),
				'comment_user_id' => $this->session->userdata('u_id'),
				'comment_text' => $this->input->post('comment'),
				'comment_date' => date('Y-m-d H:i:s')
			);
			
			$query = $this->m_wall_status->addCommentToWallStatusComment($data);
			if($query){
				echo json_encode("true");
			}else{
				echo json_encode("false");
			}
		}
	}