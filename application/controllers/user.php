<?php defined('BASEPATH') OR exit('No direct script access allowed');
	define("BASE_URL", DIRECTORY_SEPARATOR . "weightloss" . DIRECTORY_SEPARATOR);
	define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']);
	
	class user extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_user');
	  $this->load->model('m_linkpost');
	  $this->load->model('m_notif');
    }
		public function test(){
			$id = $this->session->userdata('u_id');
			$dir = "images/uploads/".$id;
			$di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
			$ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
			foreach ( $ri as $file ) {
				$file->isDir() ?  rmdir($file) : unlink($file);
			}
			return true;
		} 
		public function getAllTotalWeightLoss(){
			$data = $this->m_user->getAllTotalWeightLoss();
			echo json_encode($data);
		}
		public function getAllApproveLinksNoWarning(){
			$data= $this->m_linkpost->getAllApproveLinksNoWarning();
			echo json_encode($data);
		}
		public function topWeightLoss(){
			$data = $this->m_user->topWeightLoss();
			echo json_encode($data);
		}
		public function rankUserPoints(){
			$data = $this->m_user->getAllUserPoints();
			echo json_encode($data);
		}
		public function addUser(){
			$password = $this->input->post('upassword');
			$newPass = md5($password );
			$data = array(
				'u_bio' => $this->input->post('bio'),
				'u_username' => $this->input->post('uname'),
				'u_password' => $newPass,
				'u_fullname' => $this->input->post('fname'),
				'u_email' =>  $this->input->post('email'),
				'u_bday' =>  $this->input->post('bday'),
				'u_points' => 0,
				'u_role' => 1,
				'u_rights' => 0,
			);
			//print_r($data);
			$query = $this->m_user->save($data);
			if($query){
				$id = md5($query);
				$hash = "7d7edf4122b9d9be2d4af422a1817af3";
				$email = $this->input->post('email') ;
				$link = base_url()."login/updaterights/".$hash.$query;
				$config = array();
				$config['useragent']           = "CodeIgniter";
				$config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
				$config['protocol']            = "smtp";
				$config['smtp_host']           = "localhost";
				$config['smtp_port']           = "25";
				$config['mailtype'] = 'html';
				$config['charset']  = 'utf-8';
				$config['newline']  = "\r\n";
				$config['wordwrap'] = TRUE;

				$this->load->library('email');

				$this->email->initialize($config);
				$this->email->from("support@digitalbloom.co.uk", "Top Secret");
				$this->email->to($email);
				//$message = "Hi,<br><br>Click this link to verify your email.<br>".$link."<br><br>Thanks,<br>Support";
				$message = "Verification Link:".$link;
				$this->email->subject("Email Verification");
				$this->email->message($message);

				if($this->email->send()){
					
					echo json_encode('true');
				}
				else{
					//echo $this->email->print_debugger();
					echo json_encode('false');
				}
				
			}
			else{
				echo json_encode('false');
			}
			
		}
		
		public function editUserData(){
			$id = $this->input->post('id');
			
			$data = array(
				'u_fullname' => $this->input->post('fname'),
				'u_email' =>  $this->input->post('email'),
				'u_bday' =>  $this->input->post('bday'),
				'u_role' =>  $this->input->post('role')
			);
			//print_r($data);
			$query = $this->m_user->save($data,$id);
			if($query){
				echo json_encode('true');
			}
			else{
				echo json_encode('false');
			}
			
		}
		public function deleteuser(){
			$id = $this->input->post('id');
			
			$query = $this->m_user->delete($id);

			echo json_encode('true');

		}
		public function getThisUserData(){
			$id = $this->input->post('id');
			
			$data= $this->m_user->getUserData($id);
			echo json_encode($data);
		}
		public function viewprofile(){

			$this->load->view('header');
			$this->load->view('profile/index');
		}
		public function usermanagement(){

			$this->load->view('header');
			$this->load->view('admin/user_management');
		}
		public function changepass(){

			$this->load->view('header');
			$this->load->view('admin/changepass');
		}
		public function getUserData(){
			$id = $this->session->userdata('u_id');
			//echo $id;
			$data= $this->m_user->getUserData($id);
			echo json_encode($data);
		}
		public function getAllUsers(){
			$id = $this->session->userdata('u_id');
			//echo $id;
			$data= $this->m_user->getAllUsers($id);
			echo json_encode($data);
		}
		public function getUnameAndPass(){
			$id = $this->session->userdata('u_id');
			$pass = md5($this->input->post('editoldpass'));
			
			$query= $this->m_user->getUnameAndPass($pass,$id);
			
			if(count($query) > 0){
				echo json_encode(array('status' => "true",'user_data' => $query));
			}else{
				echo json_encode(array('status' => "false"));
			}
		}
		public function updateUnameAndPass(){
			$id = $this->session->userdata('u_id');
			$data = array(
				'u_password' => md5($this->input->post('newpassword')),
				'u_username' => $this->input->post('newusername')
			);
			
			$query = $this->m_user->save($data,$id);
			if($query){
				$setdata = array(
					'u_username' => $this->input->post('newusername')
				);
				$this->session->set_userdata($setdata);
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
			
		}
		public function addUserPostLink(){
			$decodeCategory = json_encode($this->input->post('addlinkCategory'));
			$decodeWarning = json_encode($this->input->post('addlinkWarning'));
			$data = array(
				'user_id' => $this->session->userdata('u_id'),
				'link' => $this->input->post('urlLinkPost'),
				'link_title' => $this->input->post('linkTitle'),
				'description' => $this->input->post('linkDescription'),
				'tags' => $this->input->post('linkTag'),
				'post_icon' => $this->input->post('pickValue'),
				'warning' =>  $decodeWarning,
				'category' => $decodeCategory,
				'status' => 0,
			);
			
			//print_r($data);
			$query = $this->m_linkpost->save($data);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		}
		public function updateUserProfile(){
			$id = $this->session->userdata('u_id');
			$filePath = "images/uploads/".$id;
			
			if (file_exists($filePath)) {
				$config['upload_path']  =  "images/uploads/".$id;
			} else {
				mkdir("images/uploads/".$id);
				$config['upload_path']  =  "images/uploads/".$id;
			}
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
		
			$imgPic = $this->m_user->getMyPicture();
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('imgfile'))
            {
                if ($imgPic[0]->prof_pic == NULL)
				{
					$error = array('error' => $this->upload->display_errors());
		 
					echo json_encode($error);
					echo json_encode("false");
				}
				else
				{

					$noimgdata = array(
						'u_bio' => $this->input->post('u_bio'),
						'u_fullname' => $this->input->post('u_fullname'),
						'u_bday' => $this->input->post('u_bday'),
						'u_email' => $this->input->post('u_email')
					);
					$query = $this->m_user->save($noimgdata,$id);
					if($query){
						echo json_encode(true);
					}else{
						echo json_encode(false);
					}
				}
            }else
            {
				
				if($imgPic[0]->prof_pic != "" || $imgPic[0]->prof_pic != NULL){
					$path = "images/uploads/".$id."/".$imgPic[0]->prof_pic;
					unlink(ROOT_PATH . $path);
				}
				
				$image = $this->upload->data();
				
				$data = array(
					'u_bio' => $this->input->post('u_bio'),
					'u_fullname' => $this->input->post('u_fullname'),
					'u_bday' => $this->input->post('u_bday'),
					'u_email' => $this->input->post('u_email'),
					'prof_pic'   =>  $image['file_name']
				);
				$query = $this->m_user->save($data,$id);
				if($query){
					$this->session->set_userdata($data);
					echo json_encode(true);
				}else{
					echo json_encode(false);
				}
			}
			
			
		}
		
		public function adminpanel(){
			$data['links'] = $this->m_linkpost->getLinks();
			$data['alinks'] = $this->m_linkpost->getApproveLinks();
			
			$this->load->view('header');
			$this->load->view('admin/index',$data);
			//$this->load->view('footer');
		}
		public function getThisLink(){
			$id = $this->input->post('id');
			$data= $this->m_linkpost->getThisLink($id);
			echo json_encode($data);
		}
		public function getApproveDataPost(){
			$id = $this->input->post('id');
			$data= $this->m_linkpost->get($id);
			echo json_encode($data);
		}
		public function getAllApproveLinks(){
			$data= $this->m_linkpost->getAllApproveLinks();
			echo json_encode($data);
		}
		public function getApprovePost(){
			$data= $this->m_linkpost->getApproveLinks();
			echo json_encode($data);
		}
		public function getUnapprovePost(){
			$data= $this->m_linkpost->getLinks();
			echo json_encode($data);
		}
		public function approvePost(){
			$id = $this->input->post('id');
			$user_id = $this->input->post('user_id');
			$points = $this->m_user->getUserPoints($user_id);
			$new_points = $points[0]->u_points + 10;
			
			$getFrom = $this->input->post('getFrom');
			$getTo = $this->input->post('getTo');
			$strFrom = strtotime($getFrom);
			$strTo = strtotime($getTo);
			$fromFormat = date("Y-m-d H:i:s", $strFrom);
			$toFormat = date("Y-m-d H:i:s", $strTo);
			$data = array(
				//'publish_from' => $fromFormat,
				'publish_date_time' => $toFormat,
				'status' => 1,
			);
			$pointArray = array(
				'u_points' => $new_points
			);
			//print_r($data);
			$query = $this->m_linkpost->save($data,$id);
			$query2 = $this->m_user->save($pointArray,$user_id);
			if($query && $query2){
				$geturl = $this->m_linkpost->get_by(array('u_id' => $id));
				echo json_encode($geturl);
			}else{
				echo json_encode('false');
			}
			
		}
		public function approvePostNow(){
			$id = $this->input->post('id');
			$user_id = $this->input->post('user_id');
			$points = $this->m_user->getUserPoints($user_id);
		
			$new_points = $points[0]->u_points + 10;
			$getDate = date('Y-m-d H:i:s');
			//$getFrom = date('Y-m-d H:i:s');
			//$getTo = date('Y-m-d');

			//$strFrom = strtotime($getFrom);
			//$strTo = strtotime(($getTo." "."23:59:00"));
			//$fromFormat = date("Y-m-d H:i:s", $strFrom);
			//$toFormat = date("Y-m-d H:i:s", $strTo);
			$data = array(
				'publish_date_time' => $getDate,
				'status' => 1,
			);
			$pointArray = array(
				'u_points' => $new_points
			);
			//print_r($data);
			$query = $this->m_linkpost->save($data,$id);
			$query2 = $this->m_user->save($pointArray,$user_id);
			if($query && $query2){
				$geturl = $this->m_linkpost->get_by(array('u_id' => $id));
				echo json_encode($geturl);
			}else{
				echo json_encode('false');
			}
			
		}
		public function updateUserPostLink(){
			$id = $this->input->post('id');
			$decodeCategory = json_encode($this->input->post('editlinkCategory'));
			$decodeWarning = json_encode($this->input->post('editlinkWarning'));
			$getFrom = $this->input->post('dbGetFrom');
			$getTo = $this->input->post('dbGetTo');
			$strFrom = strtotime($getFrom);
			$strTo = strtotime($getTo);
			$fromFormat = date("Y-m-d H:i:s", $strFrom);
			$toFormat = date("Y-m-d H:i:s", $strTo);
			$data = array(
				'link' => $this->input->post('urlLinkPost'),
				'link_title' => $this->input->post('linkTitle'),
				'description' => $this->input->post('linkDescription'),
				'tags' => $this->input->post('linkTag'),
				'post_icon' => $this->input->post('pickValue'),
				'warning' =>  $decodeWarning,
				'category' => $decodeCategory,
				//'publish_from' => $fromFormat,
				'publish_date_time' => $toFormat
			);
			$query = $this->m_linkpost->save($data,$id);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
			
		}
		public function approveLinksCron(){
			$query = $this->m_linkpost->getTenUnapproveLinks();

			foreach($query as $row){
				$id = $row->u_id;
				$data = array(
					'status' => 1
				);
				$this->m_linkpost->save($data,$id);
			}

		}
		public function deletepost(){
			$id = $this->input->post('id');
			
			$query = $this->m_linkpost->delete($id);

			echo json_encode('true');

		}
		public function deleteApprovepost(){
			$id = $this->input->post('id');
			
			$query = $this->m_linkpost->delete($id);

			echo json_encode('true');

		}
		public function getAllMyWeightLoss(){

			$query = $this->m_user->getAllMyWeightLoss();

			echo json_encode($query);

		}
		public function getWeightLoss(){

			$query = $this->m_user->getWeightLoss();

			echo json_encode($query);

		}
		public function getSpecificWeightLoss(){
			$id = $this->input->post('id');
			
			$query = $this->m_user->getSpecificWeightLoss($id);

			echo json_encode($query);

		}
		public function checkInputWeightLoss(){

			$query = $this->m_user->checkInputWeightLoss();

			if(count($query) > 0){
				echo json_encode(1);
			}else{
				echo json_encode(0);
			}

		}
		public function addWeightLoss(){
			$weight = $this->input->post('weightLoss');
			$query = $this->m_user->addWeightLoss($weight);

			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}

		}
		public function editWeightLoss(){
			$id = $this->input->post('id');
			$data = array(
				'weight_loss' => $this->input->post('weightLoss')
			);
			
			$query = $this->m_user->editWeightLoss($data,$id);
			if($query){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
			
		}
				public function getMyNotifs(){
			$data= $this->m_notif->getMyNotif();
			echo json_encode($data);
		}
		public function countNotReadNotif(){
			$data= $this->m_notif->countNotReadNotif();
			echo json_encode(count($data));
		}
		public function makeNotifRead(){
			$data= $this->m_notif->makeNotifRead();
			if($data){
				echo json_encode('true');
			}else{
				echo json_encode('false');
			}
		}

	}