<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class tags extends Admin_Controller {
		public function __construct() {
		parent::__construct();
		$this->load->model('m_linkpost');
    }
    
		public function view($tag){

			$this->load->view('header');
			$this->load->view('view-tag',array("tag"=>$tag));

		}
		
		public function getThisTag(){
			$tag = $this->input->post('tag');
			$data = $this->m_linkpost->viewTagLinks($tag);
			echo json_encode($data);
			
		}
		
		public function test($tags){
			
			$data = $this->m_linkpost->viewTagLinks($tags);
			echo count($data);
			echo json_encode($data);
		}
	}