 var allLinksDataTable = $('#allLinksTable').DataTable({
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 5
						});
(function() {
	'use strict';

	window.addEventListener('load', function() {
		//getapprovelink();
		getAllFavoriteLinks();
	}, false);
})();
function removeExtraCharacter(str){

	var res1 = str.replace(']', "");
	var res2 = res1.replace('[', "");
	return res2;
}
function getapprovelink(){
   $.ajax({
		type: "GET",
		url: "favorite_links/getAllUserFavorites",
		dataType: "json",
		success:
			function(data) {
				console.log(data);
				if(data.length > 0){
					$("#tbodylinktable").empty();
					for(var x=0;x < data.length;x++){

						$("#linkTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].link_title)
								)
								.append($('<td>')
									.append(data[x].description)
								)
								.append($('<td>')
									.append($('<a>')
										.attr('href', data[x].link)
										.attr('target', "_blank")
										.append(data[x].link)
									)
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].category))
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].warning))
								)
								.append($('<td>')
									.append(data[x].tags)
								)
								/*.append($('<td>')
									.append($('<button>')
										.attr('onclick', "removeLink("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)*/
								
							);
					}
					$("#linkTable").show();
					$("#nofavoritelink").hide();
					$('#linkTable').DataTable();
				}else{
					$("#linkTable").hide();
					$("#nofavoritelink").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function getAllFavoriteLinks(){
   $.ajax({
		type: "GET",
		url: "favorite_links/getAllUserFavorites",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallLinksTable").empty();
					for(var x=0;x < data.length;x++){
						$("#allLinksTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<article>')
										.attr('class', "post")
										.append($('<div>')
											.attr('class', "post-header")
											.append($('<div>')
												.attr('class', "post-controls")
												.append($('<ul>')
													.attr('class', "meta last")
													.append($('<i>')
														.attr('style', "font-size: 16px;    padding: 0 5px;")
														.append($('<a>')
															.attr('onclick', "")
															.attr('href', "#")
															.append($('<i>')
																.attr('class', "fa fa-times")
															)
														)
													)
												)
											)
											.append($('<div>')
												.attr('class', "post-meta")
												.append($('<ul>')
													.attr('class', "meta")
													.append($('<i>')
														.append($('<a>')
															.attr('onclick', "")
															.append($('<i>')
																.attr('class', "fa fa-globe")
															)
															.append("&nbsp;")
															.append($('<strong>')
																.append(data[x].link_title)
																.append("&ensp;")
																.append("&emsp;")
															)
														)
													)
													.append($('<i>')
														.append($('<a>')
															.attr('onclick', "")
															.append($('<i>')
																.attr('class', "fa fa-exclamation-triangle")
															)
															.append("&nbsp;")
															.append(data[x].tags)
														)
													)
												)
											)
										)
										.append($('<div>')
											.attr('class', "post-header")
											.append($('<div>')
												.attr('class', "image-holder")
												.append($('<img>')
													.attr('src', "https://api.site-shot.com/?url="+data[x].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=250")
												)
											)
											.append($('<div>')
												.attr('class', "text")
												.append($('<p>')
													.append(data[x].description)
												)
												.append($('<button>')
													.attr('type', "button")
													.attr('onclick', "targetToIframe("+data[x].u_id+")")
													.attr('class', "btn btn-default pull-left")
													.append("VIEW")
												)
												.append($('<span>')
													.attr('class', "by")
													.append($('<strong>')
														.append("Tipsad av:")
													)
													.append("&nbsp;")
													.append(data[x].u_username)
												)
											)
										)
									)
								)
							);
					}
					$("#allLinksTable").show();
					$("#nolinks").hide();
					$("#allLinksTable thead").hide();
					$("#allLinksTable tbody td").css({"padding": "0","border": "none"});
					$("#allLinksTable tbody td .post p").css({"max-width": "88%","word-break": "break-all"});
					allLinksDataTable.on( 'xhr', function ( e, settings, json ) {
						//console.log( 'Ajax event occurred. Returned data: ', json );
					} );
					setTimeout(function(){ $("#ajaxload").hide();$("#linkscontainer").show(); }, 3000);
				}else{
					$("#allLinksTable").hide();
					$("#nolinks").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}