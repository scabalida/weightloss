(function($){
	$("#loginForm").submit(function(e) {
		e.preventDefault();
				 
		$.ajax({
			type: "POST",
			url: "login/loginNow",
			dataType: "json",
			data:  $(this).serialize() ,
			success:
				function(data) {
					console.log(data);
					if(data == "true"){
						window.location.reload();
						console.log("true");
						$(".loginerror").hide();
					}
					else if(data == "false"){
						console.log("false");
						$(".loginerror").show();
					}
				},
			error:
				function(data){
					$(".loginerror").show();		
				}
		});
	});
})(jQuery);