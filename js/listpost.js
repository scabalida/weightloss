var captchaStat = 0;
var linkPostId = 0;
var userID = 0;
var approveDataTable = $('#approvelinkTable').DataTable();
(function() {
	'use strict';
	document.getElementById("btnEditPostLink").addEventListener('click', function() {
		
		var forms = document.getElementsByClassName('needs-validation-editlink');
		var editPostLinkForm = document.getElementById('editApprovePostForm');
		var validation = Array.prototype.filter.call(forms, function(editPostLinkForm) {
			editPostLinkForm.addEventListener('submit', function(event) {
				var pick = document.getElementsByName('editPicks');
				var pickValue;
				for (var i = 0; i < pick.length; i++) {
					if (pick[i].checked) {
						pickValue = pick[i].value;       
					}
				}

				var editlinkWarning = $("#editLinkWarning").data("kendoMultiSelect").value();
				var editlinkCategory = $("#editLinkCategory").data("kendoMultiSelect").value();
				var urlLinkPost = $("#editUrlLinkPost").val();
				var linkDescription = $("#editLinkDescription").val();
				var linkTitle = $("#editlinkTitle").val();
				var linkTag = $("#editLinkTag").val();
				//var getFrom = new Date($("#editPublishedFrom").val());
				var getTo = new Date($("#editPublishedTo").val());
				if (editPostLinkForm.checkValidity() === false) {
					//console.log("false");
					if(editlinkCategory == ""){
						$('.editlinkCategory').show();
					}else{$('.editlinkCategory').hide();}
					if(editlinkWarning == ""){
						$('.editlinkWarning').show();
					}else{$('.editlinkWarning').hide();}
					event.preventDefault();
					event.stopPropagation();
				}else{
					event.preventDefault();
						/*var yyyyFrom = getFrom.getFullYear();
						var mmFrom = getFrom.getMonth() + 1;
						var ddFrom = getFrom.getDate();
						var hoursFrom = getFrom.getHours();
						var minFrom = getFrom.getMinutes();*/
						var newTags = linkTag.replace(/\s/g, ",");
						var yyyyTo = getTo.getFullYear();
						var mmTo = getTo.getMonth() + 1;
						var ddTo = getTo.getDate();
						var hoursTo = getTo.getHours();
						var minTo = getTo.getMinutes();
						//var dbGetFrom = yyyyFrom+"-"+mmFrom+"-"+ddFrom+" "+hoursFrom+":"+minFrom+":00";
						var dbGetTo = yyyyTo+"-"+mmTo+"-"+ddTo+" "+hoursTo+":"+minTo+":00";
						$('.editlinkCategory').hide();
						$('.editlinkWarning').hide();
						/*console.log("true");
						console.log(urlLinkPost);
						console.log(linkDescription);
						console.log(linkTag);
						console.log(editlinkWarning);
						console.log(editlinkCategory);
						console.log(pickValue);*/
						$.ajax({
							type: "POST",
							url: "user/updateUserPostLink",
							dataType: "json",
							data:  {dbGetTo:dbGetTo,id:linkPostId,linkTitle:linkTitle,pickValue:pickValue,urlLinkPost:urlLinkPost,linkDescription:linkDescription,linkTag:newTags,editlinkWarning:editlinkWarning,editlinkCategory:editlinkCategory},
							success:
								function(data) {
									//console.log(data);
									if(data == "true"){
										$('#editApprovePost').modal("hide");
										$('#editPostSuccess').show();
										getapprovelink();
										setTimeout(function(){ $('#editPostSuccess').hide(); }, 3000);
										//console.log("true");
									}
									else if(data == "false"){
										//console.log("false");
										$('#editPostSuccess').hide();
									}
								},
							error:
								function(data){
									//console.log("false");		
								}
						});
					
				}
				editPostLinkForm.classList.add('was-validated');
			});
		});
		//getunapprovelink();
		
	}, false);
	getunapprovelink();
})();
$("#pills-approved-tab").click(function(){
    getapprovelink();
	
	$("#approvelinkTable").show();
	$("#pills-approved").show();
	$("#pills-unapprove").hide();
	$("#linkTable").hide();
	
});
$("#pills-unapprove-tab").click(function(){
	
	//approveDataTable.destroy();
   getunapprovelink();	
   $("#approvelinkTable").hide();
	$("#linkTable").show();
	$("#pills-approved").hide();
	$("#pills-unapprove").show();
});
function getunapprovelink(){
   $.ajax({
		type: "GET",
		url: "user/getUnapprovePost",
		dataType: "json",
		success:
			function(data) {
				var url = $("#getBaseURL").val();
				//console.log(data.length);
				if(data.length > 0){
					$("#tbodylinktable").empty();
					for(var x=0;x < data.length;x++){
						var iconURL = url+"images/"+data[x].post_icon;
						//console.log(iconURL);
						//console.log(data);
						//console.log(url);
						
						$("#linkTable").find('tbody')
							.append($('<tr>')
								/*.append($('<td>')
									.append($('<img>')
										.attr('src', iconURL)
									)
								)*/
								.append($('<td>')
									.append(data[x].link_title)
								)
								.append($('<td>')
									.append(data[x].description)
								)
								.append($('<td>')
									.append($('<a>')
										.attr('href', data[x].link)
										.attr('target', "_blank")
										.append(data[x].link)
									)
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].category))
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].warning))
								)
								
								.append($('<td>')
									.append(data[x].tags)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "editApproveLinkNow("+data[x].u_id+","+data[x].user_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-check-square")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "declineLinkPostId("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "approveLinkPostId("+data[x].u_id+","+data[x].user_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-primary text-center")
										.append($('<i>')
											.attr('class', "fa fa-clock-o")
										)
									)
								)
							);
					}
					$("#linkTable").show();
					$("#unapprovenodata").hide();
					$('#linkTable').DataTable();
				}else{
					$("#linkTable").hide();
					$("#unapprovenodata").show();
				}
			},
		error:
		function(data){
			//console.log("false");		
		}
	});		
}

function getapprovelink(){
	approveDataTable.destroy();
   $.ajax({
		type: "GET",
		url: "user/getApprovePost",
		dataType: "json",
		success:
			function(data) {
				var url = $("#getBaseURL").val();
				//console.log(data.length);
				//console.log(data);
				if(data.length > 0){
					$("#tbodyapprovelinktable").empty();
					for(var x=0;x < data.length;x++){
						var iconURL = url+"images/"+data[x].post_icon;
						//console.log(iconURL);
						
						//console.log(url);
						
						$("#approvelinkTable").find('tbody')
							.append($('<tr>')
								/*.append($('<td>')
									.append($('<img>')
										.attr('src', iconURL)
									)
								)*/
								.append($('<td>')
									.append(data[x].link_title)
								)
								.append($('<td>')
									.append(data[x].description)
								)
								.append($('<td>')
									.append($('<a>')
										.attr('href', data[x].link)
										.attr('target', "_blank")
										.append(data[x].link)
									)
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].category))
								)
								.append($('<td>')
									.append(removeExtraCharacter(data[x].warning))
								)
								.append($('<td>')
									.append(data[x].tags)
								)
								.append($('<td>')
									.append(data[x].publish_date_time)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "editApproveLink("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "removeApproveLink("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
								
							);
					}
					$("#approvelinkTable").show();
					$("#approvenodata").hide();
					
					approveDataTable = $('#approvelinkTable').DataTable( {
						  "ordering": false
						} );
				}else{
					$("#approvelinkTable").hide();
					$("#approvenodata").show();
				}
			},
		error:
		function(data){
			//console.log("false");		
		}
	});		
}
$("#approveNow").click(function(){
	var getFrom = new Date($("#publishfrom").val());
	var getTo = new Date($("#publishto").val());
	//console.log($("#publishfrom").val());
	//console.log($("#publishto").val());
	if($("#publishto").val() != ""){
		$(".pubDateFrom").hide();
		$(".pubDateTo").hide();
		var yyyyFrom = getFrom.getFullYear();
		var mmFrom = getFrom.getMonth() + 1;
		var ddFrom = getFrom.getDate();
		var hoursFrom = getFrom.getHours();
		var minFrom = getFrom.getMinutes();
		var yyyyTo = getTo.getFullYear();
		var mmTo = getTo.getMonth() + 1;
		var ddTo = getTo.getDate();
		var hoursTo = getTo.getHours();
		var minTo = getTo.getMinutes();
		var dbGetFrom = yyyyFrom+"-"+mmFrom+"-"+ddFrom+" "+hoursFrom+":"+minFrom+":00";
		var dbGetTo = yyyyTo+"-"+mmTo+"-"+ddTo+" "+hoursTo+":"+minTo+":00";
	   $.ajax({
			type: "POST",
			url: "user/approvePost",
			dataType: "json",
			data:  {id:linkPostId,user_id:userID,getFrom:dbGetFrom,getTo:dbGetTo},
			success:
				function(data) {
					//console.log(data[0].link);
					if(data[0].u_id != ""){
						$('#approvePostLinkModal').modal("hide");
						$("#getPostLinkURL").attr("src",data[0].link);
						getunapprovelink();
						setTimeout(function(){ $('#openApproveLink').modal("show"); }, 200);
					}else{
						$('#approvePostLinkModal').modal("hide");
						getunapprovelink();
					}
				},
			error:
			function(data){
				console.log("false");		
			}
		});
	}
	if($("#publishfrom").val() == ""){
		$(".pubDateFrom").show();
	}
	if($("#publishto").val() == ""){
		$(".pubDateTo").show();
	}
});
$("#deleteNow").click(function(){
   $.ajax({
		type: "POST",
		url: "user/deletepost",
		dataType: "json",
		data:  {id:linkPostId},
		success:
			function(data) {
				//console.log(data[0].link);
				if(data == "true"){
					$('#deletePostLinkModal').modal("hide");
					$('.deleteMessageSucc').show();
					setTimeout(function(){ $('.deleteMessageSucc').hide();}, 2000);
					getunapprovelink();
				}else{
					$('#deletePostLinkModal').modal("hide");
					$('.deleteMessageError').show();
					setTimeout(function(){ $('.deleteMessageError').hide(); }, 2000);
					getunapprovelink();
				}
				/*if(data == "true"){
					//setTimeout(function(){ window.location.reload(); }, 1000);
					console.log("true");
				}
				else if(data == "false"){
					console.log("false");
				}*/
			},
		error:
		function(data){
			//console.log("false");		
		}
	});
});
$("#deleteApproveNow").click(function(){
   $.ajax({
		type: "POST",
		url: "user/deleteApprovepost",
		dataType: "json",
		data:  {id:linkPostId},
		success:
			function(data) {
				//console.log(data[0].link);
				if(data == "true"){
					$('#deleteApprovePostLinkModal').modal("hide");
					$('.deleteMessageSucc').show();
					setTimeout(function(){ $('.deleteMessageSucc').hide();}, 2000);
					getapprovelink();
				}else{
					$('#deleteApprovePostLinkModal').modal("hide");
					$('.deleteMessageError').show();
					setTimeout(function(){ $('.deleteMessageError').hide(); }, 2000);
					getapprovelink();
				}
				/*if(data == "true"){
					//setTimeout(function(){ window.location.reload(); }, 1000);
					console.log("true");
				}
				else if(data == "false"){
					console.log("false");
				}*/
			},
		error:
		function(data){
			//console.log("false");		
		}
	});
});
function editApproveLinkNow(linkPostId,user_id){
	
		$.ajax({
			type: "POST",
			url: "user/approvePostNow",
			dataType: "json",
			data:  {id:linkPostId,user_id:user_id},
			success:
				function(data) {
					//console.log(data[0].link);
					$("#linkTable").show();
					$("#approvelinkTable").hide();
					if(data[0].u_id != ""){
						$("#getPostLinkURL").attr("src",data[0].link);
						getunapprovelink();
						setTimeout(function(){ $('#openApproveLink').modal("show"); }, 200);
					}else{
						getunapprovelink();
					}
				},
			error:
			function(data){
				//console.log("false");		
			}
		});	
}
function removeExtraCharacter(str){

	var res1 = str.replace(']', "");
	var res2 = res1.replace('[', "");
	return res2;
}
function removeApproveLink(id){
	linkPostId = id;
	$('#deleteApprovePostLinkModal').modal("show");
	//console.log(linkPostId);
}
function editApproveLink(id){
	linkPostId = id;
	$("#edlinkcateg").empty();
	$("#edlinkwarn").empty();
	$("#edlinkcateg").html('<select id="editLinkCategory" class="form-control" multiple="multiple" data-placeholder="Select..." required><option>FUNNY</option><option>DIET AND FOOD</option><option>SPORT</option><option>CARDIO</option><option>WEIGHT TRAINING</option><option>MOTIVATIONAL</option><option>WORKOUT MUSIC</option><option>MENTAL HEALTH</option><option>MEDICAL</option><option>TECHNOLOGY</option><option>OTHER</option></select>');
	$("#edlinkwarn").html('<select class="form-control" multiple="multiple" id="editLinkWarning" data-placeholder="Select..." required><option>18+</option><option>BAD LANGUAGE</option><option>NUDITY</option></select>');
	$.ajax({
		type: "POST",
		url: "user/getApproveDataPost",
		dataType: "json",
		data:  {id:linkPostId},
		success:
			function(data) {
				//console.log(data);
				var iconD = data.post_icon;
				var iconD = iconD.replace(".png", "");
				var iconD = iconD.replace("ico", "");
				var categoryData = JSON.parse(data.category);
				var warningData = JSON.parse(data.warning);
				$("#editLinkWarning").kendoMultiSelect().data("kendoMultiSelect").value(warningData);
				$("#editLinkCategory").kendoMultiSelect().data("kendoMultiSelect").value(categoryData);
				$('#editUrlLinkPost').val(data.link);
				$('#editLinkDescription').val(data.description);
				$('#editlinkTitle').val(data.link_title);
				$('#editLinkTag').val(data.tags);
				//$('#editPublishedFrom').data("DateTimePicker").date(new Date(data.publish_from));
				
				$('#editPublishedTo').data("DateTimePicker").date(new Date(data.publish_to));
				
				switch (iconD) {
					case '1':
						$("#pick-1").prop('checked', true);
						break;
					case '2':
						$("#pick-2").prop('checked', true);
						break;
					case '3':
						$("#pick-3").prop('checked', true);
						break;
					case '4':
						$("#pick-4").prop('checked', true);
						break;
					case '5':
						$("#pick-5").prop('checked', true);
						break;
					case '6':
						$("#pick-6").prop('checked', true);
						break;
					case '7':
						$("#pick-7").prop('checked', true);
				}
				
				$('#editApprovePost').modal("show");

			},
		error:
		function(data){
			//console.log("false");		
		}
	});
	
}
function declineLinkPostId(id){
	linkPostId = id;
	$('#deletePostLinkModal').modal("show");
	//console.log(linkPostId);
}
function approveLinkPostId(id,u_id){
	linkPostId = id;
	userID = u_id;
	$("#publishfrom").val("");
	$("#publishto").val("");
	$('#approvePostLinkModal').modal("show");
	//console.log(linkPostId);
}