var userID = 0;
(function() {
	'use strict';
	
	var addlinkCategory = $("#addlinkCategory").kendoMultiSelect({
    autoClose: false
    }).data("kendoMultiSelect");
	
    var addlinkWarning = $("#addlinkWarning").kendoMultiSelect({
    autoClose: false
    }).data("kendoMultiSelect");
	
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var addUserForm = document.getElementById('addUserForm');
		var validation = Array.prototype.filter.call(forms, function(addUserForm) {
			  addUserForm.addEventListener('submit', function(event) {
				if (addUserForm.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
					if($('#upassword').val() == $('#cpassword').val()){
						$('.errorPass').hide();
					}else{
						$('.errorPass').show();
					}
				}else{
					event.preventDefault();
					console.log("true");
					if($('#upassword').val() == $('#cpassword').val()){
						$('.errorPass').hide();
						$.ajax({
							type: "POST",
							url: "user/addUser",
							dataType: "json",
							data:  $(this).serialize() ,
							success:
								function(data) {
									console.log(data);
									if(data == "true"){
										$('#signInModal').modal("hide"); 
										$('#registerSuccess').modal("show"); 
										setTimeout(function(){ window.location.reload(); }, 3000);
										console.log("true");
									}
									else if(data == "false"){
										console.log("false");
										$('#userAddError').show();
									}
								},
							error:
								function(data){
									console.log(data);		
									console.log("false");
									$('#userAddError').show();									
								}
						});
					}else{
						$('.errorPass').show();
					}
				}
				addUserForm.classList.add('was-validated');
			  });
		});
	}, false);

	$("#btnEditUserPass").click(function(){
		var forms = document.getElementsByClassName('needs-validation-edituserpass');
		var editUserPass = document.getElementById('editUserPassForm');
		var validation = Array.prototype.filter.call(forms, function(editUserPass) {
			  editUserPass.addEventListener('submit', function(event) {
				if (editUserPass.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
					if($('#editnewpass').val() == $('#editcnewpass').val()){
						$('.errorUserPass').hide();
					}else{
						$('.errorUserPass').show();
					}
				}else{
					event.preventDefault();
					console.log("true");
					if($('#editnewpass').val() == $('#editcnewpass').val()){
						$('.errorUserPass').hide();
							$.ajax({
								type: "POST",
								url: "user/getUnameAndPass",
								dataType: "json",
								data:  $(this).serialize() ,
								success:
									function(data) {
										console.log(data);
										console.log(data.status);
										if(data.status == 'true'){
											var newUsername = $("#editusername").val();
											var newPass = $("#editnewpass").val();
												$.ajax({
													type: "POST",
													url: "updateUnameAndPass",
													dataType: "json",
													data:  {newusername:newUsername,newpassword:newPass},
													success:
														function(data) {
															console.log(data);
															if(data == 'true'){
																$("#editusername").val(newUsername);
																$('#editUserPassSuccess').show();	
																setTimeout(function(){ $('#editUserPassSuccess').hide(); }, 250);
																console.log("true");
															}
															else if(data== 'false'){
																console.log("false");
																$('#editUserPassError').show();	
																setTimeout(function(){ $('#editUserPassError').hide();	 }, 2500);
															}
														},
													error:
														function(data){
															console.log("false");
																$('#editUserPassError').show();	
																setTimeout(function(){ $('#editUserPassError').hide();	 }, 2500);									
														}
												});
										//setTimeout(function(){ window.location.reload(); }, 3000);
										console.log("true");
									}
									else if(data.status == 'false'){
										console.log("false");
										$('#editUserPassNotMatch').show();	
									}
								},
							error:
								function(data){
									console.log(data);		
									console.log("false");
									$('#editUserPassNotMatch').show();									
								}
						});
					}else{
						$('.errorUserPass').show();
					}
				}
				editUserPass.classList.add('was-validated');
			  });
		});
	});
	$("#deleteNow").click(function(){
	   $.ajax({
			type: "POST",
			url: "user/deleteuser",
			dataType: "json",
			data:  {id:userID},
			success:
				function(data) {
					//console.log(data[0].link);
					if(data == "true"){
						$('#removeUserModal').modal("hide");
						$('.deleteMessageSucc').show();
						setTimeout(function(){ $('.deleteMessageSucc').hide();}, 2000);
						getAllUsers();
					}else{
						$('#removeUserModal').modal("hide");
						$('.deleteMessageError').show();
						setTimeout(function(){ $('.deleteMessageError').hide(); }, 2000);
						getAllUsers();
					}
				},
			error:
			function(data){
				console.log("false");		
			}
		});
	});
	
	//rankedUsers();
	topWeightLoss();
	myWeightLossChart();
	getAllTotalWeightLoss();
})();

function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}
function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}
function myWeightLossChart(){
	$.ajax({
		type: "GET",
		url: "user/getWeightLoss",
		dataType: "json",
		success:
		function(data) {
			//console.log(data);
			var sumAll = 0;
			$("#chartContainer").empty();
			if(data.length > 0){
				var newArray = [];
				for(var x=0;x < data.length;x++){
					var thisArray = {
						y: parseFloat(data[x].y),
						label: reformatDate(data[x].label)
						
					};
					sumAll += parseFloat(data[x].y);
					newArray.push(thisArray);
				}
				var getSort = newArray.sort();
				var getLastArr = getSort.length - 1;
				var minX = getSort[getLastArr].y - 25;
				var maxX = getSort[0].y + 25;
				
				if(minX < 0){
					minX = 0;
				}
				$("#totalWeightLoss").html(sumAll);
				//console.log(maxX);
				//console.log(minX);
				//console.log(sumAll);
				//console.log(newArray);
				var options = {
					exportEnabled: false,
					animationEnabled: true,
					title: {
						text: "Weight Loss"
					},
					axisX: {
						title: "Date",
					},
					axisY: {
						title: "In Pounds",
						minimum: minX,
						maximum: maxX
					},
					data: [
					{
						type: "splineArea",
						dataPoints:newArray
					}
					]
				};
				$("#chartContainer").CanvasJSChart(options);
				$("#chartContainer").show();
				$("#noWeightLossData").hide();
			}else{
				$("#chartContainer").hide();
				$("#noWeightLossData").show();
			}
		},
		error:
		function(data){
			console.log("false");		
		}
	});
}
function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}
function removeUser(id){
	userID = id;
	$('#removeUserModal').modal("show");
	
}
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}
function topWeightLoss(){
	//var key = 
	   $.ajax({
		type: "GET",
		url: "user/topWeightLoss",
		dataType: "json",
		success:
			function(data) {
				console.log();
				var newArray = sortByKey(data,"my_weightloss");
				if(newArray.length > 0){
					$("#topWeightLoss").empty();
					var zxc = 0;
					if(newArray.length > 9){zxc = 10}else{newArray.length = zxc;}
					for(var x=0;x < zxc;x++){
						$("#topWeightLoss")
							.append($('<li>')
								.attr('style', "padding: 18px 50px 18px 40px;")
								.append($('<div>')
									.attr('class', "left-col")
									.append($('<div>')
										.append($('<a>')
											.attr('href', window.location.href+"profile/view/"+newArray[x].u_id)
											.append(newArray[x].u_username)
										)
									)
								)
								.append($('<div>')
									.attr('class', "right-col")
									.append($('<div>')
										.append(newArray[x].my_weightloss+" lbs")
									)
								)
							);
					}
					$("#topWeightLoss").show();
					$("#notopWeightLoss").hide();
					
				}else{
					$("#topWeightLoss").hide();
					$("#notopWeightLoss").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});	
}
function rankedUsers(){
	   $.ajax({
		type: "GET",
		url: "user/rankUserPoints",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#rankUserPoints").empty();
					for(var x=0;x < data.length;x++){
						$("#rankUserPoints")
							.append($('<li>')
								.attr('style', "padding: 18px 50px 18px 40px;")
								.append($('<div>')
									.attr('class', "left-col")
									.append($('<div>')
										.append(data[x].u_username)
									)
								)
								.append($('<div>')
									.attr('class', "right-col")
									.append($('<div>')
										.append(data[x].u_points)
									)
								)
							);
					}
					$("#rankUserPoints").show();
					$("#noRankUser").hide();
					
				}else{
					$("#rankUserPoints").hide();
					$("#noRankUser").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});	
}
function getAllTotalWeightLoss(){
	$.ajax({
		type: "GET",
		url: "user/getAllTotalWeightLoss",
		dataType: "json",
		success:
			function(data) {
				$('#totWeightLossUsers').html(data);

			},
		error:
		function(data){
			console.log("false");		
		}
	});
}
function editUserData(id){
	//console.log(id);
	userID = id;
	$.ajax({
		type: "POST",
		url: "user/getThisUserData",
		dataType: "json",
		data:  {id:userID},
		success:
			function(data) {
			//	console.log(data);
				$('#fname').val(data[0].u_fullname);
				$('#email').val(data[0].u_email);
				document.getElementById('bday').value = data[0].u_bday;
				document.getElementById('user_role').value = data[0].u_role;
				$('#editUserModal').modal("show");

			},
		error:
		function(data){
			console.log("false");		
		}
	});
}