var captchaStat = 0;
var linkPostId = 0;
 var allLinksDataTable = $('#allLinksTable').DataTable();
(function() {
	'use strict';

	window.addEventListener('load', function() {
		
		var forms = document.getElementsByClassName('needs-validation-addlink');
		var addPostLinkForm = document.getElementById('addPostLinkForm');
		var validation = Array.prototype.filter.call(forms, function(addPostLinkForm) {
			  addPostLinkForm.addEventListener('submit', function(event) {
				var pick = document.getElementsByName('picks');
				var pickValue;
				for (var i = 0; i < pick.length; i++) {
					if (pick[i].checked) {
						console.log(pick[i]);
						console.log(pick[i].value);
						pickValue = pick[i].value;       
					}
				}

				var addlinkWarning = $("#addlinkWarning").data("kendoMultiSelect").value();
				var addlinkCategory = $("#addlinkCategory").data("kendoMultiSelect").value();
				var urlLinkPost = $("#urlLinkPost").val();
				var linkTitle = $("#linkTitle").val();
				
				var chkYoutube = urlLinkPost.indexOf("watch?v=");
				var newLink = "";
				
				var linkDescription = $("#linkDescription").val();
				var linkTag = $("#linkTag").val();
				captchaCallback();
				if (addPostLinkForm.checkValidity() === false && captchaStat == 0) {
					console.log("false");
					if(addlinkCategory == ""){
						$('.addlinkCategory').show();
					}else{$('.addlinkCategory').hide();}
					if(addlinkWarning == ""){
						$('.addlinkWarning').show();
					}else{$('.addlinkWarning').hide();}
					event.preventDefault();
					event.stopPropagation();
				}else{
					event.preventDefault();
					if(captchaStat == 1){
						if(chkYoutube > 0){
							newLink = urlLinkPost.replace("watch?v=", "embed/");
						}else{
							newLink = urlLinkPost;
						}
						$('.addlinkCategory').hide();
						$('.addlinkWarning').hide();
						var newTags = linkTag.replace(/\s/g, ",");
						/*console.log("true");
						console.log(newLink);
						console.log(linkDescription);
						console.log(linkTag);
						console.log(newTags);
						console.log(addlinkWarning);
						console.log(addlinkCategory);*/
						$.ajax({
							type: "POST",
							url: "user/addUserPostLink",
							dataType: "json",
							data:  {pickValue:pickValue,linkTitle:linkTitle,urlLinkPost:newLink,linkDescription:linkDescription,linkTag:newTags,addlinkWarning:addlinkWarning,addlinkCategory:addlinkCategory},
							success:
								function(data) {
									console.log(data);
									if(data == "true"){
										$('#linkPostSuccess').show();
										setTimeout(function(){ window.location.reload(); }, 3000);
										console.log("true");
									}
									else if(data == "false"){
										console.log("false");
										$('#linkPostSuccess').hide();
									}
								},
							error:
								function(data){
									console.log("false");		
								}
						});
					}
				}
				addPostLinkForm.classList.add('was-validated');
			  });
		});
		//getunapprovelink();
		getAllApproveLinks();
		favoredToday();
		favoredMonth();
		favoredYear();
	}, false);
})();


function captchaCallback(){
		if(grecaptcha.getResponse() !== ""){
			captchaStat = 1;
		}else{
			captchaStat = 0;
		}
		console.log(captchaStat);
	}


function reloadme(){
	window.location.reload();
}
function chckHeart(x){
	if(x == 1){
		return $('<i>').attr('class', "fa fa-heart").attr('style', "color:red");
	}else{
		return $('<i>').attr('class', "fa fa-heart");
	}
}
function applyPostControl(x,id,hearted){
	if(x == 1){
		return $('<div>')
			.attr('class', "post-controls")
			.append($('<ul>')
				.attr('class', "meta last")
				.append($('<i>')
					.attr('style', "font-size: 16px;    padding: 0 5px;")
					.append($('<a>')
						.attr('onclick', "heartThisLink("+id+")")
						.attr('style', "cursor:pointer")
						.append(chckHeart(hearted))
					)
				)
				.append($('<i>')
					.attr('style', "font-size: 16px;    padding: 0 5px;")
					.append($('<a>')
						.attr('onclick', "")
						.attr('href', "#")
						.append($('<i>')
							.attr('class', "fa fa-pencil")
						)
					)
				)
				.append($('<i>')
					.attr('style', "font-size: 16px;    padding: 0 5px;")
						.append($('<a>')
							.attr('onclick', "")
							.attr('href', "#")
							.append($('<i>')
							.attr('class', "fa fa-times")
						)
					)
				)
			);
	}else{
		return "";
	}
}
function splitTag(tags){
	var newTag = tags.replace(/#/g, "");
	var newTag = newTag.replace(/\s/g, "");
	var res = newTag.split(",");
	var tagsData = [];
	//console.log(newTag);
	for(x = 0; x < res.length; x++){
		var d = "<a href="+window.location.href+"tags/view/"+res[x]+"> #"+res[x]+" </a>";
		tagsData.push(d);
	}
	return tagsData;
}
function getAllApproveLinks(){
	allLinksDataTable.destroy();
   $.ajax({
		type: "GET",
		url: "user/getAllApproveLinks",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallLinksTable").empty();
					for(var x=0;x < data.length;x++){
						$("#allLinksTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append($('<article>')
										.attr('class', "post")
										.append($('<div>')
											.attr('class', "post-header")
											.append(applyPostControl(data[x].loggedin,data[x].u_id,data[x].hearted_already)
											)
											.append($('<div>')
												.attr('class', "post-meta")
												.append($('<ul>')
													.attr('class', "meta")
													.append($('<i>')
														.append($('<a>')
															.attr('onclick', "")
															.append($('<i>')
																.attr('class', "fa fa-globe")
															)
															.append("&nbsp;")
															.append($('<strong>')
																.append(data[x].link_title)
																.append("&ensp;")
																.append("&emsp;")
															)
														)
													)
													.append($('<i>')
														.append($('<a>')
															.attr('onclick', "")
															.append($('<i>')
																.attr('class', "fa fa-exclamation-triangle")
															)
															.append("&nbsp;")
															.append(splitTag(data[x].tags))
														)
													)
												)
											)
										)
										.append($('<div>')
											.attr('class', "post-header")
											.append($('<div>')
												.attr('class', "image-holder")
												.append($('<img>')
													.attr('src', "https://api.site-shot.com/?url="+data[x].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=250")
												)
											)
											.append($('<div>')
												.attr('class', "text")
												.append($('<p>')
													.append(data[x].description)
												)
												.append($('<a>')
													.attr('type', "button")
													.attr('href', window.location.href+"comments/view_link/"+data[x].u_id)
													//.attr('onclick', "targetToIframe("+data[x].u_id+")")
													.attr('class', "btn btn-default pull-left")
													.append("VIEW")
												)
												
												.append($('<span>')
													.attr('class', "by")
													.append($('<strong>')
														.append("Tipsad av:")
													)
													.append("&nbsp;")
													.append($('<a>')
														.attr('href', window.location.href+"profile/view/"+data[x].user_id)
														.append(data[x].u_username)
													)
												)
												.append($('<strong>')
													.attr('style', "font-size: 11px")
													.append(data[x].comment_count+" kommentarer&emsp;")
												)
												.append($('<strong>')
													.attr('style', "font-size: 11px")
													.append(data[x].clicks+" klick")
												)
											)
										)
									)
								)
							);
					}
					$("#allLinksTable").show();
					$("#nolinks").hide();
					$("#allLinksTable thead").hide();
					$("#allLinksTable tbody td").css({"padding": "0","border": "none"});
					$("#allLinksTable tbody td .post p").css({"max-width": "88%","word-break": "break-all"});
					 allLinksDataTable = $('#allLinksTable').DataTable({
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 5
						});
					
					setTimeout(function(){ $("#ajaxload").hide();$("#ajaxBg").hide();$("#linkscontainer").show(); }, 3000);
				}else{
					$("#allLinksTable").hide();
					$("#nolinks").show();
					setTimeout(function(){ $("#ajaxload").hide();$("#ajaxBg").hide();$("#linkscontainer").show(); }, 3000);
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function heartThisLink(linkId){
	console.log(linkId);
	if(linkId == "" || linkId == 0|| linkId == undefined ){
		$("#linkHeartError").show();
		setTimeout(function(){ $("#linkHeartError").hide(); }, 2500);
	}else{
		$.ajax({
				type: "POST",
				url: "favorite_links/addUserFavoriteLink",
				dataType: "json",
				data:  {link_id:linkId},
				success:
					function(data) {
						getAllApproveLinks();
						if(data == "finish"){
							$("#addedHeartAlready").show();
							setTimeout(function(){ $("#addedHeartAlready").hide(); }, 1500);
						}else if(data == "true"){
							$("#linkHeartSuccess").show();
							setTimeout(function(){ $("#linkHeartSuccess").hide(); }, 1500);
						}else{
							$("#linkHeartError").show();
							setTimeout(function(){ $("#linkHeartError").hide(); }, 1500);
						}
						
					},
				error:
				function(data){
					console.log("false");		
				}
		});
	}
}
function targetToIframe(id){
	console.log(id);
	$.ajax({
			type: "POST",
			url: "user/getThisLink",
			dataType: "json",
			data:  {id:id},
			success:
				function(data) {
					console.log(data[0].link);
					document.getElementById('iframeSRC').src=data[0].link;
					$("#home-container").hide();
					$("#iframe-container").show();
				},
			error:
			function(data){
				console.log("false");		
			}
		});
}
function favoredToday(){
	$.ajax({
			type: "GET",
			url: "favorite_links/getAllFavoredDay",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					//console.log(data.data[0].link_title);
					if(data.data.length > 0 ){
						$("#favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
						$("#favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
						$("#favored_link_title").html(data.data[0].link_title);
						$("#favored_link_hearted").html(data.count);
						
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer").show(); }, 3000);
					}else{
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer .tab-content-holder").hide();$("#noFavoriteDay").show(); }, 3000);
					}
				},
			error:
			function(data){
				//console.log("false");		
				setTimeout(function(){ $("#favoritedAjaxload").hide();$("#favoriteDayContainer").show();$("#favoriteDayContainer .tab-content-holder").hide();$("#noFavoriteDay").show(); }, 3000);
			}
		});
}
function favoredMonth(){
	$.ajax({
			type: "GET",
			url: "favorite_links/getAllFavoredMonth",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					//console.log(data.data[0].link_title);
					if(data.data.length > 0 ){
						$("#month_favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
						$("#month_favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
						$("#month_favored_link_title").html(data.data[0].link_title);
						$("#month_favored_link_hearted").html(data.count);
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad").show(); }, 3000);
					}else{
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad .tab-content-holder").hide();$("#noFavoriteMonth").show(); }, 3000);
					}
				},
			error:
			function(data){
				//console.log("false");		
				setTimeout(function(){ $("#favoritedAjaxload").hide();$("#manad").show();$("#manad .tab-content-holder").hide();$("#noFavoriteMonth").show(); }, 3000);
			}
		});
}
function favoredYear(){
	$.ajax({
			type: "GET",
			url: "favorite_links/getAllFavoredYear",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					//console.log(data.data[0].link_title);
					if(data.data.length > 0){
						$("#year_favored_link_url").attr("onclick","targetToIframe("+data.data[0].u_id+")");
						$("#year_favored_link_image").attr('src', "https://api.site-shot.com/?url="+data.data[0].link+"&format=jpeg&width=600&height=500&timeout=10000&full_size=1&max_height=200")
						$("#year_favored_link_title").html(data.data[0].link_title);
						$("#year_favored_link_hearted").html(data.count);
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar").show(); }, 3000);
					}else{
						setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar .tab-content-holder").hide();$("#noFavoriteYear").show(); }, 3000);
					}
				},
			error:
			function(data){
				//console.log("false");		
				setTimeout(function(){ $("#favoritedAjaxload").hide();$("#ar").show();$("#ar .tab-content-holder").hide();$("#noFavoriteYear").show(); }, 3000);
			}
		});
}