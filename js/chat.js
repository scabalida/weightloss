var chatID = 0;
var roomID = 0;
var myRoomID = 0;
(function($){
	$.ajaxSetup({
	  cache: false
	});
	setInterval(function(){ 
		checkChatSeen();
	}, 2000);
	setInterval(function(){ 
		if(chatID != 0){
			getChatID(chatID,roomID,myRoomID);
		}
	}, 2000);
	getMyChatRooms();
	$("#addChatMessage").submit(function(e){
		e.preventDefault();
		var getMsg = $("#addChat").val();
		var myChatUserID = $("#myChatUserID").val();
		var rmID = $("#room_id").val();
		console.log(rmID);
		if(getMsg != "" || getMsg != undefined){
			$.ajax({
				type: "POST",
				url: "chat/addChatMessage",
				data:  {myChatUserID:myChatUserID,getMsg:getMsg,chtrmID:rmID},
				dataType: "json",
				success:
					function(data) {
						$("#addChat").val("");
						
					},
				error:
				function(data){
					//console.log("false");		
				}
			});
		}
	   
	});
	$("#addChat").click(function(e){
		e.preventDefault();
		chatSeened(myRoomID);
	});
	$('#addChat').keyup(function (event) {
		//event.preventDefault();
		if (event.keyCode == 13) {
			var content = this.value;  
			var caret = getCaret(this);          
			if(event.shiftKey){
				this.value = content.substring(0, caret - 1) + "\n" + content.substring(caret, content.length);
				event.stopPropagation();
				//console.log("shift+enter");
			} else {
				this.value = content.substring(0, caret - 1) + content.substring(caret, content.length);
				//console.log("enter");
				var getMsg = $("#addChat").val();
				var myChatUserID = $("#myChatUserID").val();
				var rmID = $("#room_id").val();
				console.log(rmID);
				if(getMsg != "" || getMsg != undefined){
					$.ajax({
						type: "POST",
						url: "chat/addChatMessage",
						data:  {myChatUserID:myChatUserID,getMsg:getMsg,chtrmID:rmID},
						dataType: "json",
						success:
							function(data) {
								$("#addChat").val("");
								
							},
						error:
						function(data){
							//console.log("false");		
						}
					});
				}
			}
		}
	});
})(jQuery);
function getCaret(el) { 
    if (el.selectionStart) { 
        return el.selectionStart; 
    } else if (document.selection) { 
        el.focus();
        var r = document.selection.createRange(); 
        if (r == null) { 
            return 0;
        }
        var re = el.createTextRange(), rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
        return rc.text.length;
    }  
    return 0; 
}
function checkChatSeen(){
	$.ajax({
			type: "GET",
			url: "chat/getMyChatRooms",
			cache: false,
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					if(data.length > 0){
						for(var x=0;x < data.length;x++){
							if(data[x].chat_seen == 1){
								getMyChatRooms();
							}
						}
					}	
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function getMyChatRooms(){
	$.ajax({
			type: "GET",
			url: "chat/getMyChatRooms",
			cache: false,
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					if(data.length > 0){
						$("#allChatRoom").empty();
						for(var x=0;x < data.length;x++){
							if(data[x].chat_seen == 0){
								$("#allChatRoom")
									.append($('<li>')
										.append($('<a>')
											.attr('style', "cursor:pointer")
											.attr('id', "chtRoom")
											.attr('onclick', "getChatID("+data[x].your_chat_user_id+","+data[x].room_user_ID+","+data[x].u_id+")")
											.append($('<div>')
												.attr('class', "left-col")
												.attr('style', "width:50%")
												.append($('<h4>')
													.append(data[x].my_chatmate_fullname)
												)
											)
											.append($('<div>')
												.attr('class', "right-col")
												.attr('style', "width:50%")
												.append($('<time>')
													.append(reformatDate(data[x].date_created))
												)
												.append($('<time>')
													.append("<br>"+reformatHrs(data[x].date_created))
												)
											)
										)
									);
							}else{
								$("#allChatRoom")
									.append($('<li>')
										.append($('<a>')
											.attr('style', "cursor:pointer")
											.attr('id', "chtRoom")
											.attr('onclick', "getChatID("+data[x].your_chat_user_id+","+data[x].room_user_ID+","+data[x].u_id+")")
											.append($('<div>')
												.attr('class', "left-col")
												.attr('style', "width:50%")
												.append($('<h4>')
													.append(data[x].my_chatmate_fullname)
												)
												.append($('<p>')
													.attr('style', "color: #eb9e40;")
													.append("New Message")
												)
											)
											.append($('<div>')
												.attr('class', "right-col")
												.attr('style', "width:50%")
												.append($('<time>')
													.append(reformatDate(data[x].date_created))
												)
												.append($('<time>')
													.append("<br>"+reformatHrs(data[x].date_created))
												)
											)
										)
									);
							}
						}
					}else{
						
					}	
					
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function getTheChatPic(profpic,id,baseURL){
	
	if(profpic == ""){
		return baseURL+"images/user-pic.png";
	}else{
		return baseURL+"images/uploads/"+id+"/"+profpic;
	}

}
function chatSeened(id){
	//console.log(id);
	$.ajax({
			type: "POST",
			url: "chat/seenChat",
			cache: false,
			dataType: "json",
			data:  {id:id},
			success:
				function(data) {
					//console.log(data);
					
					getMyChatRooms();
							
						
						
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function getChatID(id,room_id,my_id){
	document.getElementById("myChatUserID").value = id;
	document.getElementById("room_id").value = room_id;
	document.getElementById("my_room_id").value = my_id;
	$("#chat-area").show();
	//chatSeened(my_id);
	chatID = id;
	roomID = room_id;
	myRoomID = my_id;
	//console.log(chatID);
	//console.log(roomID);
	$.ajax({
		type: "POST",
		async: true,
		url: "chat/getMyChatMessage/?_="+Math.random(),
		data:  {chat_id:id},
		cache: false,
		dataType: "json",
		success:
			function(data) {
				//console.log(data);	
				$("#chat-msg").empty();
				if(data.length > 0){
						for(var x=0;x < data.length;x++){
							
							if(data[x].position == 0){
								$("#chat-msg")
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
											.append($('<div>')
												.attr('class', "alert alert-dark")
												.attr('role', "alert")
												.append($('<div>')
													.attr('class', "row")
													.append($('<div>')
														.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
														.attr('style', "padding: 0 0 0 10px")
														.append($('<img>')
															.attr('src', getTheChatPic(data[x].prof_pic,data[x].user_id,data[x].baseURL))
														)
													)
													.append($('<div>')
														.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
														.append($('<p>')
															.attr('class', "chat-text")
															.append(data[x].chat_message)
														)
													)
												)
											)
										)
										.append($('<div>')
											.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
											.attr('style', "padding-right: 30px;padding-left: 0;position:relative")
											.append($('<p>')
												.attr('class', "chat-time")
												.attr('style', "position:relative")
												.append(reformatChatDate(data[x].chat_date,data[x].chat_hrs))
											)
										)
									);
							}else{
								$("#chat-msg")
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
											.attr('style', "padding-left: 18px;padding-right: 0;")
											.append($('<p>')
												.attr('class', "chat-time")
												.attr('style', "margin: 0px 0px;    position: absolute;text-align:left;")
												.append(reformatChatDate(data[x].chat_date,data[x].chat_hrs))
											)
										)
										.append($('<div>')
											.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
											.append($('<div>')
												.attr('class', "alert alert-secondary")
												.attr('role', "alert")
												.append($('<div>')
													.attr('class', "row")
													.append($('<div>')
														.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
														.append($('<p>')
															.attr('class', "chat-text")
															.append(data[x].chat_message)
														)
													)
													.append($('<div>')
														.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
														.attr('style', "padding: 0 0 0 10px")
														.append($('<img>')
															.attr('src', getTheChatPic(data[x].prof_pic,data[x].user_id,data[x].baseURL))
														)
													)
												)
											)
										)
										
									);
							}
						}
						
						$( "#chat-msg" ).scrollTop(100000);

					}		
			},
		error:
			function(data){
				//console.log("false");	
				$("#chat-msg").empty();	
			}
	});
}
function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "Jan";
		break;
		case 2:
		mDesc = "Feb";
		break;
		case 3:
		mDesc = "Mar";
		break;
		case 4:
		mDesc = "Apr";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "Jun";
		break;
		case 7:
		mDesc = "Jul";
		break;
		case 8:
		mDesc = "Aug";
		break;
		case 9:
		mDesc = "Sep";
		break;
		case 10:
		mDesc = "Oct";
		break;
		case 11:
		mDesc = "Nov";
		break;
		case 12:
		mDesc = "Dec";
		break;
	}
	return mDesc;
}
function reformatChatDate(d,hrs){
	var dbDate = new Date(d);
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	var hh = dbDate.getHours();
	var mn = dbDate.getMinutes();
	
	return "<span>"+getMonthDesc(mm)+" "+dd+"</span><br>"+"<span>"+hrs+"</span>";
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	//console.log(dbDate.getHours());
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}
function reformatHrs(d){
	var dbDate = new Date(d);

	var hh = dbDate.getHours();
	var mn = dbDate.getMinutes();
	//console.log(hh);
	if(hh == 0){
		hh = "00";
	}
	if(mn == 0){
		mn = "00";
	}
	//console.log(dbDate.getHours());
	return hh+":"+mn
}
