(function($){
	$.ajaxSetup({
	  cache: false
	});
	getFriendsData();
	$("#searchUserForm").submit(function(e){
		e.preventDefault();
		searchFullName();
	});
})(jQuery);
function searchFullName(){
	var name = $("#getUserFullName").val();
		if(name != ""){
		   $.ajax({
				type: "POST",
				url: "friends/searchUser",
				dataType: "json",
				data:{name:name},
				success:
					function(data) {
						//console.log(data);
						if(data.length > 0){
							$("#searchResult").empty();
							for(var x=0;x < data.length;x++){
								$("#searchResult")
									.append($('<li>')
										.append($('<div>')
											.attr('class', "left-col")
											.append($('<h4>')    
												.attr('style', "text-transform: capitalize;")
												.append(data[x].u_fullname)
											)
										)
										.append($('<div>')
											.attr('class', "right-col")
											.append(
												getUserStat(data[x].fr_status,data[x].u_id)
											)
										)
									);
							}
						}else{
							$("#searchResult").empty();
							$("#searchResult")
									.append($('<li>')
										.append($('<div>')
											.attr('class', "left-col")
											.append($('<h4>')    
												.attr('style', "text-transform: capitalize;")
												.append("Search Not Found.")
											)
										)
									);
						}
					},
				error:
				function(data){
					//console.log("false");		
				}
			});
		}
}
function getUserStat(status,id){
	if(status == 1){
		return $('<span>').append("Request Sent");
	}else{
		return $('<i>').attr('class', "fa fa-user-plus").attr('style', "font-size: 20px;cursor:pointer").attr('onclick', "addFriendUser("+id+")");
	}
}
function addFriendUser(id){
	//console.log(id);
	$.ajax({
			type: "POST",
			url: "friends/addFriendUser",
			dataType: "json",
			data:{id:id},
			success:
				function(data) {
					getFriendsData();
					searchFullName();
				},
			error:
			function(data){
				//console.log("false");		
			}
	});
}
function getFriendPic(baseURL,u_id,prof_pic){
	if(prof_pic == ""){
		return baseURL+"images/user-pic.png";
	}else{
		return baseURL+"images/uploads/"+u_id+"/"+prof_pic;
	}
}
function getFriendsData(){
	$.ajax({
			type: "GET",
			cache: false,
			url: "friends/getFriendsData",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					if(data.length > 0){
						$("#listOfFriends").empty();
						for(var x=0;x < data.length;x++){
							$("#listOfFriends")
								.append($('<div>')
									.attr('class', "col-md-6 col-sm-6 col-xs-12")
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6")
											.append($('<div>')
												.attr('class', "img-friend-container")
												.append($('<img>')
														.attr('src',getFriendPic(data[x].baseURL,data[x].u_id,data[x].prof_pic) )
												)
											)
										)
										.append($('<div>')
											.attr('class', "col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6")
											.attr('style', "padding-left: 5px;")
											.append($('<div>')
												.append($('<i>')
													.attr('onclick', "addChatRoom("+data[x].u_id+")")
													.attr('class', "fa fa-envelope")
													.attr('style', "cursor:pointer;display: block;text-align: right;font-size: 20px;")
												)
											)
											.append($('<p>')
												.attr('style', "font-size:2.2vh;margin-bottom: 5px;margin-top:0px")
												.append($('<strong>')
													.attr('style', "display: block;text-transform:capitalize;text-align: left;")
													.append(data[x].u_fullname)
												)
											)
											.append($('<p>')
												.attr('style', "display:block;text-align:left;word-break: break-all;")
												.append(data[x].u_email)
											)
										)
										
									)
								);
						}
						$("#listOfFriends").show();
						$("#noListOfFriends").hide();
					}else{
						$("#listOfFriends").hide();
						$("#noListOfFriends").show();
					}	
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function addChatRoom(friend_id){
	$.ajax({
			type: "POST",
			url: "friends/addChatRoom",
			dataType: "json",
			data: {friend_id:friend_id},
			success:
				function(data) {
					window.location.href = data+"chat";
				},
			error:
			function(data){
				//console.log("false");		
			}
		});
}
