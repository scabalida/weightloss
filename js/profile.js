 var weightLossDataTable = $('#weightLossTable').DataTable();
 var userWallStatusTable = $('#userWallStatusTable').DataTable();
 var wall_comment_id = "";
 var wall_id = "";
 var wall_user_id = "";
 
(function() {
	'use strict';
		document.getElementById("btnEditProfile").addEventListener('click', function() {
		
		var forms = document.getElementsByClassName('needs-validation-editprofile');
		var editUserProfileForm = document.getElementById('editUserProfileForm');
		var validation = Array.prototype.filter.call(forms, function(editUserProfileForm) {
			editUserProfileForm.addEventListener('submit', function(event) {
				
				var bio = $("#editbio").val();
				var fullname = $("#editfullname").val();
				var bday = $("#editbday").val();
				var email = $("#editemail").val();
				 var imgPic = document.getElementById('imgfile');
				if (editUserProfileForm.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
					if(imgPic.value == ""){
					  $('.errorImgPic').show();
					 }else{
					  $('.errorImgPic').hide();
					 }
				}else{
					event.preventDefault();

						//console.log("true");
						//console.log(fullname);
						//console.log(bday);
						//console.log(email);
						var passdata = new FormData(this);
						   passdata.append('u_bio', bio);
						   passdata.append('u_fullname', fullname);
						   passdata.append('u_email', email);
						   passdata.append('u_bday', bday);
						$.ajax({
							type: "POST",
							url: "user/updateUserProfile",
							data: passdata,
							processData:false,
							contentType:false,
							cache:false,
							async:true,
							success:
								function(data) {
									console.log(data);
									if(data == 'true'){
										$('#editProfSuccess').show();
										$('#editProfError').hide();
										editApproveLink();
										setTimeout(function(){ $('#editProfSuccess').hide(); }, 2500);
										console.log("true");
									}
									else if(data == 'false'){
										console.log("false");
										$('#editProfError').show();
										$('#editProfSuccess').hide();
									}
								},
							error:
								function(data){
									console.log("false");		
									$('#editProfError').show();
									$('#editProfSuccess').hide();
								}
						});
				
				}
				editUserProfileForm.classList.add('was-validated');
			});
		});
		//getunapprovelink();
		
	}, false);
	$("#imgfile").change(function() {
	
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg","image/gif"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3])))
		{
			$('#dispImgProfile').attr('src','user-pic.png');
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
		if($("#imgfile").val() == ""){
			$('.errorImgPic').show();
		}else{
			$('.errorImgPic').hide();
		}
	});
	editApproveLink();
	checkInputWeightLoss();
	getMyWallStatus();
	getAllWeightLoss();
	myWeightLossChart();
	$("#clickEditProf").click(function(){
		$("#myProf").hide();
		$("#editProf").show();
		editApproveLink();
	});
	$("#clickMyProf").click(function(){
		$("#myProf").show();
		$("#editProf").hide();
		editApproveLink();
	});
	$("#addWeightLossTday").submit(function(e){
		e.preventDefault();
		var weightLoss = $("#weightloss").val();
		//console.log(weightLoss);
		if(weightLoss == "" || weightLoss == undefined){
			$("#addWeightLossTday .invalid-feedback").show();
		}else{
			$("#addWeightLossTday .invalid-feedback").hide();
			$.ajax({
				type: "POST",
				url: "user/addWeightLoss",
				data:  {weightLoss:weightLoss},
				dataType: "json",
				success:
					function(data) {
					//console.log(data);
						if(data == "true"){
							$("#weightloss").prop( "disabled", true );
							$("#addWeightLossTday button").prop( "disabled", true );
							$("#addWeightLossSuccess").show();
							setTimeout(function(){ $('#addWeightLossSuccess').hide(); }, 2000);
						}else{
							$("#weightloss").prop( "disabled", false );
							$("#addWeightLossTday button").prop( "disabled", false );
							$("#addWeightLossError").show();
							setTimeout(function(){ $('#addWeightLossError').hide(); }, 2000);
						}
						$("#weightloss").val("");
						myWeightLossChart();
						getAllWeightLoss();
					},
				error:
				function(data){
					console.log("false");		
				}
			});
		}
	});
	$("#editWeightLossForm").submit(function(e){
		e.preventDefault();
		var weightLoss = $("#editWeightloss").val();
		var id = $("#weightID").val();
		console.log(weightLoss);
		if(weightLoss == "" || weightLoss == undefined){
			$("#editWeightLossForm .invalid-feedback").show();
		}else{
			$("#editWeightLossForm .invalid-feedback").hide();
			$.ajax({
				type: "POST",
				url: "user/editWeightLoss",
				data:  {weightLoss:weightLoss,id:id},
				dataType: "json",
				success:
					function(data) {
					//console.log(data);
						if(data == "true"){
							$("#editWeightLossSuccess").show();
							setTimeout(function(){ $('#editWeightLossSuccess').hide(); }, 2000);
						}else{
							$("#editWeightLossError").show();
							setTimeout(function(){ $('#editWeightLossError').hide(); }, 2000);
						}
						$("#editWeightLossModal").modal("hide");
						$("#editWeightloss").val("");
						$("#weightID").val("");
						myWeightLossChart();
						getAllWeightLoss();
					},
				error:
				function(data){
					console.log("false");		
				}
			});
		}
	});
	$("#addWallStatusForm").submit(function(e){
		e.preventDefault();
		var status_description = $("#status_description").val();
		
		if(status_description != "" && status_description != undefined){
			$.ajax({
				type: "POST",
				url: "profile/addWallStatus",
				data:  {status_description:status_description},
				dataType: "json",
				success:
					function(data) {
						console.log(data);
						$("#status_description").val("");
						userWallStatusTable = $('#userWallStatusTable').DataTable();
						getMyWallStatus();
					},
				error:
				function(data){
					console.log("false");		
				}
			});
		}
	});	
})();
function myWeightLossChart(){
	$.ajax({
		type: "GET",
		url: "user/getWeightLoss",
		dataType: "json",
		success:
		function(data) {
			//console.log(data);
			var totW = 0;
			$("#chartContainer").empty();
			if(data.length > 0){
				var newArray = [];
				for(var x=0;x < data.length;x++){
					var thisArray = {
						y: parseFloat(data[x].y),
						label: reformatDate(data[x].label)
						
					};
					//sumAll += parseFloat(data[x].y);
					newArray.push(thisArray);
				}
				var getSort = newArray.sort();
				var getLastArr = getSort.length - 1;
				var minX = getSort[getLastArr].y - 25;
				var maxX = getSort[0].y + 25;
				totW = newArray[0].y - newArray[getSort.length-1].y;
				if(minX < 0){
					minX = 0;
				}
				$("#totalWeightLoss").html(totW+" lbs");
				//console.log(maxX);
				//console.log(minX);
				console.log(getSort);
				console.log(newArray);
				var options = {
					exportEnabled: false,
					animationEnabled: true,
					title: {
						text: "Weight Loss"
					},
					axisX: {
						title: "Date",
					},
					axisY: {
						title: "In Pounds",
						minimum: minX,
						maximum: maxX
					},
					data: [
					{
						type: "splineArea",
						dataPoints:newArray
					}
					]
				};
				$("#chartContainer").CanvasJSChart(options);
				$("#chartContainer").show();
				$("#noWeightLossData").hide();
			}else{
				$("#chartContainer").hide();
				$("#noWeightLossData").show();
			}
		},
		error:
		function(data){
			console.log("false");		
		}
	});
}
function imageIsLoaded(e) {
	
	$('#dp').attr('src', e.target.result);
	//console.log($('#dp').attr('src'));
}
function reformatDate(d){
	var dbDate = new Date(d);
	var yyyy = dbDate.getFullYear();
	var mm = dbDate.getMonth() + 1;
	var dd = dbDate.getDate();
	return getMonthDesc(mm)+" "+dd+", "+yyyy;
}
function editApproveLink(){
	$.ajax({
		type: "POST",
		url: "user/getUserData",
		dataType: "json",
		success:
			function(data) {
				var thisURL = window.location.hostname;
				var dbDate = new Date(data[0].u_bday);
				var yyyy = dbDate.getFullYear();
				var mm = dbDate.getMonth() + 1;
				var dd = dbDate.getDate();
				var newDate = yyyy+"-"+minTwoDigits(mm)+"-"+minTwoDigits(dd);
				var dispDate = getMonthDesc(mm)+" "+minTwoDigits(dd)+", "+yyyy;
				var myP = 0;
				if(data[0].u_points == ""){myP = 0}else{myP = data[0].u_points;}
			//	console.log(data);
				$('input[name="editfullname"]').val(data[0].u_fullname);
				$('#editbio').val(data[0].u_bio);
				$('#myfullname').html(data[0].u_fullname);
				document.getElementById("editbday").value = newDate;
				
				$('#myusername').html(data[0].u_username+" ("+myP+")");
				
				$('#editemail').val(data[0].u_email);
				$('#myaddress').html(data[0].u_email);
				$('#mybio').html(data[0].u_bio);
				$('#mybday').html(dispDate);
				if(data[0].prof_pic){
					$('#dispImgProfile').attr('src',"images/uploads/"+data[0].u_id+"/"+data[0].prof_pic);
					$('#myImgProfile').attr('src',"images/uploads/"+data[0].u_id+"/"+data[0].prof_pic);
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
	
}

function minTwoDigits(n) {
  return (n < 10 ? '0' : '') + n;
}
function getMonthDesc(m){
	var mDesc = "";
	switch (m){
		case 1:
		mDesc = "January";
		break;
		case 2:
		mDesc = "February";
		break;
		case 3:
		mDesc = "March";
		break;
		case 4:
		mDesc = "April";
		break;
		case 5:
		mDesc = "May";
		break;
		case 6:
		mDesc = "June";
		break;
		case 7:
		mDesc = "July";
		break;
		case 8:
		mDesc = "August";
		break;
		case 9:
		mDesc = "September";
		break;
		case 10:
		mDesc = "October";
		break;
		case 11:
		mDesc = "November";
		break;
		case 12:
		mDesc = "December";
		break;
	}
	return mDesc;
}
function checkInputWeightLoss(){
	$.ajax({
		type: "GET",
		url: "user/checkInputWeightLoss",
		dataType: "json",
		success:
			function(data) {
				//console.log(data);
				if(data == 0){
					$("#weightloss").prop( "disabled", false );
					$("#addWeightLossTday button").prop( "disabled", false );
				}else{
					$("#weightloss").prop( "disabled", true );
					$("#addWeightLossTday button").prop( "disabled", true );
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});
	
}
function getAllWeightLoss(){
	weightLossDataTable.destroy();
   $.ajax({
		type: "GET",
		url: "user/getAllMyWeightLoss",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyWeightLossTable").empty();
					for(var x=0;x < data.length;x++){
						$("#weightLossTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(reformatDate(data[x].date))
								)
								.append($('<td>')
									.append(data[x].weight_loss)
								)
								.append($('<td>')
									.attr('style', "width:25px;")
									.append($('<button>')
										.attr('onclick', "editWeightLoss("+data[x].id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
							);
					}
					$("#weightLossTable").show();
					$("#noWeightLossTable").hide();
					weightLossDataTable = $('#weightLossTable').DataTable({
						  "ordering": false,
						  "pageLength": 5
						});
				}else{
					$("#weightLossTable").hide();
					$("#noWeightLossTable").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}
function editWeightLoss(id){
	$.ajax({
		type: "POST",
		url: "user/getSpecificWeightLoss",
		data:  {id:id},
		dataType: "json",
		success:
			function(data) {
				console.log(data);
				$("#editWeightloss").val(data[0].weight_loss);
				$("#weightID").val(data[0].id);
				$("#editWeightLossModal").modal("show");
			},
		error:
		function(data){
			console.log("false");		
		}
	});
}
function chckComments(num,id){
	if(num > 0){
		return "<span style='font-size:11px;color:#007bff;cursor:pointer' id='viewCommentsArea"+id+"' onclick='viewComments("+id+")'>&emsp;View Comments ("+num+")</span><div id='showComments"+id+"' style='display:none;background:#fff;    margin-top: 10px;'></div>";
	}
}
function replyToThisWallStatus(id,user_id){

	$("#txtArea"+id).show();
			
}
function hideReplyComment(id){
	$("#txtArea"+id).hide();
}
function getMyWallStatus(){
	userWallStatusTable.destroy();
	var base_url = $("#base_url").val();
   $.ajax({
		type: "GET",
		url: "profile/getMyWallStatus",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyuserWallStatusTable").empty();
					for(var x=0;x < data.length;x++){
						$("#userWallStatusTable").find('tbody')
									.append($('<tr>')
										.append($('<td>')
											.append($('<div>')
												.attr('class', "alert alert-secondary")
												.attr('role', "alert")
												.append($('<div>')
													.attr('class', "row")
													.append($('<div>')
														.attr('class', "col-lg-11 col-xl-11 col-md-11 col-sm-11 col-xs-11")
														.append($('<h5>')
															.append(data[x].status_description)
															.append($('<br>'))
															.append($('<br>'))
															.append("<span style='font-size: 12px;'>"+data[x].date+"&ensp;-&ensp;Current Weight: "+data[x].current_weight+" lbs</span>")
														)
														.append($('<span>')
															.append("&ensp;&ensp;<i onclick=replyToThisWallStatus("+data[x].id+","+data[x].user_id+") class='fa fa-comment' style='font-size:15px;cursor:pointer;margin-left: -10px;'> <span style='font-family: Open Sans, sans-serif;font-size: 13px;'>Comment</span></i>")
														)
														.append($('<span>')
															.append(chckComments(data[x].numComments,data[x].id))
														)
	
													)
													.append($('<div>')
														.attr('class', "col-lg-1 col-xl-1 col-md-1 col-sm-1 col-xs-1")
														.attr('style', "padding: 0 0 0 10px")
														.append($('<img>')
															.attr('style', "max-width: 65px;border-radius: 50%;")
															.attr('src', base_url+data[x].prof_pic)
														)
													)
													.append($('<div>')
														.attr('id', "txtArea"+data[x].id)
														.attr('style', "width: 95%;padding-top: 8px;margin-left: 4%;display:none")
														.append($('<div>')
															.attr('class', "input-group mb-3")
															.append("<span onclick=hideReplyComment("+data[x].id+") style='margin-top: -5px;padding-right: 10px;font-size: 36px;font-weight: bold;cursor:pointer'>-</span>")
															.append($('<textarea>')
																.attr('class', "form-control")
																.attr('rows', "1")
																.attr('id', "replyToComment"+data[x].id)
																.attr('placeholder', "Reply To This Comment")
															)
															.append($('<div>')
																.attr('class', "input-group-append")
																.append($('<button>')
																	.attr('class', "btn btn-dark")
																	.attr('onclick', "saveReplyTo("+data[x].id+","+data[x].user_id+")")
																	.attr('type', "button")
																		.append("KOMMENTERA")
																)
															)
														)
													)
												)
											)
										)
									);
					}
					$("#userWallStatusTable").show();
					userWallStatusTable = $('#userWallStatusTable').DataTable({
						  "searching": false,
						  "lengthChange": false,
						  "ordering": false,
						  "pageLength": 10
						});
				}else{
					$("#userWallStatusTable").hide();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
	
}

	function saveReplyTo(wall_status_id,wall_status_user_id){
			var comment = $("#replyToComment"+wall_status_id).val();
			console.log(comment);
			if(comment != undefined && comment != ""){
				$.ajax({
						type: "POST",
						url: "profile/addWallStatusComment",
						dataType: "json",
						data:  {wall_status_id:wall_status_id,wall_status_user_id:wall_status_user_id,comment:comment},
						success:
							function(data) {
								getMyWallStatus();
							},
						error:
						function(data){	
						}
				});
			}
	}
	function viewComments(id){
			//console.log(id);
			var base_url = $("#base_url").val();
			$.ajax({
					type: "POST",
					url: "profile/getWallStatusComments",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							console.log(data);
							for(var x=0;x < data.length;x++){
								$("#showComments"+id)
								.append($('<table style="display:block;padding-left: 15px;">')
									.append($('<tr>')
										.append($('<td>')
											.append($('<img>')
												.attr('style', "max-width: 35px;border-radius: 50%;")
												.attr('src', base_url+data[x].pic)
											)
										)
										.append($('<td>')
											.attr('style', "width: 100%")
											.append($('<p>')
												.append("<strong style='color:#007bff;'>"+data[x].fullname+"</strong>")
												.append("&emsp;"+data[x].comment_text+"")
												.append($('<br>'))
												.append($('<br>'))
												.append("<span onclick=replyToWallComment("+data[x].id+","+data[x].wall_status_id+","+data[x].wall_status_user_id+") style='font-family: Open Sans, sans-serif;cursor:pointer'><i class='fa fa-comment'></i>&nbsp;Reply</span>")
												.append("<span style='color: rgb(0, 123, 255);cursor: pointer;' onclick=viewReplies('"+data[x].id+"')>&emsp;View Replies</span>")
												.append("<i style='float: right;'>"+data[x].comment_date+"</i>")
											)
											.append($('<div>')
												.attr('id', "showCommentsToWall"+data[x].id)
												.attr('style', "display:none;background:#fff;    margin-top: 10px;")
											)
											.append($('<div>')
														.attr('id', "txtAreaComment"+data[x].id)
														.attr('style', "width: 95%;padding-top: 8px;margin-left: 4%;display:none")
														.append($('<div>')
															.attr('class', "input-group mb-3")
															.append("<span onclick=hideReplyWallComment("+data[x].id+") style='margin-top: -5px;padding-right: 10px;font-size: 36px;font-weight: bold;cursor:pointer'>-</span>")
															.append($('<textarea>')
																.attr('class', "form-control")
																.attr('rows', "1")
																.attr('id', "wallReplyToComment"+data[x].id)
																.attr('placeholder', "Reply To This Comment")
															)
															.append($('<div>')
																.attr('class', "input-group-append")
																.append($('<button>')
																	.attr('class', "btn btn-dark")
																	.attr('onclick', "saveReplyToWallComment()")
																	.attr('type', "button")
																		.append("KOMMENTERA")
																)
															)
														)
													)
										)
									)
								);
							}
							$("#showComments"+id).show();
							$("#viewCommentsArea"+id).hide();
						},
					error:
						function(data){	
					}
				});
	}
	function replyToWallComment(id,wall_status_id,wall_status_user_id){

		$("#txtAreaComment"+id).show();
		wall_comment_id = id;
		wall_id = wall_status_id;
		wall_user_id = wall_status_user_id;
				
	}
	function hideReplyWallComment(id){
		$("#txtAreaComment"+id).hide();
	}
	function saveReplyToWallComment(){
			var comment = $("#wallReplyToComment"+wall_comment_id).val();
			console.log(wall_comment_id);
			console.log(wall_id);
			console.log(wall_user_id);
			console.log(comment);
			if(comment != undefined && comment != ""){
				
				$.ajax({
						type: "POST",
						url: "profile/addCommentToWallStatusComment",
						dataType: "json",
						data:  {comment_id:wall_comment_id,wall_status_id:wall_id,wall_status_user_id:wall_user_id,comment:comment},
						success:
							function(data) {
								$("#showComments"+wall_id).empty();
								viewComments(wall_id);
							},
						error:
						function(data){	
						}
				});
			}
	}
		function viewReplies(id){
			//console.log(id);
			var base_url = $("#base_url").val();
			$.ajax({
					type: "POST",
					url: "profile/getWallStatusCommentFromComments",
					dataType: "json",
					data:  {id:id},
					success:
						function(data) {
							console.log(data);
							$("#showCommentsToWall"+id).empty();
							for(var x=0;x < data.length;x++){
								
								$("#showCommentsToWall"+id)
								.append($('<table style="display:block;padding-left: 15px;">')
									.append($('<tr>')
										.append($('<td>')
											.append($('<img>')
												.attr('style', "max-width: 35px;border-radius: 50%;")
												.attr('src', base_url+data[x].pic)
											)
										)
										.append($('<td>')
											.append($('<p>')
												.attr('style', "margin-bottom: 0;")
												.append("<strong style='color:#007bff;'>"+data[x].fullname+"</strong>")
												.append("&emsp;"+data[x].comment_text+"")
												.append($('<br>'))
												.append($('<br>'))
												.append("<i>"+data[x].comment_date+"</i>")
											)
										)
									)
								);
							}
							$("#showCommentsToWall"+id).show();
							
						},
					error:
						function(data){	
					}
				})
		}