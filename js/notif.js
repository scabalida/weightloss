(function($){
	$.ajaxSetup({
	  cache: false
	});

	countNotReadNotif();
	setInterval(function(){ 
		//getMyNotif();
		countNotReadNotif();
		countFR();
	}, 2000);
	$("#dropdownFRLink").click(function(){
		getMyFriendRequest();
	});	
	$("#dropdownNotifLink").click(function(){
		getMyNotif();
	   $.ajax({
			type: "POST",
			url: "user/makeNotifRead",
			dataType: "json",
			success:
				function(data) {
					if(data == "true"){
						$("#countNotifs").html(0);
						$("#countNotifs").hide();
					}
				},
			error:
			function(data){
				//console.log("false");		
			}
		});
	});
})(jQuery);
function countFR(){
	$.ajax({
			type: "GET",
			url: "friends/getFriendRequest",
			dataType: "json",
			success:
				function(data) {
					if(data.length > 0){
						$("#countFriendRequest").show();
						$("#countFriendRequest").html(data.length);
					}else{
						$("#countFriendRequest").hide();
					}
					
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function countNotReadNotif(){
	$.ajax({
			type: "GET",
			url: "user/countNotReadNotif",
			dataType: "json",
			success:
				function(data) {
					if(data > 0){
						$("#countNotifs").show();
						$("#countNotifs").html(data);
					}else{
						$("#countNotifs").hide();
					}
					
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}

function getThePic(profpic,type,baseURL,id){
	if(type == 1 || type == 2 || type == 4 || type == 5){
		if(profpic == ""){
			return baseURL+"images/user-pic.png";
		}else{
			return baseURL+"images/uploads/"+id+"/"+profpic;
		}
	}else{
		if(profpic == ""){
			return baseURL+"images/user-pic.png";
		}else{
			return baseURL+"images/balloon.jpg";
		}
	}
}
function getTheNotifText(type,fullname,notif_text,link_name){
	
	if(type == 1 || type == 2){
		return "<strong>"+fullname+"</strong> "+notif_text+" <strong>"+link_name+"</strong>";
	}else if(type == 4){
		return "<strong>"+fullname+"</strong> "+notif_text+" in <strong>"+link_name+"</strong>";
	}else if(type == 5){
		return "<strong>"+fullname+"</strong> "+notif_text+" in <strong>"+link_name+"</strong>";
	}else if(type == 3){
		return "<strong>"+notif_text+"</strong>";
	}
}
function getMyNotif(){
	$("#dropdown-text").empty();
	$.ajax({
			type: "GET",
			cache: false,
			url: "user/getMyNotifs",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					if(data.length > 0){
						for(var x=0;x < data.length;x++){
							$("#dropdown-text")
								.append($('<a>')
									.attr('class', "dropdown-item")
									.attr('href', data[x].baseURL+"comments/view_link/"+data[x].link_id)
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3")
											.append($('<img>')
													.attr('src', getThePic(data[x].prof_pic,data[x].notif_type,data[x].baseURL,data[x].notif_user_id))
											)
										)
										.append($('<div>')
											.attr('class', "col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9")
											.append($('<p>')
												.attr('style', "font-size:14px;margin-bottom: 5px;margin-top: 5px;")
												.append(getTheNotifText(data[x].notif_type,data[x].u_fullname,data[x].notif_text,data[x].link_name))
											)
											.append($('<p>')
												.append(data[x].notif_date)
											)
										)
										
									)
								);
						}
					}else{
						$("#dropdown-text")
								.append($('<div>')
									.attr('class', "alert alert-light")
									.attr('role', "alert")
									.attr('style', "width: 250px;")
									.append($('<h5>')
										.append("No notification/s.")
									)
								);
					}	
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function getThePicFR(profpic,baseURL,id){

		if(profpic == ""){
			return baseURL+"images/user-pic.png";
		}else{
			return baseURL+"images/uploads/"+id+"/"+profpic;
		}

}
function getMyFriendRequest(){
		$("#dropdown-text").empty();
		$.ajax({
			type: "GET",
			cache: false,
			url: "friends/getFriendRequest",
			dataType: "json",
			success:
				function(data) {
					//console.log(data);
					if(data.length > 0){
						
						for(var x=0;x < data.length;x++){
							$("#dropdown-text")
								.append($('<div>')
									.attr('class', "dropdown-item")
									.append($('<div>')
										.attr('class', "row")
										.append($('<div>')
											.attr('class', "col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4")
											.append($('<img>')
												.attr('style', "width:100%;height:60px")
												.attr('src', getThePicFR(data[x].prof_pic,data[x].baseURL,data[x].u_id))
											)
										)
										.append($('<div>')
											.attr('class', "col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8")
											.attr('style', "padding-left:0")
											.append($('<p>')
												.attr('style', "text-align:center;font-size:16px;margin-bottom: 5px;margin-top: 5px;font-weight: bold;text-transform: capitalize;")
												.append(data[x].u_fullname)
												
											)
											.append($('<p>')
												.attr('style', "text-align:center")
												.append($('<button>')
													.attr('class', "btn btn-primary")
													.attr('type', "button")
													.attr('style', "margin-right:4px;padding:5px 5px 5px 5px;")
													.attr('onclick', "confirmFriendRequest("+data[x].u_id+")")
													.append("Confirm")
												)
												.append($('<button>')
													.attr('class', "btn btn-secondary")
													.attr('type', "button")
													.attr('style', "margin-right:4px;padding:5px 5px 5px 5px;")
													.attr('onclick', "deleteFriendRequest("+data[x].u_id+")")
													.append("Delete")
												)
											)
											
										)
										
									)
								);
						}
					}else{
						$("#dropdown-text")
								.append($('<div>')
									.attr('class', "alert alert-light")
									.attr('role', "alert")
									.attr('style', "width: 250px;border:0")
									.append($('<h5>')
										.append("No Friend Request/s.")
									)
								);
					}	
				},
			error:
			function(data){
				//console.log(data);		
			}
		});
}
function confirmFriendRequest(friend_id){
	//console.log(friend_id);
	$.ajax({
			type: "POST",
			url: "friends/confirmFriendRequest",
			dataType: "json",
			data: {friend_id:friend_id},
			success:
				function(data) {
					getMyFriendRequest();
				},
			error:
			function(data){
				//console.log("false");		
			}
		});

}
function deleteFriendRequest(friend_id){
	console.log(friend_id);
	$.ajax({
			type: "POST",
			url: "friends/deleteFriendRequest",
			dataType: "json",
			data: {friend_id:friend_id},
			success:
				function(data) {
					getMyFriendRequest();
				},
			error:
			function(data){
				//console.log("false");		
			}
		});
}