var userID = 0;
(function() {
	'use strict';
	
	var addlinkCategory = $("#addlinkCategory").kendoMultiSelect({
    autoClose: false
    }).data("kendoMultiSelect");
	
    var addlinkWarning = $("#addlinkWarning").kendoMultiSelect({
    autoClose: false
    }).data("kendoMultiSelect");
	
	window.addEventListener('load', function() {
		var forms = document.getElementsByClassName('needs-validation');
		var addUserForm = document.getElementById('addUserForm');
		var validation = Array.prototype.filter.call(forms, function(addUserForm) {
			  addUserForm.addEventListener('submit', function(event) {
				if (addUserForm.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
					if($('#upassword').val() == $('#cpassword').val()){
						$('.errorPass').hide();
					}else{
						$('.errorPass').show();
					}
				}else{
					event.preventDefault();
					console.log("true");
					if($('#upassword').val() == $('#cpassword').val()){
						$('.errorPass').hide();
						$.ajax({
							type: "POST",
							url: "user/addUser",
							dataType: "json",
							data:  $(this).serialize() ,
							success:
								function(data) {
									console.log(data);
									if(data == "true"){
										$('#signInModal').modal("hide"); 
										$('#registerSuccess').modal("show"); 
										setTimeout(function(){ window.location.reload(); }, 3000);
										console.log("true");
									}
									else if(data == "false"){
										console.log("false");
										$('#userAddError').show();
									}
								},
							error:
								function(data){
									console.log(data);		
									console.log("false");
									$('#userAddError').show();									
								}
						});
					}else{
						$('.errorPass').show();
					}
				}
				addUserForm.classList.add('was-validated');
			  });
		});
	}, false);

	$("#btnEditUserPass").click(function(){
		var forms = document.getElementsByClassName('needs-validation-edituserpass');
		var editUserPass = document.getElementById('editUserPassForm');
		var validation = Array.prototype.filter.call(forms, function(editUserPass) {
			  editUserPass.addEventListener('submit', function(event) {
				if (editUserPass.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
					if($('#editnewpass').val() == $('#editcnewpass').val()){
						$('.errorUserPass').hide();
					}else{
						$('.errorUserPass').show();
					}
				}else{
					event.preventDefault();
					console.log("true");
					if($('#editnewpass').val() == $('#editcnewpass').val()){
						$('.errorUserPass').hide();
							$.ajax({
								type: "POST",
								url: "user/getUnameAndPass",
								dataType: "json",
								data:  $(this).serialize() ,
								success:
									function(data) {
										console.log(data);
										console.log(data.status);
										if(data.status == 'true'){
											var newUsername = $("#editusername").val();
											var newPass = $("#editnewpass").val();
												$.ajax({
													type: "POST",
													url: "updateUnameAndPass",
													dataType: "json",
													data:  {newusername:newUsername,newpassword:newPass},
													success:
														function(data) {
															console.log(data);
															if(data == 'true'){
																$("#editusername").val(newUsername);
																$('#editUserPassSuccess').show();	
																setTimeout(function(){ $('#editUserPassSuccess').hide(); }, 250);
																console.log("true");
															}
															else if(data== 'false'){
																console.log("false");
																$('#editUserPassError').show();	
																setTimeout(function(){ $('#editUserPassError').hide();	 }, 2500);
															}
														},
													error:
														function(data){
															console.log("false");
																$('#editUserPassError').show();	
																setTimeout(function(){ $('#editUserPassError').hide();	 }, 2500);									
														}
												});
										//setTimeout(function(){ window.location.reload(); }, 3000);
										console.log("true");
									}
									else if(data.status == 'false'){
										console.log("false");
										$('#editUserPassNotMatch').show();	
									}
								},
							error:
								function(data){
									console.log(data);		
									console.log("false");
									$('#editUserPassNotMatch').show();									
								}
						});
					}else{
						$('.errorUserPass').show();
					}
				}
				editUserPass.classList.add('was-validated');
			  });
		});
	});
	getAllUsers();
	$("#deleteNow").click(function(){
	   $.ajax({
			type: "POST",
			url: "user/deleteuser",
			dataType: "json",
			data:  {id:userID},
			success:
				function(data) {
					//console.log(data[0].link);
					if(data == "true"){
						$('#removeUserModal').modal("hide");
						$('.deleteMessageSucc').show();
						setTimeout(function(){ $('.deleteMessageSucc').hide();}, 2000);
						getAllUsers();
					}else{
						$('#removeUserModal').modal("hide");
						$('.deleteMessageError').show();
						setTimeout(function(){ $('.deleteMessageError').hide(); }, 2000);
						getAllUsers();
					}
				},
			error:
			function(data){
				console.log("false");		
			}
		});
	});
	$("#editUserBtn").click(function(){
		var forms = document.getElementsByClassName('edituser-needs-validation');
		var fname = document.getElementById('fname').value;
		var email = document.getElementById('email').value;
		var bday = document.getElementById('bday').value;
		var role = document.getElementById('user_role').value;
		var editUserForm = document.getElementById('editUserForm');
		var validation = Array.prototype.filter.call(forms, function(editUserForm) {
			  editUserForm.addEventListener('submit', function(event) {
				if (editUserForm.checkValidity() === false) {
					console.log("false");
					event.preventDefault();
					event.stopPropagation();
				}else{
					event.preventDefault();
					console.log("true");
						$.ajax({
							type: "POST",
							url: "user/editUserData",
							dataType: "json",
							data:  {id:userID,fname:fname,email:email,bday:bday,role:role},
							success:
								function(data) {
									console.log(data);
									if(data == "true"){
										getAllUsers();
										$('#editUserModal').modal("hide"); 
										$('#userEditSuccess').show(); 
										setTimeout(function(){$('#userEditSuccess').hide();}, 2000);
										console.log("true");
									}
									else if(data == "false"){
										getAllUsers();
										console.log("false");
										$('#userEditError').show();
									}
								},
							error:
								function(data){
									console.log(data);		
									console.log("false");
									$('#userEditError').show();									
								}
						});
				}
				editUserForm.classList.add('was-validated');
			  });
		});	
	});
})();

function getAllUsers(){
   $.ajax({
		type: "GET",
		url: "user/getAllUsers",
		dataType: "json",
		success:
			function(data) {
				if(data.length > 0){
					$("#tbodyallusers").empty();
					for(var x=0;x < data.length;x++){
						$("#userTable").find('tbody')
							.append($('<tr>')
								.append($('<td>')
									.append(data[x].u_fullname)
								)
								.append($('<td>')
									.append(data[x].u_email)
								)
								.append($('<td>')
									.append(setDate(data[x].u_bday))
								)
								.append($('<td>')
									.append(data[x].current_weight + " lbs")
								)
								.append($('<td>')
									.append(data[x].total_weight_loss + " lbs")
								)
								.append($('<td>')
									.append(userType(data[x].u_role))
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "editUserData("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-success text-center")
										.append($('<i>')
											.attr('class', "fa fa-edit")
										)
									)
								)
								.append($('<td>')
									.append($('<button>')
										.attr('onclick', "removeUser("+data[x].u_id+")")
										.attr('type', "button")
										.attr('class', "pull-right btn btn-danger text-center")
										.append($('<i>')
											.attr('class', "fa fa-trash")
										)
									)
								)
							);
					}
					$("#userTable").show();
					$("#nousers").hide();
					$('#userTable').DataTable();
				}else{
					$("#userTable").hide();
					$("#nousers").show();
				}
			},
		error:
		function(data){
			console.log("false");		
		}
	});		
}

function userType(d){
	if(d == 1){
		return "Regular";
	}else if(d == 2){
		return "Donator";
	}else if(d == 3){
		return "VIP";
	}else if(d == 4){
		return "Updater";
	}else if(d == 5){
		return "Operator";
	}else if(d == 6){
		return "Admin";
	}else{
		return "Regular";
	}
	
}

function setDate(n){
	var d = new Date(n);
	
	var month = d.getMonth();
	var mWord;
	switch(month){
		case 0:
        mWord = "January";
        break;
    case 1:
        mWord = "February";
        break;
	case 2:
        mWord = "March";
        break;
	case 3:
        mWord = "April";
        break;
	case 4:
        mWord = "May";
        break;
	case 5:
        mWord = "June";
        break;
	case 6:
        mWord = "July";
        break;
	case 7:
        mWord = "August";
        break;
	case 8:
        mWord = "September";
        break;
	case 9:
        mWord = "October";
        break;
	case 10:
        mWord = "November";
        break;
	case 11:
        mWord = "December";
        break;
	}
	var nDate = d.getDate();
	var nYear = d.getFullYear();
	
	return mWord+" "+nDate+", "+nYear;
}
function removeUser(id){
	userID = id;
	$('#removeUserModal').modal("show");
	
}
function editUserData(id){
	//console.log(id);
	userID = id;
	$.ajax({
		type: "POST",
		url: "user/getThisUserData",
		dataType: "json",
		data:  {id:userID},
		success:
			function(data) {
			//	console.log(data);
				$('#fname').val(data[0].u_fullname);
				$('#email').val(data[0].u_email);
				document.getElementById('bday').value = data[0].u_bday;
				document.getElementById('user_role').value = data[0].u_role;
				$('#editUserModal').modal("show");

			},
		error:
		function(data){
			console.log("false");		
		}
	});
}